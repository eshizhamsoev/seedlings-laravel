<?php

return [
    'account' => env('CDEK_ACCOUNT'),
    'secret' => env('CDEK_SECRET')
];
