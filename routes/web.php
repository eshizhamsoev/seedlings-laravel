<?php

use App\Http\Controllers\CartApiController;
use App\Http\Controllers\ModalController;
use App\Http\Controllers\OrderApiController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PickUpPointApiController;
use App\Http\Controllers\WishlistApiController;
use Illuminate\Support\Facades\Route;

Route::prefix('api/website/')->group(function () {
    Route::post('subscribe')->name('subscribe');
    Route::get('modals/{modalType}', [ModalController::class, 'show'])
        ->name('modal');

    Route::get('search', [\App\Http\Controllers\SearchApiController::class, 'search'])->name('search');

    Route::prefix('cart')->group(function () {
        Route::get('', [CartApiController::class, 'index'])->name('cart.index');
        Route::get('info', [CartApiController::class, 'getCartInfo'])->name('cart.info');
        Route::post('add', [CartApiController::class, 'add'])->name('cart.add');
        Route::post('update', [CartApiController::class, 'update'])->name('cart.update');
    });

    Route::prefix('wishlist')->group(function () {
        Route::post('remove', [WishlistApiController::class, 'remove'])->name('wishlist.remove');
        Route::post('toggle', [WishlistApiController::class, 'toggle'])->name('wishlist.toggle');
    });

    Route::prefix('order')->group(function () {
        Route::get('', [OrderApiController::class, 'index'])->name('order.index');
        Route::post('', [OrderApiController::class, 'save'])->name('order.save');
        Route::post('finish', [OrderApiController::class, 'finish'])->name('order.finish');
    });

    Route::prefix('pick-up-points')->group(function () {
        Route::get('cities', [PickUpPointApiController::class, 'getCities'])->name('pick-up-point.cities');
        Route::get('points', [PickUpPointApiController::class, 'getPoints'])->name('pick-up-point.points');
    });
});

//Route::get('order-payment', [OrderController::class, 'pay'])->name('order.payment');

Route::get('{fullUrl}', [PageController::class, 'index'])
    ->where('fullUrl', '^(?!nova).*')
    ->name('page')
    ->middleware('maintenance');

