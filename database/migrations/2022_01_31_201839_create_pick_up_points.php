<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickUpPoints extends Migration
{
    public function up()
    {
        Schema::create('pick_up_points', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('status')
                ->default(false);

            $table->string('name');

            $table->string('city');
            $table->text('address');
            $table->string('postal_code');
            $table->text('address_full');
            $table->text('address_comment')->nullable();
            $table->text('work_time');

            $table->string('lat');
            $table->string('lng');

            $table->string('type');
            $table->string('code')->nullable();

            $table->float('max_weight')->nullable();

            $table->float('width')->nullable();
            $table->float('height')->nullable();
            $table->float('depth')->nullable();

            $table->unique(['type', 'code']);


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pick_up_points');
    }
}
