<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReview extends Migration
{
    public function up()
    {
        Schema::create('product_review', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->foreignId('review_id')->constrained('reviews')->cascadeOnDelete();
            $table->foreignId('product_id')->constrained('products')->cascadeOnDelete();
            $table->unique(['product_id', 'review_id']);
            $table->integer('priority')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_review');
    }
}
