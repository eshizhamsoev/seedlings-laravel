<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->foreignId('order_id')->nullable()->constrained('orders')->onDelete('cascade');
            $table->string('name');
            $table->json('additional_params')->nullable();
            $table->decimal('price', 20, 2);
            $table->decimal('old_price', 20, 2)->nullable();
            $table->unsignedInteger('count');
            $table->foreignId('product_id')->nullable()->constrained('products')->onDelete('set null');
            $table->nullableMorphs('offer');


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
