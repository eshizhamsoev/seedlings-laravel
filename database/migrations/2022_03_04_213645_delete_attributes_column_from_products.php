<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteAttributesColumnFromProducts extends Migration
{
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('attributes');
        });
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->json('attributes')->nullable();
        });
    }
}
