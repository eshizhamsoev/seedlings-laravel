<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCollections extends Migration
{
    public function up()
    {
        Schema::create('product_collections', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->json('settings')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_collections');
    }
}
