<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToOffersTables extends Migration
{
    public function up()
    {
        Schema::table('simple_offers', function (Blueprint $table) {
            $table->decimal('old_price', 20, 2)->nullable();
            $table->integer('stock_quantity')->default(0);
        });
        Schema::table('named_offers', function (Blueprint $table) {
            $table->decimal('old_price', 20, 2)->nullable();
            $table->integer('stock_quantity')->default(0);
        });
    }

    public function down()
    {
        Schema::table('simple_offers', function (Blueprint $table) {
            $table->dropColumn('old_price');
            $table->dropColumn('stock_quantity');
        });
        Schema::table('named_offers', function (Blueprint $table) {
            $table->dropColumn('old_price');
            $table->dropColumn('stock_quantity');
        });
    }
}
