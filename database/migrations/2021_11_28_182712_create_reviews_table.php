<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('author');
            $table->string('role')->nullable();
            $table->date('date')->nullable();
            $table->unsignedTinyInteger('rating')->nullable();
            $table->mediumText('text')->nullable();
            $table->integer('priority')->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
