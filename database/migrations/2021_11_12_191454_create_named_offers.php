<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNamedOffers extends Migration
{
    public function up()
    {
        Schema::create('named_offers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->decimal('price', 20, 2);

            $table->foreignId('product_id')->constrained('products')->unique()->onDelete('cascade');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('named_offers');
    }
}
