<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewAnswers extends Migration
{
    public function up()
    {
        Schema::create('review_answers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->foreignId('review_id')->constrained('reviews');
            $table->date('date')->nullable();
            $table->mediumText('text')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('review_answers');
    }
}
