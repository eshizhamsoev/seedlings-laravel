<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSeoDataTable extends Migration
{
    public function up()
    {
        Schema::table('seo_data', function (Blueprint $table) {
            $table->string('og_image')->nullable();
        });
    }

    public function down()
    {
        Schema::table('seo_data', function (Blueprint $table) {
            $table->dropColumn('og_image');
        });
    }
}
