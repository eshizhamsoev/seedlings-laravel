<?php

use App\Enums\Website\BlockColor;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorColumnToPageBlocks extends Migration
{
    public function up()
    {
        Schema::table('page_blocks', function (Blueprint $table) {
            $table->string('color')->default( BlockColor::GRAY);
        });
    }

    public function down()
    {
        Schema::table('page_blocks', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
