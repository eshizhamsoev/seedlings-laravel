<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurableBlocks extends Migration
{
    public function up()
    {
        Schema::create('configurable_blocks', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('type');
            $table->string('name');

            $table->json('settings')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('configurable_blocks');
    }
}
