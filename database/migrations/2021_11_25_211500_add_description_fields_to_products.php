<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionFieldsToProducts extends Migration
{
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text('short_description')->nullable();
            $table->mediumText('description')->nullable();
            $table->mediumText('how_to_plant')->nullable();
            $table->mediumText('recommendation')->nullable();
        });
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('short_description');
            $table->dropColumn('description');
            $table->dropColumn('how_to_plant');
            $table->dropColumn('recommendation');
        });
    }
}
