<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerItemsTable extends Migration
{
    public function up()
    {
        Schema::create('banner_items', function (Blueprint $table) {
            $table->id();

            $table->foreignId('banner_id')
                ->constrained()
                ->onDelete('cascade');

            $table->string('name');
            $table->mediumText('description');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('banner_items');
    }
}
