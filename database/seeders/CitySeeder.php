<?php

namespace Database\Seeders;

use App\Models\Data\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = new City();
        $city->name = 'Москва';
        $city->domain = 'seedlings.test';
        $city->save();
    }
}
