<?php
declare(strict_types=1);

use ElasticAdapter\Indices\Mapping;
use ElasticAdapter\Indices\Settings;
use ElasticMigrations\Facades\Index;
use ElasticMigrations\MigrationInterface;

final class CreateProductsIndex implements MigrationInterface
{
    /**
     * Run the migration.
     */
    public function up(): void
    {
        Index::create('products', function (Mapping $mapping, Settings $settings) {
            // to add a new field to the mapping use method name as a field type (in Camel Case),
            // first argument as a field name and optional second argument for additional field parameters
            $mapping->integer('id');
            $mapping->integer('categoryId');
            $mapping->integer('priority');
            $mapping->text('name', ['boost' => 2]);
            $mapping->float('minPrice');

            // you can define a dynamic template as follows
            $mapping->dynamicTemplate('filterAttributes', [
                'path_match' => 'filterAttributes.*',
                'mapping' => [
                    'type' => 'keyword',
                ],
            ]);
            // you can define a dynamic template as follows
            $mapping->dynamicTemplate('searchAttributes', [
                'path_match' => 'searchAttributes.*',
                'mapping' => [
                    'type' => 'text',
                ],
            ]);
//
//            // you can also change the index settings and the analysis configuration
//            $settings->index([
//                'number_of_replicas' => 2,
//                'refresh_interval' => -1
//            ]);
//
            $settings->analysis([
                'filter' => [
                    'russian_stop' => [
                        'type' => 'stop',
                        "stopwords" =>  "_russian_"
                    ],
                    'russian_stemmer' => [
                        'type' => 'stemmer',
                        'language' => 'russian'
                    ]
                ],
                'analyzer' => [
                    'rebuilt_russian' => [
                        'tokenizer' => 'standard',
                        "filter" => [
                            "lowercase",
                            "russian_stop",
                            "russian_stemmer"
                        ]
                    ]
                ]
            ]);
        });
    }

    /**
     * Reverse the migration.
     */
    public function down(): void
    {
        Index::drop('products');
    }
}
