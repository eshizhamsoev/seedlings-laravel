<?php

namespace App\Support;

function createUrl($params, $url = null, $query = null): string
{
    $url = $url ?? request()->getRequestUri();
    $path = parse_url($url, PHP_URL_PATH);
    parse_str(parse_url($url, PHP_URL_QUERY), $query);
    return $path . '?' . http_build_query(array_merge($query, $params));
}

function previousUrl()
{
    $prev = url()->previous();
    if (!$prev) {
        return null;
    }

    $current = url()->current();
    if ($current === $prev) {
        return null;
    }

    $str = \Str::of($prev);
    $hostName = \request()->getSchemeAndHttpHost();
    if (!$str->startsWith($hostName)) {
        return null;
    }
    return str_replace($hostName, '', $prev);
}

function rubles(float $number, bool $signed = true)
{
    $formatted = number_format($number, 2, '.', ' ');
    $text = str_replace(".00", "", $formatted);
    if ($signed) {
        $text .= ' ₽';
    }
    return $text;
}
