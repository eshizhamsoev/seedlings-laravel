<?php

namespace App\ConfigurableBlockSettings;

use App\Models\Data\Product\ProductCollection;
use App\View\Data\Block\ProductGridData;
use App\View\Data\Block\ProductSliderData;
use App\View\Data\Common\LinkData;
use Laravel\Nova\Fields\Field;
use R64\NovaFields\Number;
use R64\NovaFields\Select;
use R64\NovaFields\Text;

class ProductGridBlockSettings implements ConfigurableBlockSettings
{
    private const HEADING_FIELD = 'heading';
    private const COLLECTION_FIELD = 'collection';
    private const LINK_URL_FIELD = 'link-url';
    private const LINK_TEXT_FIELD = 'link-text';
    private const LIMIT = 'limit';

    public function getSettings(): array
    {
        return [
            Text::make('Heading', self::HEADING_FIELD),
            Select::make('Collection', self::COLLECTION_FIELD)
                ->options(ProductCollection::pluck('name', 'id')),
            Text::make('Link url', self::LINK_URL_FIELD),
            Text::make('Link text', self::LINK_TEXT_FIELD),
            Number::make('Limit', self::LIMIT)
        ];
    }

    public function extractSettings(array $data): ?ProductGridData
    {
        $collectionId = $data[self::COLLECTION_FIELD] ?? null;
        $heading = $data[self::HEADING_FIELD] ?? '';
        $collection = ProductCollection::find($collectionId);
        if (!$collection) {
            return null;
        }

        $linkData = null;
        if (isset($data[self::LINK_URL_FIELD]) && isset($data[self::LINK_TEXT_FIELD])) {
            $linkData = new LinkData(
                $data[self::LINK_URL_FIELD],
                $data[self::LINK_TEXT_FIELD]
            );
        }

        $limit = $data[self::LIMIT] ?? 8;

        return new ProductGridData($collection->getSettings(), $heading, $limit, $linkData);
    }
}
