<?php

namespace App\ConfigurableBlockSettings;


use App\Nova\Data\Catalog\Category;
use App\View\Data\Block\FeaturedCategoriesData;
use OptimistDigital\MultiselectField\Multiselect;
use R64\NovaFields\Text;

class FeaturedCategoriesBlockSettings implements ConfigurableBlockSettings
{
    private const CATEGORIES_FIELD = 'categories';
    private const HEADING_FIELD = 'heading';


    public function getSettings(): array
    {
        return [
            Multiselect::make('Categories', self::CATEGORIES_FIELD)
                ->asyncResource(
                    Category::class
                )
                ->reorderable()
                ->nullable(),
            Text::make('Heading', self::HEADING_FIELD)
//            Row::make('Categories', self::CATEGORIES_FIELD, [
//                Select::make('id')
//                    ->options(
//                        Category::all()->pluck('name', 'id')
//                    )
//                    ->nullable(),
//            ])->childConfig([
//                'fieldClasses' => 'w-full px-8 py-6',
//                'hideLabelInForms' => true,
//            ]),
        ];
    }

    public function extractSettings(array $data)
    {

        return new FeaturedCategoriesData(
            json_decode($data[self::CATEGORIES_FIELD] ?? null) ?? [],
            $data[self::HEADING_FIELD] ?? ''
        );
    }

}
