<?php

namespace App\ConfigurableBlockSettings;

class CartBlockSettings implements ConfigurableBlockSettings
{

    public function getSettings(): array
    {
        return [];
    }

    public function extractSettings(?array $data)
    {
        return null;
    }
}
