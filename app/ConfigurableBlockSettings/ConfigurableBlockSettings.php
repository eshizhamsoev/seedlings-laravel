<?php

namespace App\ConfigurableBlockSettings;


use Laravel\Nova\Fields\Field;

interface ConfigurableBlockSettings
{
    /**
     * @return Field[]
     */
    public function getSettings(): array;

    public function extractSettings(array $data);

}
