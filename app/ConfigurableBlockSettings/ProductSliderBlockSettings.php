<?php

namespace App\ConfigurableBlockSettings;

use App\Models\Data\Product\ProductCollection;
use App\View\Data\Block\ProductSliderData;
use Laravel\Nova\Fields\Field;
use R64\NovaFields\Select;
use R64\NovaFields\Text;

class ProductSliderBlockSettings implements ConfigurableBlockSettings
{
    private const HEADING_FIELD = 'heading';
    private const COLLECTION_FIELD = 'collection';

    public function getSettings(): array
    {
        return [
            Text::make('Heading', self::HEADING_FIELD),
            Select::make('Collection', self::COLLECTION_FIELD)
                ->options(ProductCollection::pluck('name', 'id')),
        ];
    }

    public function extractSettings(array $data): ?ProductSliderData
    {
        $collectionId = $data[self::COLLECTION_FIELD] ?? null;
        $heading = $data[self::HEADING_FIELD] ?? '';
        $collection = ProductCollection::find($collectionId);
        if (!$collection) {
            return null;
        }
        return new ProductSliderData($collection->getSettings(), $heading);
    }
}
