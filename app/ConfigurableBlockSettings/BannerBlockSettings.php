<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\Website\BannerTemplate;
use App\Models\Website\Banner\Banner;
use App\View\Data\Block\BannerData;
use R64\NovaFields\Select;
use SimpleSquid\Nova\Fields\Enum\Enum;

class BannerBlockSettings implements ConfigurableBlockSettings
{

    private const BANNER_FIELD = 'banner';
    private const TEMPLATE_FIELD = 'template';

    public function getSettings(): array
    {
        return [
            Select::make('Banner', self::BANNER_FIELD)
                ->options(Banner::pluck('name', 'id')),
            Enum::make('Template', self::TEMPLATE_FIELD)->attach(BannerTemplate::class)
        ];
    }

    public function extractSettings(?array $data): ?BannerData
    {
        if (!isset($data[self::TEMPLATE_FIELD]) || !isset($data[self::BANNER_FIELD])) {
            return null;
        }

        $banner = Banner::find($data[self::BANNER_FIELD]);
        if (!$banner) {
            return null;
        }

        $template = BannerTemplate::fromValue($data[self::TEMPLATE_FIELD]);

        return new BannerData($banner, $template);
    }
}
