<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\Website\BlockType;

class SettingResolver
{
    const CLASSES = [
        BlockType::CATALOG => CatalogBlockSettings::class,
        BlockType::FEATURED_CATEGORIES => FeaturedCategoriesBlockSettings::class,
        BlockType::PRODUCT_SLIDER => ProductSliderBlockSettings::class,
        BlockType::PRODUCT_GRID => ProductGridBlockSettings::class,
        BlockType::REVIEWS => ReviewsBlockSettings::class,
        BlockType::HTML_CODE => HtmlBlockSettings::class,
        BlockType::SUBSCRIBE => SubscribeBlockSettings::class,
        BlockType::CART => CartBlockSettings::class,
        BlockType::CHECKOUT => CheckoutBlockSettings::class,
        BlockType::BLOG => BlogBlockSettings::class,
        BlockType::WISHLIST => WishlistBlockSettings::class,
        BlockType::BANNER => BannerBlockSettings::class,
        BlockType::NEWS_SLIDER => NewsSliderBlockSettings::class
    ];

    public function resolve(BlockType $blockType): ConfigurableBlockSettings
    {
        $class = self::CLASSES[$blockType->value] ?? null;
        if (!$class) {
            throw new \InvalidArgumentException('No block setting class for block type: ' . $blockType->value);
        }
        return new $class;
    }
}
