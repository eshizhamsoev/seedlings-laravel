<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartAddRequest;
use App\Http\Requests\CartUpdateRequest;
use App\Models\Data\Product\Product;
use App\Services\Cart\CartService;
use App\Services\Cart\ItemInfo;
use Cart;
use Gloudemans\Shoppingcart\CartItem;


class CartApiController extends Controller
{
    public function index(CartService $cartService)
    {

        return response()->json([
            'count' => $cartService->getCount(),
            'total' => $cartService->getTotal(),
            'items' => $cartService->getItems()->map(fn(ItemInfo $cartItem) => [
                'name' => $cartItem->getName(),
                'price' => $cartItem->getPrice(),
                'count' => $cartItem->getCount(),
//                'total' => $cartItem->total
            ])
        ]);
    }

    public function add(CartAddRequest $addRequest, CartService $cartService)
    {
        $data = $addRequest->getData();
        $cartService->addProduct($data);
        return response()->json(['success' => true]);
    }

    public function update(CartUpdateRequest $updateRequest, CartService $cartService)
    {
        $validated = $updateRequest->validated();
        $cartService->updateProduct($validated['rowId'], $validated['count']);
        return response()->json(['success' => true]);
    }

    public function getCartInfo(CartService $cartService)
    {
        return response()->json([
            'count' => $cartService->getCount(),
            'total' => $cartService->getTotal(),
//            'totals' => $cartService->getTotals()
            'items' => $cartService->getItems()
                ->map(function (ItemInfo $cartItem) {
                    $product = $cartItem->getProduct();
                    $image = $product->getFirstMedia(Product::IMAGE_COLLECTION_MAIN);
                    return [
                        'key' => $cartItem->getKey(),
                        'name' => $cartItem->getName(),
                        'price' => $cartItem->getPrice(),
                        'oldPrice' => $cartItem->getOldPrice(),
                        'count' => $cartItem->getCount(),
                        'total' => $cartItem->getTotal(),
                        'image' => $image ? [
                            'srcset' => $image->getSrcset(Product::IMAGE_CONVERSION_DETAILS_LARGE),
                            'src' => $image->getUrl(Product::IMAGE_CONVERSION_DETAILS_LARGE)
                        ] : null
                    ];
                })
                ->values()
        ]);
    }
}
