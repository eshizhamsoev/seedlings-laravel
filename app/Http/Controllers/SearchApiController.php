<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductSearchResource;
use App\Services\Catalog\CatalogQuerySettings;
use App\Services\Catalog\CatalogService;
use Illuminate\Http\Request;

class SearchApiController
{
    public function search(
        Request $request,
        CatalogService $catalogService
    ) {
        $settings = new CatalogQuerySettings();
        $searchTerm = $request->input('q');
        if (!$searchTerm || !is_string($searchTerm)) {
            return null;
        }
        $settings->setSearchQuery($searchTerm);
        $query = $catalogService->getQuery($settings);
        $results = $query->size(5)->execute();
        $models = $results->models();
        $models->load('media');
        return response()->json(ProductSearchResource::collection($models));
    }
}
