<?php

namespace App\Http\Controllers;

use App\Enums\Website\ModalType;

class ModalController
{
    public function show(string $type)
    {
        if(!ModalType::hasValue($type)){
            abort(404);
        }
        return view('modal', [
            'componentName' => 'modals.' . $type
        ]);
    }
}
