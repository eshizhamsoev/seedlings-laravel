<?php

namespace App\Http\Controllers;

use App\Enums\Order\DeliveryType;
use App\Enums\Order\PaymentMethod;
use App\Http\Requests\IncompleteOrderRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Response\OrderResponse;
use App\Models\Data\Delivery\PickUpPoint;
use App\Services\Ordering\Data\CustomerData;
use App\Services\Ordering\Data\OrderData;
use App\Services\Ordering\OrderingService;

class OrderApiController
{
    public function index(
        OrderingService $orderingService
    ) {
        $order = $orderingService->getOrder();


        return response(new OrderResponse($order));
    }

    public function save(
        IncompleteOrderRequest $incompleteOrderRequest,
        OrderingService $orderingService
    ) {
        $data = $incompleteOrderRequest->validated();
        $deliveryType = DeliveryType::fromValue($data['deliveryType']);
        $orderData = new OrderData(
            new CustomerData(
                $data['customer']['name'] ?? null,
                $data['customer']['email'] ?? null,
                $data['customer']['phone'] ?? null
            ),
            $deliveryType,
            PaymentMethod::fromValue($data['paymentMethod'] ?? PaymentMethod::ON_PLACE),
        );
        $orderData->setComment($data['comment']);
        $orderData->setAddress($data['address']);
        if (isset($data['pickUpPointId']) && $deliveryType->is(DeliveryType::PICKUP)) {
            $orderData->setPickUpPoint(PickUpPoint::find($data['pickUpPointId']));
        }
        $orderingService->updateData(
            $orderData
        );

        $order = $orderingService->getOrder();


        return response(new OrderResponse($order));
    }

    public function finish(
        OrderRequest $orderRequest,
        OrderingService $orderingService
    ) {
        $data = $orderRequest->validated();
        $orderData = new OrderData(
            new CustomerData(
                $data['customer']['name'] ?? null,
                $data['customer']['email'] ?? null,
                $data['customer']['phone'] ?? null
            ),
            DeliveryType::fromValue($data['deliveryType']),
            PaymentMethod::fromValue($data['paymentMethod']),
        );
        $orderData->setComment($data['comment']);
        $orderData->setAddress($data['address']);
        $orderingService->updateData(
            $orderData
        );
        $orderingService->getOrder();
        $orderId = $orderingService->finish();

        session()->set('orderId', $orderId);

        return response()->json([
            'redirectTo' => route('order.payment')
        ]);
    }
}
