<?php

namespace App\Http\Controllers;

use App\Http\Requests\PickUpPointsRequest;
use App\Http\Resources\PickUpPointResource;
use App\Models\Data\Delivery\PickUpPoint;
use Illuminate\Http\Request;

class PickUpPointApiController extends Controller
{
    public function getCities()
    {
        $cities = PickUpPoint::select('city')->distinct()->orderBy('city')->pluck('city');

        return response()->json($cities);
    }

    public function getPoints(PickUpPointsRequest $request)
    {
        $points = PickUpPoint::where('city', $request->validated()['city'])->get();

        return response()->json(PickUpPointResource::collection($points));
    }
}
