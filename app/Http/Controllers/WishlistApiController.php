<?php

namespace App\Http\Controllers;

use App\Http\Requests\WishlistProductRequest;
use App\Services\Wishlist\WishlistService;

class WishlistApiController extends Controller
{
    public function toggle(
        WishlistProductRequest $addRequest,
        WishlistService $wishlistService
    ) {
        $data = $addRequest->getData();
        if ($data) {
            $wishlistService->toggle($data);
            return response()->json([
                'success' => true,
                'active' => $wishlistService->has($data)
            ]);
        }
    }
    public function remove(
        WishlistProductRequest $addRequest,
        WishlistService $wishlistService
    ) {
        $data = $addRequest->getData();
        if ($data) {
            $wishlistService->remove($data);
            return response()->json([
                'success' => true,
            ]);
        }
    }
}
