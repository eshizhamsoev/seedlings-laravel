<?php

namespace App\Http\Controllers;

use App\Enums\EntityType;
use App\Enums\Website\Settings\CodePosition;
use App\Events\ViewEvent;
use App\Models\Website\Url;
use App\Services\CurrentCityRepository;
use App\Services\CurrentUrlRepository;
use App\Services\SeoDataService\SeoDataService;
use App\Services\Settings\SettingHelper;

class PageController extends Controller
{
    public function index(
        CurrentCityRepository $currentCityRepository,
        CurrentUrlRepository $currentUrlRepository,
        SettingHelper $settingHelper,
        SeoDataService $seoService,
        ?string $fullUrl = null
    ) {
        $url = Url::where('url', '=', '/' . $fullUrl)
            ->whereHas('cities',
                function ($query) use ($currentCityRepository) {
                    return $query->where('id', $currentCityRepository->getCurrentCity()->id);
                })
            ->whereHas('entity')
            ->firstOrFail();

        $currentUrlRepository->init($url);
        event(new ViewEvent($url));
        return view('page',
            [
                'url' => $url,
                'seoData' => $seoService->getSeoData($url),
                'layout' => $url->entity->getLayout(),
                'headCode' => $settingHelper->getCodePosition(CodePosition::HEAD()),
                'startBodyCode' => $settingHelper->getCodePosition(CodePosition::BODY_START()),
                'endBodyCode' => $settingHelper->getCodePosition(CodePosition::BODY_END()),
            ]
        );
    }
}
