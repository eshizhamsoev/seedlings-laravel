<?php

namespace App\Http\Response;

use App\Services\Ordering\Order;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use function App\Support\rubles;

class OrderResponse implements Arrayable
{
    private Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function toArray()
    {
        $orderData = $this->order->getOrderData();
        $deliveryCalculation = $this->order->getDeliveryCalculation();
        $decodedAddress = $this->order->getDecodedAddress();
        $point = $orderData->getPickUpPoint();

        $deliveryInfo = [];

        if ($point) {
            $deliveryInfo[] = [
                'label' => trans('checkout.delivery.pick_point'),
                'value' => $point->name
            ];
        }

        if ($deliveryCalculation) {
            $deliveryInfo[] = [
                'label' => trans('checkout.delivery.terms'),
                'value' => rubles($deliveryCalculation->getPrice())
            ];
            $deliveryInfo[] = [
                'label' => trans('checkout.delivery.price'),
                'value' => $this->getPeriod($deliveryCalculation->getStartDate(), $deliveryCalculation->getEndDate())
            ];
        }

        return [
            'data' => [
                'customer' => [
                    'name' => $orderData->getCustomerData()->getName(),
                    'email' => $orderData->getCustomerData()->getEmail(),
                    'phone' => $orderData->getCustomerData()->getPhone(),
                ],
                'comment' => $orderData->getComment(),
                'deliveryType' => $orderData->getDeliveryType(),
                'address' => $orderData->getAddress(),
                'paymentMethod' => $orderData->getPaymentMethod()->value,
                'pickUpPointId' => $point ? $point->id : null
            ],
            'pickPointCity' => $point ? $point->city : null,
            'total' => $this->order->getTotal(),
            'decodedAddress' => $decodedAddress ? [
                'lat' => $decodedAddress->getGeoLat(),
                'lng' => $decodedAddress->getGeoLng(),
            ] : null,
            'deliveryInfo' => $deliveryInfo
        ];
    }

    public function getPeriod(Carbon $start, Carbon $end)
    {
        if ($start->is($end)) {
            return $start->format('d.m');
        }
        return sprintf('%s - %s', $start->format('d.m'), $end->format('d.m'));
    }
}
