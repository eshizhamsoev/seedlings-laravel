<?php

namespace App\Http\Resources;

use App\Models\Data\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Product
 */
class ProductSearchResource extends JsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'link' => route('page', $this->url->url),
            'img' => $this->getFirstMediaUrl(Product::IMAGE_COLLECTION_MAIN, Product::IMAGE_CONVERSION_DETAILS_LARGE) ?? asset('static/images/common/product.svg')
        ];
    }
}
