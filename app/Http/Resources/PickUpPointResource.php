<?php

namespace App\Http\Resources;

use App\Models\Data\Delivery\PickUpPoint;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @mixin PickUpPoint
 */
class PickUpPointResource extends JsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'city' => $this->city,
            'address' => $this->address,
            'addressComment' => $this->address_comment,
            'workTime' => $this->work_time,
//            'images' => $this->images
        ];
    }
}
