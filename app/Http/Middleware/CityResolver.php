<?php

namespace App\Http\Middleware;

use App\Models\Data\City;
use App\Services\CurrentCityRepository;
use Closure;
use Illuminate\Http\Request;

class CityResolver
{
    private CurrentCityRepository $cityRepository;

    public function __construct(CurrentCityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function handle(Request $request, Closure $next)
    {
        if (!$request->getHost()) {
            abort(404);
        }
        $city = City::where('domain', '=', $request->getHost())->firstOrFail();
        $this->cityRepository->init($city);
        return $next($request);
    }
}
