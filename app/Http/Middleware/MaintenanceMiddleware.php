<?php

namespace App\Http\Middleware;

use App\Services\CurrentCityRepository;
use Closure;
use Illuminate\Http\Request;

class MaintenanceMiddleware
{
    private CurrentCityRepository $currentCityRepository;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
    }

    public function handle(Request $request, Closure $next)
    {
        $city = $this->currentCityRepository->getCurrentCity();
        if($city->maintenance_mode && !$request->user()){
            return abort(503);
        }
        return $next($request);
    }
}
