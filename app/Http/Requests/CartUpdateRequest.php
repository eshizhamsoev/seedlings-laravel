<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'rowId' => [
                'required',
                'string'
            ],
            'count' => [
                'required',
                'integer'
            ]
        ];
    }
}
