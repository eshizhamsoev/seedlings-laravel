<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PickUpPointsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'city' => [
                'required',
                'string'
            ]
        ];
    }
}
