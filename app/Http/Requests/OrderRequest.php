<?php

namespace App\Http\Requests;

use App\Enums\Order\DeliveryType;
use App\Enums\Order\PaymentMethod;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'customer.name' => [
                'required',
                'string',
                'max:100'
            ],

            'customer.email' => [
                'required',
                'email',
                'string',
                'max:100'
            ],

            'customer.phone' => [
                'required',
                'string',
                'regex:/\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}/'
            ],

            'comment' => [
                'nullable',
                'string',
                'max:500'
            ],

            'deliveryType' => [
                'required',
                new EnumValue(DeliveryType::class)
            ],

            'address' => [
                'required_if:deliveryType,' . DeliveryType::TO_DOOR,
                'string',
                'max:600'
            ],

            'pickUpPoint' => [
                'required_if:deliveryType,' . DeliveryType::PICKUP,
                'integer'
            ],

            'paymentMethod' => [
                'required',
                new EnumValue(PaymentMethod::class)
            ],
        ];
    }
}
