<?php

namespace App\Http\Requests;

use App\Enums\Catalog\OfferType;
use App\Models\Data\Product\Offers\SimpleOffer;
use App\Models\Data\Product\Product;
use App\Models\Data\Product\ProductTypes\VariantProduct;
use App\Services\Cart\RequestToAddData;
use BenSampo\Enum\Rules\EnumValue;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Foundation\Http\FormRequest;

class WishlistProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                'integer'
            ],
        ];
    }

    public function getData(): ?Product
    {
        $data = $this->validated();

        return Product::find($data['id']);
    }
}
