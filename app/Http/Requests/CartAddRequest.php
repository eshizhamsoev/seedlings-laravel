<?php

namespace App\Http\Requests;

use App\Enums\Catalog\OfferType;
use App\Models\Data\Product\Offers\SimpleOffer;
use App\Models\Data\Product\ProductTypes\VariantProduct;
use App\Services\Cart\RequestToAddData;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class CartAddRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'product' => ['required', 'array'],
            'product.type' => [
                'required',
                new EnumValue(OfferType::class)
            ],
            'product.id' => [
                'required',
                'integer'
            ],
            'count' => [
                'required',
                'integer'
            ]
        ];
    }

    public function getData(): ?RequestToAddData
    {
        $data = $this->validated();
        $productData = $data['product'];

        $buyable = null;
        $id = $productData['id'];
        switch ($productData['type']) {
            case OfferType::SIMPLE:
                $buyable = SimpleOffer::find($id);
                break;
            case OfferType::VARIANT:
                $buyable = VariantProduct::find($id);
                break;
        }
        if (!$buyable) {
            return null;
        }
        return new RequestToAddData($buyable, $data['count']);
    }
}
