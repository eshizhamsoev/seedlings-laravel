<?php

namespace App\Http\Requests;

use App\Enums\Order\DeliveryType;
use App\Enums\Order\PaymentMethod;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class IncompleteOrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'customer.name' => [
                'nullable',
                'string',
                'max:100'
            ],

            'customer.email' => [
                'nullable',
                'email',
                'string',
                'max:100'
            ],

            'customer.phone' => [
                'nullable',
                'string',
                'regex:/\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}/'
            ],

            'comment' => [
                'nullable',
                'string',
                'max:500'
            ],

            'deliveryType' => [
                'required',
                new EnumValue(DeliveryType::class)
            ],

            'address' => [
                'nullable',
                'string',
                'max:600'
            ],

            'pickUpPointId' => [
                'nullable',
                'integer'
            ],

            'paymentMethod' => [
                'nullable',
                new EnumValue(PaymentMethod::class)
            ],
        ];
    }
}
