<?php

namespace App\Console\Commands;

use App\Services\CdekPickUpSynchronizer\CdekSynchronizer;
use Illuminate\Console\Command;

class CdekSyncPickUpPointsCommand extends Command
{
    protected $signature = 'cdek:sync-pick-up-points';

    protected $description = 'Synchronize cdek pick up points';

    public function handle(CdekSynchronizer $cdekSynchronizer)
    {
        $cdekSynchronizer->synchronize();
    }
}
