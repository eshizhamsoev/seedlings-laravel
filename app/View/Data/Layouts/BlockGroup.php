<?php

namespace App\View\Data\Layouts;

use App\Enums\Website\BlockColor;

class BlockGroup
{
    private array $blocks = [];
    private BlockColor $color;

    public function __construct(BlockColor $color)
    {
        $this->color = $color;
    }

    /**
     * @return array
     */
    public function getBlocks(): array
    {
        return $this->blocks;
    }

    /**
     * @return BlockColor
     */
    public function getColor(): BlockColor
    {
        return $this->color;
    }

    public function addBlock(ResolvedBlock $block)
    {
        $this->blocks[] = $block;
    }
}
