<?php

namespace App\View\Data\Layouts;

class ResolvedBlock
{
    private string $componentName;
    private $data;

    public function __construct(string $componentName, $data)
    {
        $this->componentName = $componentName;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return $this->componentName;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }


}
