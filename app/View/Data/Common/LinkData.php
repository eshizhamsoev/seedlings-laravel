<?php

namespace App\View\Data\Common;

class LinkData
{
    private string $url;
    private string $text;

    public function __construct(string $url, string $text)
    {
        $this->url = $url;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }
}
