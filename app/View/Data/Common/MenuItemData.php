<?php

namespace App\View\Data\Common;

class MenuItemData
{
    public const LINK = 'link';
    public const NAME = 'name';
    private string $link;
    private string $name;

    public function __construct(string $link, string $name)
    {
        $this->link = $link;
        $this->name = $name;
    }

    static public function fromArray(array $data): ?self
    {
        if(!$data[self::LINK] || !$data[self::NAME]){
            return null;
        }
        return new self($data[self::LINK], $data[self::NAME]);
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
