<?php

namespace App\View\Data\Block;

class RecentlyWatchedData
{
    private ?int $limit;
    private ?int $excludedId;

    public function __construct(?int $limit = null, ?int $excludedId = null)
    {
        $this->limit = $limit;
        $this->excludedId = $excludedId;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param  int|null  $limit
     */
    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getExcludedId(): ?int
    {
        return $this->excludedId;
    }

    /**
     * @param  int|null  $excludedId
     */
    public function setExcludedId(?int $excludedId): void
    {
        $this->excludedId = $excludedId;
    }

}
