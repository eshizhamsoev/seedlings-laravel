<?php

namespace App\View\Data\Block;

use App\Services\Catalog\CatalogQuerySettings;
use App\View\Data\Common\LinkData;

class ProductGridData
{
    private string $heading;
    private ?LinkData $linkData;
    private CatalogQuerySettings $catalogQuerySettings;
    private int $limit;

    public function __construct(
        CatalogQuerySettings $catalogQuerySettings,
        string $heading,
        int $limit,
        ?LinkData $linkData = null
    ) {
        $this->heading = $heading;
        $this->linkData = $linkData;
        $this->catalogQuerySettings = $catalogQuerySettings;
        $this->limit = $limit;
    }

    /**
     * @return CatalogQuerySettings
     */
    public function getCatalogQuerySettings(): CatalogQuerySettings
    {
        return $this->catalogQuerySettings;
    }

    /**
     * @return string
     */
    public function getHeading(): string
    {
        return $this->heading;
    }

    /**
     * @return LinkData|null
     */
    public function getLinkData(): ?LinkData
    {
        return $this->linkData;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

}
