<?php

namespace App\View\Data\Block;

class HtmlData
{
    private string $htmlCode;

    public function __construct(string $htmlCode)
    {
        $this->htmlCode = $htmlCode;
    }

    /**
     * @return string
     */
    public function getHtmlCode(): string
    {
        return $this->htmlCode;
    }

}
