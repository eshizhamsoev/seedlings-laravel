<?php

namespace App\View\Data\Block;

class FeaturedCategoriesData
{
    private array $categoryIds;
    private string $heading;

    public function __construct(array $categoryIds, string $heading)
    {
        $this->categoryIds = $categoryIds;
        $this->heading = $heading;
    }

    /**
     * @return int[]
     */
    public function getCategoryIds(): array
    {
        return $this->categoryIds;
    }

    /**
     * @return string
     */
    public function getHeading(): string
    {
        return $this->heading;
    }

}
