<?php

namespace App\View\Data\Block;

use App\Services\Catalog\CatalogQuerySettings;

class ProductSliderData
{
    private string $heading;
    private CatalogQuerySettings $catalogQuerySettings;

    public function __construct(
        CatalogQuerySettings $catalogQuerySettings,
        string $heading
    ) {
        $this->heading = $heading;
        $this->catalogQuerySettings = $catalogQuerySettings;
    }

    /**
     * @return string
     */
    public function getHeading(): string
    {
        return $this->heading;
    }

    /**
     * @return CatalogQuerySettings
     */
    public function getCatalogQuerySettings(): CatalogQuerySettings
    {
        return $this->catalogQuerySettings;
    }
}
