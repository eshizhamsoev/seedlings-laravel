<?php

namespace App\View\Data\Block\CatalogFilter;

use App\Enums\Website\SvgIconName;

class CatalogOrderItem
{
    private string $link;
    private string $name;
    private bool $isActive;
    private bool $isDescending;

    public function __construct(string $link, string $name, bool $isDescending = false, bool $isActive = false)
    {
        $this->link = $link;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->isDescending = $isDescending;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isDescending(): bool
    {
        return $this->isDescending;
    }


}
