<?php

namespace App\View\Data\Block;

use App\Enums\Website\BannerTemplate;
use App\Models\Website\Banner\Banner;

class BannerData
{
    private Banner $banner;
    private BannerTemplate $bannerTemplate;

    public function __construct(Banner $banner, BannerTemplate $bannerTemplate)
    {
        $this->banner = $banner;
        $this->bannerTemplate = $bannerTemplate;
    }

    /**
     * @return Banner
     */
    public function getBanner(): Banner
    {
        return $this->banner;
    }

    /**
     * @return BannerTemplate
     */
    public function getBannerTemplate(): BannerTemplate
    {
        return $this->bannerTemplate;
    }
}
