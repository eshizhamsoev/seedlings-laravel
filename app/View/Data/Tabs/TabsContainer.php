<?php

namespace App\View\Data\Tabs;

class TabsContainer
{
    /**
     * @var Tab[]
     */
    private array $tabs = [];
    private string $baseId;
    private bool $hasActive = false;

    public function __construct(string $baseId)
    {
        $this->baseId = $baseId;
    }

    public function getTabs()
    {
        return $this->tabs;
    }

    public function addTab(string $heading, bool $isActive = false)
    {
        $id = $this->baseId . '-' . (count($this->tabs) + 1);
        $this->tabs[] = new Tab($heading, $id, $isActive && !$this->hasActive);
        if ($isActive) {
            $this->hasActive = true;
        }
        return $id;
    }
}
