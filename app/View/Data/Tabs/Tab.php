<?php

namespace App\View\Data\Tabs;

class Tab
{
    private string $heading;
    private string $id;
    private bool $isActive;

    public function __construct(string $heading, string $id, bool $isActive = false)
    {
        $this->heading = $heading;
        $this->id = $id;
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getHeading(): string
    {
        return $this->heading;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

}
