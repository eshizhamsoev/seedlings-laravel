<?php

namespace App\View\Data\Modals\AddToCart;

use App\Models\Data\Product\Product;
use App\Services\Cart\ItemInfo;

class CartItemInfo
{
    private ItemInfo $cartItem;

    public function __construct(ItemInfo $cartItem)
    {
        $this->cartItem = $cartItem;
    }

    public function getName()
    {
        return $this->cartItem->getName();
    }

    public function getPrice()
    {
        return $this->cartItem->getPrice();
    }

    public function getCount()
    {
        return $this->cartItem->getCount();
    }

    public function getLink()
    {
        return $this->cartItem->getProduct()->url;
    }

    public function getImage()
    {
        return $this->cartItem
            ->getProduct()
            ->getFirstMedia(Product::IMAGE_COLLECTION_MAIN);
    }
}
