<?php

namespace App\View\Components\Common;

use App\Enums\Website\InformerImage;
use Illuminate\View\Component;

class Informer extends Component
{
    private InformerImage $image;
    private bool $isGrey;
    private string $title;
    private string $subTitle;

    public function __construct(
        InformerImage $image,
        string $title,
        string $subTitle,
        bool $isGrey = false
    ) {
        $this->image = $image;
        $this->isGrey = $isGrey;
        $this->title = $title;
        $this->subTitle = $subTitle;
    }

    public function render()
    {
        return view('components.common.informer', [
            'image' => $this->image->getImage(),
            'title' => $this->title,
            'subTitle'=> $this->subTitle,
            'isGrey' => $this->isGrey
        ]);
    }
}
