<?php

namespace App\View\Components\Common;

use App\Enums\Website\Settings\Social;
use Illuminate\View\Component;

class OurSocials extends Component
{
    private bool $isWhite;

    public function __construct(
        bool $isWhite = false
    ) {
        $this->isWhite = $isWhite;
    }

    public function render()
    {
        $socials = nova_get_settings(Social::getValues());
        $links = [];
        foreach ($socials as $key => $link) {
            if (!$link) {
                continue;
            }
            $type = Social::fromValue($key);
            $links[] = [
                'title' => $type->description,
                'href' => $link,
                'icon' => $type->getIcon()
            ];
        }
        return view('components.common.our-socials',
            [
                'isWhite' => $this->isWhite,
                'links' => $links
            ]);
    }
}
