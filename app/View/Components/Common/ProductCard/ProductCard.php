<?php

namespace App\View\Components\Common\ProductCard;

use App\Enums\Catalog\ProductsView;
use App\Models\Data\Product\Product;
use Illuminate\View\Component;

class ProductCard extends Component
{
    private Product $product;
    private ProductsView $productsView;

    public function __construct(Product $product, ProductsView $productsView)
    {
        $this->product = $product;
        $this->productsView = $productsView;
    }

    public function render()
    {
        return view( $this->productsView->is(ProductsView::GRID) ? 'components.common.product-card.product-card' : 'components.common.product-card.product-card-row', [
            'url' => $this->product->url,
            'image' => $this->product->getFirstMedia(Product::IMAGE_COLLECTION_MAIN),
            'conversionName' => Product::IMAGE_CONVERSION_DETAILS_LARGE,
            'label' => $this->product->label,
            'offer' => $this->product->offer,
            'name' => $this->product->name,
            'product' => $this->product
        ]);
    }
}
