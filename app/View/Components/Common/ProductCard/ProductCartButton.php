<?php

namespace App\View\Components\Common\ProductCard;

use App\Models\Data\Product\Product;
use App\Models\Data\Product\ProductTypes\SimpleProduct;
use Illuminate\View\Component;

class ProductCartButton extends Component
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function render()
    {
        $offer = null;
        if ($this->product instanceof SimpleProduct) {
            $offer = $this->product->offer;
        }
        return view('components.common.product-card.product-cart-button', [
            'name' => $this->product->name,
            'id' => $this->product->id,
            'type' => $this->product->type,
            'offerId' => $offer ? $offer->id : null
        ]);
    }
}
