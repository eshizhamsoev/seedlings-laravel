<?php

namespace App\View\Components\Common;

use App\Models\Data\Category\Category;
use Illuminate\View\Component;

class CatalogItem extends Component
{
    private Category $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function render()
    {
        return view('components.common.catalog-item', [
            'category' => $this->category,
            'media' => $this->category->getFirstMedia(Category::IMAGE_COLLECTION_MAIN)
        ]);
    }
}
