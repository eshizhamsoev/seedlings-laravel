<?php

namespace App\View\Components\Common;

use Illuminate\View\Component;
use function App\Support\previousUrl;

class BackButton extends Component
{
    public function render()
    {
        return view('components.common.back-button', [
            'previous' => previousUrl()
        ]);
    }
}
