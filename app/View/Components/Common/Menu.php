<?php

namespace App\View\Components\Common;

use App\View\Data\Common\MenuItemData;
use Illuminate\View\Component;

class Menu extends Component
{
    private \App\Enums\Website\Settings\Menu $menu;
    private string $blockName;

    public function __construct(\App\Enums\Website\Settings\Menu $menu, string $blockName)
    {
        $this->menu = $menu;
        $this->blockName = $blockName;
    }

    public function render()
    {
        $menuItems = nova_get_setting($this->menu->value);
        $links = [];
        if ($menuItems) {
            foreach ($menuItems as $menuItem) {
                $link = MenuItemData::fromArray($menuItem);
                if ($link) {
                    $links[] = $link;
                }
            }
        }
        return view('components.common.menu', [
            'links' => $links,
            'blockName' => $this->blockName
        ]);
    }
}
