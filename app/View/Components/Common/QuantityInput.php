<?php

namespace App\View\Components\Common;

use Illuminate\View\Component;

class QuantityInput extends Component
{
    private string $fieldName;
    private string $label;
    private int $step;
    private ?int $max;
    private ?int $min;
    private ?int $value;

    public function __construct(
        string $fieldName,
        string $label,
        int $step = 1,
        ?int $max = null,
        ?int $min = null,
        ?int $value = null
    ) {
        $this->fieldName = $fieldName;
        $this->label = $label;
        $this->step = $step;
        $this->max = $max;
        $this->min = $min;
        $this->value = $value;
    }

    public function render()
    {
        return view('components.common.quantity-input', [
            'fieldName' => $this->fieldName,
            'label' => $this->label,
            'step' => $this->step,
            'min' => $this->min ?? $this->step,
            'max' => $this->max,
            'value' => $this->value ?? $this->min ?? $this->step,
        ]);
    }
}
