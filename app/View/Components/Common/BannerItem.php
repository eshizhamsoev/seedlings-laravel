<?php

namespace App\View\Components\Common;

use App\Models\Website\Banner\BannerItem as BannerItemModel;
use App\Models\Website\Banner\DimensionData;
use Illuminate\View\Component;
use Spatie\MediaLibrary\Conversions\ConversionCollection;
use Spatie\MediaLibrary\Conversions\ImageGenerators\Image;

class BannerItem extends Component
{
    static array $mediaClasses = [];
    static bool $scriptRegistered = false;
    private BannerItemModel $bannerItem;
    /**
     * @var DimensionData[]
     */
    private array $dimensions;
    private bool $lazy;

    public function __construct(BannerItemModel $bannerItem, array $dimensions, bool $lazy = true)
    {
        $this->bannerItem = $bannerItem;
        $this->dimensions = $dimensions;
        $this->lazy = $lazy;
    }

    public function render()
    {
        $images = [];

        $showTo = null;

        /** @var DimensionData[] $dimensions */
        $dimensions = collect($this->dimensions)
            ->sort(fn(
                DimensionData $a,
                DimensionData $b
            ) => $a->getMinimumScreenWidth() - $b->getMinimumScreenWidth())->reverse();

        $mediaStyles = [];
        $imageMediaClasses = [];
        foreach ($dimensions as $dimension) {
            $media = $this->bannerItem->getFirstMedia($dimension->getName());
            $srcSet = $media->getSrcset($dimension->getName());
            $showFrom = $dimension->getMinimumScreenWidth();
            $images[] = [
                'src' => $media->getUrl($dimension->getName()),
                'srcset' => $srcSet,
                'width' => $dimension->getWidth(),
                'height' => $dimension->getHeight(),
                'showFrom' => $showFrom,
                'showTo' => $showTo,
            ];
            $mediaClassName = "media_{$showFrom}-{$showTo}_{$dimension->getWidth()}-{$dimension->getHeight()}";
            $imageMediaClasses[] = $mediaClassName;
            if(!in_array($mediaClassName, self::$mediaClasses)) {
                $mediaStyles[] = [
                    'name' => $mediaClassName,
                    'from' => $showFrom,
                    'to' => $showTo,
                    'width' => $dimension->getWidth(),
                    'height' => $dimension->getHeight()
                ];
                self::$mediaClasses[] = $mediaClassName;
            }
            $showTo = $dimension->getMinimumScreenWidth() - 1;
        }

        $scriptRegistered = self::$scriptRegistered;

        self::$scriptRegistered = true;


        return view('components.common.banner-item', [
            'images' => $images,
            'defaultImage' => $images[0],
            'mediaStyles' => $mediaStyles,
            'mediaClasses' => $imageMediaClasses,
            'scriptRegistered' => $scriptRegistered,
            'lazy' => $this->lazy
        ]);
    }
}
