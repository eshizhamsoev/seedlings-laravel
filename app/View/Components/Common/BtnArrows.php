<?php

namespace App\View\Components\Common;

use Illuminate\View\Component;

class BtnArrows extends Component
{
    public function render()
    {
        return view('components.common.btn-arrows');
    }
}
