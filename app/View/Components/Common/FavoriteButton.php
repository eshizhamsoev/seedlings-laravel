<?php

namespace App\View\Components\Common;

use App\Models\Data\Product\Product;
use App\Services\Wishlist\WishlistService;
use Illuminate\View\Component;

class FavoriteButton extends Component
{
    private Product $product;
    private WishlistService $wishlistService;

    public function __construct(Product $product, WishlistService $wishlistService)
    {
        $this->product = $product;
        $this->wishlistService = $wishlistService;
    }

    public function render()
    {
        $name = $this->product->name;
        return view('components.common.favorite-button', [
            'name' => $this->product->name,
            'id' => $this->product->id,
            'added' => $this->wishlistService->has($this->product),
            'textAdd' => 'Добавить ' . $name . ' в избранное',
            'textRemove' => 'Удалить ' . $name . ' из избранного'
        ]);
    }
}
