<?php

namespace App\View\Components\Common;

use App\Models\Data\Article;
use Illuminate\View\Component;

class ArticleCard extends Component
{
    private Article $article;
    private bool $isSlide;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Article $article, bool $isSlide = false)
    {
        $this->article = $article;
        $this->isSlide = $isSlide;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.article-card', [
            'url' => $this->article->url,
            'title' => $this->article->title,
            'description' => $this->article->preview_description,
            'image' => $this->article->getFirstMedia(Article::IMAGE_COLLECTION_MAIN),
            'conversion' => $this->isSlide ? Article::IMAGE_CONVERSION_SLIDE : Article::IMAGE_CONVERSION_CARD,
            'isSlide' => $this->isSlide
        ]);
    }
}
