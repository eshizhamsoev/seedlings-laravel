<?php

namespace App\View\Components\Common;

use App\Services\CurrentCityRepository;
use Illuminate\View\Component;
use function view;

class YourTown extends Component
{
    private CurrentCityRepository $currentCityRepository;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        return view('components.common.your-town', ['city' => $this->currentCityRepository->getCurrentCity()]);
    }
}
