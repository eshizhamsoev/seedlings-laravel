<?php

namespace App\View\Components\Common;

use App\Models\Data\Review\Review as ReviewModel;
use Illuminate\View\Component;

class Review extends Component
{
    private ReviewModel $review;
    private bool $small;

    public function __construct(ReviewModel $review, bool $small = false)
    {
        $this->review = $review;
        $this->small = $small;
    }

    public function render()
    {
        return view('components.common.review', [
            'review' => $this->review,
            'small' => $this->small,
            'image' => $this->review->getFirstMedia(ReviewModel::IMAGE_AUTHOR_COLLECTION),
            'logo' => asset('/static/images/common/logo.svg')
        ]);
    }
}
