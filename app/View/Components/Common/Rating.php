<?php

namespace App\View\Components\Common;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;

class Rating extends Component
{
    private ?int $value;

    public function __construct(?int $value)
    {
        $this->value = $value;
    }

    public function render()
    {
        return view('components.common.rating', [
            'value' => $this->value,
            'icon' => SvgIconName::STAR()
        ]);
    }
}
