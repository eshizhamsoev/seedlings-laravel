<?php

namespace App\View\Components\Base;

use App\Enums\Website\LinkTarget;
use App\Models\Website\Page;
use App\Models\Website\Url;
use Illuminate\View\Component;

class Link extends Component
{
    private ?string $href;
    private ?string $currentLinkClass;
    private ?LinkTarget $target;

    /**
     * @param ?Page $page
     * @param ?Url $url
     * @param  string|null  $href
     * @param  string|null  $currentLinkClass
     * @param ?LinkTarget $target
     * @noinspection PhpMissingParamTypeInspection because laravel auto DI starts making bad things
     */
    public function __construct(
        $page = null,
        $url = null,
        ?string $href = null,
        ?string $currentLinkClass = null,
        $target = null
    ) {
        $this->href = $this->resolveHref($page, $url, $href);
        $this->currentLinkClass = $currentLinkClass;
        $this->target = $target;
    }

    private function resolveHref(
        ?Page $page = null,
        ?Url $url = null,
        ?string $href = null
    ): ?string {
        if ($page) {
            $url = $page->url;
        }
        if ($url) {
            $href = $url->url;
        }
        return $href;
    }

    public function render()
    {

        if($this->target){
            $target = $this->target;
        }else{
            $target = $this->href && str_contains($this->href, 'http') ? LinkTarget::BLANK() : LinkTarget::SELF();
        }

        $currentPathWithQuery = request()->getRequestUri();

        $isCurrent = is_null($this->href) || $this->href === $currentPathWithQuery;

        return view('components.base.link', [
            'href' => $this->href,
            'target' => $target,
            'isCurrent' => $isCurrent,
            'currentLinkClass' => $this->currentLinkClass
        ]);
    }
}
