<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class YoutubeLink extends Component
{
    private string $code;

    public function __construct(string $code)
    {
        $this->code = $code;
    }

    public function render()
    {
        return view('components.base.youtube-link', [
            'link' => sprintf('https://www.youtube.com/embed/%s', $this->code)
        ]);
    }
}
