<?php

namespace App\View\Components\Base;

use App\Enums\Website\ModalType;
use Illuminate\View\Component;

class ModalOpener extends Component
{
    private ModalType $modalType;

    public function __construct(ModalType $modalType)
    {
        $this->modalType = $modalType;
    }

    public function render()
    {
        return view('components.base.modal-opener', [
            'type' => $this->modalType->value
        ]);
    }
}
