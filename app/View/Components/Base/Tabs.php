<?php

namespace App\View\Components\Base;

use App\View\Data\Tabs\TabsContainer;
use Illuminate\View\Component;

class Tabs extends Component
{
    private static $id = 0;
    public TabsContainer $tabsContainer;

    public function __construct()
    {
        self::$id += 1;
        $this->tabsContainer = new TabsContainer('tabs-' . self::$id);
    }

    public function render()
    {
        return view('components.base.tabs', [
            'tabsContainer' => $this->tabsContainer
        ]);
    }
}
