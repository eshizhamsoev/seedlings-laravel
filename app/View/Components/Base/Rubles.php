<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;
use function App\Support\rubles;

class Rubles extends Component
{
    private float $number;

    public function __construct(float $number)
    {
        $this->number = $number;
    }

    public function render()
    {
        return view('components.base.rubles', [
            'value' => rubles($this->number)
        ]);
    }
}
