<?php

namespace App\View\Components\Base;

use App\Enums\Website\Forms\CheckingFieldType;
use Illuminate\View\Component;

class CheckingInputField extends Component
{
    private string $name;
    private string $label;
    private CheckingFieldType $type;
    private string $value;
    private bool $isChecked;
    private bool $isDisabled;

    public function __construct(
        string $name,
        string $label = '',
        CheckingFieldType $type,
        string $value = '1',
        bool $isChecked = false,
        bool $isDisabled = false
    ) {
        $this->name = $name;
        $this->label = $label;
        $this->type = $type;
        $this->value = $value;
        $this->isChecked = $isChecked;
        $this->isDisabled = $isDisabled;
    }

    public function render()
    {
        return view('components.base.checking-input-field', [
            'name' => $this->name,
            'label' => $this->label,
            'type' => $this->type,
            'value' => $this->value,
            'isChecked' => $this->isChecked,
            'isDisabled' => $this->isDisabled
        ]);
    }
}
