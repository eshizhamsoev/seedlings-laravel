<?php

namespace App\View\Components\Base;

use App\Enums\Website\Forms\TextFieldType;
use Illuminate\View\Component;

class TextField extends Component
{
    private string $label;
    private string $name;
    private ?string $value;
    private TextFieldType $type;
    private ?string $placeholder;
    private bool $isDisabled;

    public function __construct(
        string $label,
        string $name,
        TextFieldType $type,
        string $value = null,
        string $placeholder = null,
        bool $isDisabled = false
    ) {
        $this->label = $label;
        $this->name = $name;
        $this->value = $value;
        $this->type = $type;
        $this->placeholder = $placeholder;
        $this->isDisabled = $isDisabled;
    }

    public function render()
    {
        return view('components.base.text-field', [
            'name' => $this->name,
            'value' => $this->value,
            'label' => $this->label,
            'placeholder' => $this->placeholder ?? $this->label,
            'type' => $this->type,
            'isDisabled' => $this->isDisabled
        ]);
    }
}
