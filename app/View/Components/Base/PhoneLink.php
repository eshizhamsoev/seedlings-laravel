<?php

namespace App\View\Components\Base;

use App\Services\PhoneFormatter\PhoneFormatter;
use App\Services\PhoneFormatter\PhoneNumber;
use Illuminate\View\Component;

class PhoneLink extends Component
{
    private string $phone;
    private PhoneFormatter $phoneFormatter;

    public function __construct(PhoneFormatter $phoneFormatter, string $phone)
    {
        $this->phone = $phone;
        $this->phoneFormatter = $phoneFormatter;
    }

    public function render()
    {
        $number = new PhoneNumber($this->phone);
        $formattedPhone = $this->phoneFormatter->format($number);


        return view('components.base.phone-link', [
            'phoneNumber' => $number->getInternationalNumber(),
            'phoneFormatted' => $formattedPhone
        ]);
    }

}
