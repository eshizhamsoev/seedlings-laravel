<?php

namespace App\View\Components\Modals;

use App\Models\Data\City;
use App\Services\CurrentCityRepository;
use Illuminate\View\Component;

class CityList extends Component
{
    private CurrentCityRepository $currentCityRepository;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        $cities = City::whereNotNull('url')->get();
        $currentCity = $this->currentCityRepository->getCurrentCity();
        return view('components.modals.city-list', [
            'cities' => $cities,
            'currentCity' => $currentCity
        ]);
    }
}
