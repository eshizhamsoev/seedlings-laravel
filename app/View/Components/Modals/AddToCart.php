<?php

namespace App\View\Components\Modals;

use App\Enums\Website\Settings\SpecialPage;
use App\Services\Cart\CartService;
use App\Services\Cart\ItemInfo;
use App\Services\Settings\SettingHelper;
use App\View\Components\Block\Cart;
use App\View\Data\Modals\AddToCart\CartItemInfo;
use Illuminate\View\Component;

class AddToCart extends Component
{
    private CartService $cartService;
    private SettingHelper $settingHelper;

    public function __construct(CartService $cartService, SettingHelper $settingHelper)
    {
        $this->cartService = $cartService;
        $this->settingHelper = $settingHelper;
    }

    public function render()
    {
        return view('components.modals.add-to-cart', [
            'count' => $this->cartService->getCount(),
            'total' => $this->cartService->getTotal(),
            'items' => $this->cartService
                ->getItems()
                ->map(fn(ItemInfo $item) => new CartItemInfo($item)),
            'cartUrl' => $this->settingHelper->getPage(SpecialPage::CART())->url
        ]);
    }
}
