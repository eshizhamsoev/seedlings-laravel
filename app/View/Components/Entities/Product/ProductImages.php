<?php

namespace App\View\Components\Entities\Product;

use App\Models\Data\Product\Product;
use Illuminate\View\Component;

class ProductImages extends Component
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function render()
    {
        return view('components.entities.product.product-images', [
            'gallery' => $this->product->getMedia(Product::IMAGE_COLLECTION_MAIN)
        ]);
    }
}
