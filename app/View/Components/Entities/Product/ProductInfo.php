<?php

namespace App\View\Components\Entities\Product;

use App\Models\Data\Product\Product;
use Illuminate\View\Component;

class ProductInfo extends Component
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function render()
    {
        $offer = $this->product->offer;
        return view('components.entities.product.product-info', [
            'heading' => $this->product->name,
            'product' => $this->product,
            'offer' => $offer,
            'profit' => $offer ? $offer->getSaleProfit() : null
        ]);
    }
}
