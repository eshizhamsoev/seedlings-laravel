<?php

namespace App\View\Components\Entities\Product;

use App\Models\Data\Product\Product;
use Illuminate\View\Component;

class ProductSpecification extends Component
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function render()
    {
        return view('components.entities.product.product-specification', [
            'lines' => $this->product->attributes
        ]);
    }
}
