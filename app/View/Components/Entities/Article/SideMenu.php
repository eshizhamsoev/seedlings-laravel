<?php

namespace App\View\Components\Entities\Article;

use App\Models\Data\Article;
use Illuminate\View\Component;

class SideMenu extends Component
{
    /**
     * @var Article[]
     */
    private array $articles;
    private ?int $activeId;
    private ?string $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(array $articles, ?int $activeId = null, ?string $title = null)
    {
        $this->articles = $articles;
        $this->activeId = $activeId;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.entities.article.side-menu', [
            'articles' => $this->articles,
            'activeId' => $this->activeId,
            'title' => $this->title
        ]);
    }
}
