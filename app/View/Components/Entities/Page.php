<?php


namespace App\View\Components\Entities;


use App\Models\Website\Page as PageModel;
use Illuminate\View\Component;

class Page extends Component
{
    private PageModel $page;

    public function __construct(PageModel $page)
    {
        $this->page = $page;
    }

    public function render()
    {
        return view('components.entities.page', [
            'page' => $this->page
        ]);
    }
}
