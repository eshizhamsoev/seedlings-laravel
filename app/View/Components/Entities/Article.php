<?php

namespace App\View\Components\Entities;

use App\Models\Data\Article as ArticleModel;
use Illuminate\View\Component;

class Article extends Component
{
    private ArticleModel $article;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(ArticleModel $article)
    {
        $this->article = $article;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {

        $otherArticles = ArticleModel::orderBy('publication_date', 'desc')->limit(10)->get();

        return view('components.entities.article.article', [
            'article' => $this->article,
            'otherArticles' => $otherArticles->all()
        ]);
    }
}
