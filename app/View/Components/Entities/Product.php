<?php

namespace App\View\Components\Entities;

use App\Models\Data\Product\Product as ProductModel;
use App\Services\Catalog\CatalogQuerySettings;
use App\View\Data\Block\ProductGridData;
use App\View\Data\Block\RecentlyWatchedData;
use Illuminate\View\Component;
use function App\Support\previousUrl;

class Product extends Component
{
    private ProductModel $product;

    public function __construct(
        ProductModel $product
    ) {
        $this->product = $product;
    }

    public function render()
    {
        $relatedSettings = new CatalogQuerySettings();
        $relatedIds = $this->product->relatedProducts()->pluck('products.id')->all();
        if ($relatedIds) {
            $relatedSettings->setProductIds($relatedIds);
            $relatedProductData = new ProductGridData(
                $relatedSettings,
                'Похожие товары',
                0
            );
        } else {
            $relatedProductData = null;
        }

        return view('components.entities.product', [
            'product' => $this->product,
            'previous' => previousUrl(),
            'recently_watched_settings' => new RecentlyWatchedData(4, $this->product->id),
            'relatedProductData' => $relatedProductData
        ]);
    }
}
