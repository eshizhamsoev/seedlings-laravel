<?php

namespace App\View\Components\Entities;

use App\Models\Data\Category\Category as CategoryModel;
use App\Services\Catalog\CatalogQuerySettings;
use Illuminate\View\Component;

class Category extends Component
{
    private CategoryModel $category;

    public function __construct(CategoryModel $category)
    {
        $this->category = $category;
    }

    public function render()
    {
        $settings = new CatalogQuerySettings();
        $settings->setCategoryIds([$this->category->id]);
        return view('components.entities.category', [
            'settings' => $settings,
            'h1' => $this->category->name,
            'category' => $this->category,
        ]);
    }
}
