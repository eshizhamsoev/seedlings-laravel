<?php

namespace App\View\Components\Block\Banners;

use App\Enums\Website\Settings\SpecialPage;
use App\Models\Website\Banner\Banner;
use App\Services\Settings\SettingHelper;
use Illuminate\View\Component;

class Home extends Component
{
    private SettingHelper $settingHelper;
    private Banner $banner;

    public function __construct(Banner $banner, SettingHelper $settingHelper)
    {
        $this->settingHelper = $settingHelper;
        $this->banner = $banner;
    }

    public function render()
    {
        return view('components.block.banners.home', [
            'banner' => $this->banner,
            'dimensions' => $this->banner->getDimensions(),
            'catalogPage' => $this->settingHelper->getPage(SpecialPage::CATALOG())
        ]);
    }
}
