<?php

namespace App\View\Components\Block;

use App\Enums\Catalog\OrderAscending;
use App\Enums\Catalog\OrderField;
use App\Enums\Catalog\OrderParamName;
use App\Enums\Catalog\ProductsView;
use App\Services\Catalog\CatalogQuerySettings;
use App\Services\Catalog\CatalogService;
use Illuminate\Http\Request;
use Illuminate\View\Component;

class CatalogFilter extends Component
{
    private Request $request;
    private CatalogService $catalogService;
    private CatalogQuerySettings $catalogQuerySettings;

    public function __construct(CatalogQuerySettings $settings, Request $request, CatalogService $catalogService)
    {
        $this->request = $request;
        $this->catalogService = $catalogService;
        $this->catalogQuerySettings = $settings;
    }

    public function render()
    {
        $orderField = $this->getOrderField();
        $orderAscending = $this->getOrderAscending();
        if ($orderField) {
            $this->catalogQuerySettings->setOrderField($orderField);
        }
        if ($orderAscending) {
            $this->catalogQuerySettings->setOrderAscending($orderAscending);
        }

//        ->aggregateRaw([
//        'ff' => [
//            'terms' => ['field' => 'filterAttributes.14'],
//        ],
//        'priceFrom' => [
//            'min' => ['field' => 'minPrice']
//        ],
//        'priceTo' => [
//            'max' => ['field' => 'minPrice']
//        ]
//    ])
        $query = $this->catalogService->getQuery($this->catalogQuerySettings);
        if (!$query) {
            return null;
        }
        $products = $query
//            ->aggregate('priceFrom', ['min' => ['field' => 'minPrice']])
            ->paginate(24);

        return view('components.block.catalog-filter', [
            'products' => $products->models(),
            'view' => ProductsView::GRID(),
            'links' => $products->links('components.base.pagination'),
            'orderField' => $orderField,
            'orderAscending' => $orderAscending,
            'minMax' => null, //$this->catalogService->getMinMaxPrice($this->catalogQuerySettings)
        ]);
    }

    private function getOrderField(): ?OrderField
    {
        $field = $this->request->get(OrderParamName::FIELD);
        if (!$field) {
            return null;
        }

        if (!OrderField::hasValue($field)) {
            return null;
        }

        return OrderField::fromValue($field);
    }

    private function getOrderAscending(): ?OrderAscending
    {
        $field = $this->request->get(OrderParamName::ASCENDING);
        if (!$field) {
            return null;
        }

        if (!OrderAscending::hasValue($field)) {
            return null;
        }

        return OrderAscending::fromValue($field);
    }
}
