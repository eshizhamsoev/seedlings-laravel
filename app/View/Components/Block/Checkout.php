<?php

namespace App\View\Components\Block;

use Illuminate\View\Component;

class Checkout extends Component
{
    public function render()
    {
        return view('components.block.checkout');
    }
}
