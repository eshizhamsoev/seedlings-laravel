<?php

namespace App\View\Components\Block;

use App\Services\Catalog\CatalogQuerySettings;
use App\Services\RecentlyWatched\RecentlyWatchedProductRepository;
use App\View\Data\Block\ProductGridData;
use App\View\Data\Block\RecentlyWatchedData;
use Illuminate\View\Component;

class RecentlyWatched extends Component
{
    private RecentlyWatchedProductRepository $recentlyWatchedProductRepository;
    private RecentlyWatchedData $data;

    public function __construct(
        RecentlyWatchedData $data,
        RecentlyWatchedProductRepository $recentlyWatchedProductRepository
    ) {
        $this->recentlyWatchedProductRepository = $recentlyWatchedProductRepository;
        $this->data = $data;
    }

    public function render()
    {
        $products = $this->recentlyWatchedProductRepository->getProductIds(
            $this->data->getLimit(),
            $this->data->getExcludedId()
        );
        if (!$products) {
            return null;
        }

        $settings = new CatalogQuerySettings();
        $settings->setProductIds($products);


        return view('components.block.recently-watched', [
            'gridData' => new ProductGridData(
                $settings,
                'Недавно просмотренные',
                6
            )
        ]);
    }
}
