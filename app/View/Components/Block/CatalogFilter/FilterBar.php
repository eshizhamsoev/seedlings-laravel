<?php

namespace App\View\Components\Block\CatalogFilter;

use App\Enums\Catalog\OrderAscending;
use App\Enums\Catalog\OrderField;
use App\Enums\Catalog\OrderParamName;
use App\Enums\Catalog\ProductsView;
use App\View\Data\Block\CatalogFilter\CatalogOrderItem;
use Illuminate\View\Component;
use function App\Support\createUrl;

class FilterBar extends Component
{
    private ProductsView $view;
    private ?OrderField $orderField;
    private ?OrderAscending $orderAscending;

    public function __construct(
        ProductsView $view,
        ?OrderField $orderField = null,
        ?OrderAscending $orderAscending = null
    ) {
        $this->view = $view;
        $this->orderField = $orderField;
        $this->orderAscending = $orderAscending;
    }

    public function render()
    {
        return view('components.block.catalog-filter.filter-bar', [
            'view' => $this->view,
            'orders' => [
                new CatalogOrderItem(
                    createUrl([
                        OrderParamName::FIELD => OrderField::PRICE,
                        OrderParamName::ASCENDING => OrderAscending::ASC
                    ]),
                    'От дешевых к дорогим',
                    false,
                    $this->orderField && $this->orderField->is(OrderField::PRICE)
                    &&
                    (!$this->orderAscending || $this->orderAscending->is(OrderAscending::ASC))
                ),
                new CatalogOrderItem(
                    createUrl([
                        OrderParamName::FIELD => OrderField::PRICE,
                        OrderParamName::ASCENDING => OrderAscending::DESC
                    ]),
                    'От дорогих к дешевым',
                    true,
                    $this->orderField && $this->orderField->is(OrderField::PRICE)
                    && $this->orderAscending && $this->orderAscending->is(OrderAscending::DESC)
                )
            ]
        ]);
    }
}
