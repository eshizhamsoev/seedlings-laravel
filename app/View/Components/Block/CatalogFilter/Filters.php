<?php

namespace App\View\Components\Block\CatalogFilter;

use App\Services\Catalog\MinMaxData;
use Illuminate\View\Component;

class Filters extends Component
{
    private ?MinMaxData $minMax;

    public function __construct(?MinMaxData $minMax)
    {
        $this->minMax = $minMax;
    }

    public function render()
    {
        return view('components.block.catalog-filter.filters', [
            'minMax' => $this->minMax
        ]);
    }
}
