<?php

namespace App\View\Components\Block;

use App\Services\Catalog\CatalogService;
use App\View\Data\Block\ProductGridData;
use Illuminate\View\Component;

class ProductGrid extends Component
{
    private ProductGridData $productGridData;
    private CatalogService $catalogService;

    public function __construct(
        ProductGridData $data,
        CatalogService $catalogService
    ) {
        $this->productGridData = $data;
        $this->catalogService = $catalogService;
    }

    public function render()
    {
        $query = $this->catalogService->getQuery($this->productGridData->getCatalogQuerySettings());

        $limit = $this->productGridData->getLimit();

        if ($limit !== 0) {
            $query->size($limit);
        }

        $products = $query->execute()->models();

        if ($products->isEmpty()) {
            return null;
        }
        return view('components.block.product-grid', [
            'products' => $products,
            'title' => $this->productGridData->getHeading(),
            'linkData' => $this->productGridData->getLinkData()
        ]);
    }
}
