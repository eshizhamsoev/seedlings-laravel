<?php

namespace App\View\Components\Block;

use App\Models\Data\Article;
use Illuminate\View\Component;

class NewsSlider extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $news = Article::orderBy('publication_date', 'desc')->limit(8)->get();
        return view('components.block.news-slider', [
            'news' => $news
        ]);
    }
}
