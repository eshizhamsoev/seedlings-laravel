<?php

namespace App\View\Components\Block;

use App\Models\Data\Review\Review;
use Illuminate\View\Component;

class Reviews extends Component
{
    public function render()
    {
        $reviews = Review::limit(10)->get();
        return view('components.block.reviews', [
            'reviews' => $reviews
        ]);
    }
}
