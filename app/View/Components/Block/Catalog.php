<?php

namespace App\View\Components\Block;

use App\Models\Data\Category\Category;
use Illuminate\View\Component;

class Catalog extends Component
{
    public function render()
    {
        return view('components.block.catalog', [
            'categories' => Category::whereIsRoot()->get()
        ]);
    }
}
