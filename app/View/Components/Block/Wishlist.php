<?php

namespace App\View\Components\Block;

use App\Services\Catalog\CatalogQuerySettings;
use App\Services\Catalog\CatalogService;
use App\Services\Wishlist\WishlistService;
use Illuminate\View\Component;

class Wishlist extends Component
{
    private WishlistService $wishlistService;
    private CatalogService $catalogService;

    public function __construct(WishlistService $wishlistService, CatalogService $catalogService)
    {
        $this->wishlistService = $wishlistService;
        $this->catalogService = $catalogService;
    }

    public function render()
    {
        $settings = new CatalogQuerySettings();
        $settings->setProductIds($this->wishlistService->getItems());
        $products = $this->catalogService->getQuery($settings)->get();
        return view('components.block.wishlist', [
            'products' => $products
        ]);
    }
}
