<?php

namespace App\View\Components\Block;

use App\Services\Catalog\CatalogService;
use App\View\Data\Block\ProductSliderData;
use Illuminate\View\Component;

class ProductSlider extends Component
{
    private ?ProductSliderData $data;
    private CatalogService $catalogService;

    public function __construct(?ProductSliderData $data, CatalogService $catalogService)
    {
        $this->data = $data;
        $this->catalogService = $catalogService;
    }

    public function render()
    {
        if (!$this->data) {
            return null;
        }

        $settings = $this->data->getCatalogQuerySettings();
        $products = $this->catalogService->getQuery($settings)->size(10)->execute();
        return view('components.block.product-slider', [
            'heading' => $this->data->getHeading(),
            'products' => $products->models()
        ]);
    }
}
