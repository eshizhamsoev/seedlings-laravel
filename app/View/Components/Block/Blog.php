<?php

namespace App\View\Components\Block;

use App\Enums\Website\Settings\SpecialPage;
use App\Models\Data\Article;
use App\Models\Website\Page;
use Carbon\Carbon;
use Illuminate\View\Component;

class Blog extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {

        $articles = Article::where('publication_date', '<=', Carbon::today())->orderBy('publication_date', 'desc')->paginate(12);

        return view('components.block.blog', [
            'url' => Page::findOrFail(nova_get_setting(SpecialPage::BLOG))->url,
            'articles' => $articles
        ]);
    }
}
