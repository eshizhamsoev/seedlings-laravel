<?php

namespace App\View\Components\Block;

use App\Enums\Website\Settings\SpecialPage;
use App\Services\Cart\CartService;
use App\Services\Settings\SettingHelper;
use Illuminate\View\Component;

class Cart extends Component
{
    private CartService $cartService;
    private SettingHelper $settingHelper;

    public function __construct(CartService $cartService, SettingHelper $settingHelper)
    {
        $this->cartService = $cartService;
        $this->settingHelper = $settingHelper;
    }

    public function render()
    {
        return view('components.block.cart', [
            'isEmpty' => $this->cartService->getCount() === 0,
            'catalogPage' => $this->settingHelper->getPage(SpecialPage::CATALOG()),
            'checkoutPage' => $this->settingHelper->getPage(SpecialPage::CHECKOUT())
        ]);
    }
}
