<?php

namespace App\View\Components\Block;

use App\Models\Data\Category\Category;
use App\View\Data\Block\FeaturedCategoriesData;
use Illuminate\View\Component;

class FeaturedCategories extends Component
{
    private FeaturedCategoriesData $data;

    public function __construct(FeaturedCategoriesData $data)
    {
        $this->data = $data;
    }

    public function render()
    {

        $categoryMap = Category::whereIn('id', $this->data->getCategoryIds())
            ->get()
            ->mapWithKeys(fn($category) => [
                $category->id => $category
            ]);
        $categories = [];
        foreach ($this->data->getCategoryIds() as $id) {
            $category = $categoryMap[$id] ?? null;
            if ($category) {
                $categories[] = $category;
            }
        }

        return view('components.block.featured-categories', [
            'categories' => $categories,
            'heading' => $this->data->getHeading()
        ]);
    }
}
