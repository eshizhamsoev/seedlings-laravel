<?php

namespace App\View\Components\Block;

use Illuminate\View\Component;

class Subscribe extends Component
{
    public function render()
    {
        return view('components.block.subscribe');
    }
}
