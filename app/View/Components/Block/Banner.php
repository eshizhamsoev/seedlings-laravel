<?php

namespace App\View\Components\Block;

use App\Models\Website\Banner\DimensionData;
use App\View\Data\Block\BannerData;
use Illuminate\View\Component;

class Banner extends Component
{
    private BannerData $data;

    static int $banner = 0;

    public function __construct(BannerData $data)
    {
        $this->data = $data;
        self::$banner++;
    }

    public function render()
    {
        if (!$this->data) {
            return null;
        }
        $banner = $this->data->getBanner();
        $banner->load('items', 'items.media');


        return view('components.block.banner',
            [
                'banner' => $banner,
                'bannerBlockName' => 'block.banners.' . $this->data->getBannerTemplate()->value
            ]
        );
    }
}
