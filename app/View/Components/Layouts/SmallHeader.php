<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class SmallHeader extends Component
{
    public function render()
    {
        return view('components.layouts.small-header', [

        ]);
    }
}
