<?php

namespace App\View\Components\Layouts;

use App\Enums\Website\Settings\SpecialPage;
use App\Services\Cart\CartService;
use App\Services\CurrentCityRepository;
use App\Services\Settings\SettingHelper;
use Illuminate\View\Component;

class Header extends Component
{
    private CurrentCityRepository $currentCityRepository;
    private CartService $cartService;
    private SettingHelper $settingHelper;

    public function __construct(
        CurrentCityRepository $currentCityRepository,
        CartService $cartService,
        SettingHelper $settingHelper
    ) {
        $this->currentCityRepository = $currentCityRepository;
        $this->cartService = $cartService;
        $this->settingHelper = $settingHelper;
    }

    public function render()
    {
        $city = $this->currentCityRepository->getCurrentCity();
        return view('components.layouts.header', [
            'logo' => asset('/static/images/common/logo.svg'),
            'cartQuantity' => $this->cartService->getCount(),
            'cartPage' => $this->settingHelper->getPage(SpecialPage::CART()),
            'catalogPage' => $this->settingHelper->getPage(SpecialPage::CATALOG()),
            'wishlistPage' => $this->settingHelper->getPage(SpecialPage::WISHLIST()),
            'currentCity' => $city
        ]);
    }
}
