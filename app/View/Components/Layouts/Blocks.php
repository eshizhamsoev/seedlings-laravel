<?php

namespace App\View\Components\Layouts;

use App\ConfigurableBlockSettings\SettingResolver;
use App\Models\Website\ConfigurableBlock;
use App\Nova\Website\ConfigurableNovaBlock;
use App\View\Data\Layouts\BlockGroup;
use App\View\Data\Layouts\ResolvedBlock;
use Illuminate\View\Component;

class Blocks extends Component
{
    private array $blocks;
    private SettingResolver $resolver;

    public function __construct(SettingResolver $resolver, array $blocks)
    {
        $this->blocks = $blocks;
        $this->resolver = $resolver;
    }

    public function render()
    {
        return view('components.layouts.blocks', [
            'groups' => $this->groupBlocks($this->blocks)
        ]);
    }

    private function resolveBlock(ConfigurableBlock $block): ResolvedBlock
    {
        return new ResolvedBlock(
            'block.' . $block->type,
            $this->resolver->resolve($block->type)->extractSettings($block->settings)
        );
    }

    /**
     * @param  ConfigurableBlock[]  $blocks
     * @return BlockGroup[]
     */
    private function groupBlocks(array $blocks): array
    {
        $groups = [];
        $prev = null;
        $group = null;
        foreach ($blocks as $block) {
            if (!$prev || $prev->pivot->color->isNot($block->pivot->color)) {
                $group = new BlockGroup($block->pivot->color);
                $groups[] = $group;
            }
            $group->addBlock($this->resolveBlock($block));
        }
        return $groups;
    }
}
