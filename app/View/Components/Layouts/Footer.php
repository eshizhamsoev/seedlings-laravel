<?php

namespace App\View\Components\Layouts;

use App\Enums\Website\Settings\SpecialPage;
use App\Models\Website\Page;
use App\Services\CurrentCityRepository;
use App\Services\Settings\SettingHelper;
use Illuminate\View\Component;

class Footer extends Component
{
    private CurrentCityRepository $currentCityRepository;
    private SettingHelper $settingHelper;

    public function __construct(
        CurrentCityRepository $currentCityRepository,
        SettingHelper $settingHelper
    )
    {
        $this->currentCityRepository = $currentCityRepository;
        $this->settingHelper = $settingHelper;
    }

    public function render()
    {
        return view('components.layouts.footer', [
            'logo' => asset('/static/images/common/logo.svg'),
            'currentCity' => $this->currentCityRepository->getCurrentCity(),
            'contacts' => $this->settingHelper->getPage(SpecialPage::CONTACTS())
        ]);
    }
}
