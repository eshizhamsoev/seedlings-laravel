<?php

namespace App\View\Components\Layouts;

use App\Services\CurrentCityRepository;
use Illuminate\View\Component;

class MobileMenu extends Component
{
    private CurrentCityRepository $currentCityRepository;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        $current = $this->currentCityRepository->getCurrentCity();
        return view('components.layouts.mobile-menu');
    }

    private function getEmailLink(): ?string
    {
        if (!nova_get_setting(SettingsFields::EMAIL)) {
            return null;
        }
        return 'mailto:' . nova_get_setting(SettingsFields::EMAIL);
    }
}
