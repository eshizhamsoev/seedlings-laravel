<?php

namespace App\View\Components\Layouts\Header;

use Illuminate\View\Component;

class Search extends Component
{
    public function render()
    {
        return view('components.layouts.header.search');
    }
}
