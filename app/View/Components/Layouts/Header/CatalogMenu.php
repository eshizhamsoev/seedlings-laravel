<?php

namespace App\View\Components\Layouts\Header;

use App\Models\Data\Category\Category;
use Illuminate\View\Component;

class CatalogMenu extends Component
{
    public function render()
    {
        return view('components.layouts.header.catalog-menu', [
            'categories' => Category::with([
                'children' => fn($query) => $query->whereHas('url'),
                'url',
                'children.url'
            ])
                ->whereHas('url')
                ->whereIsRoot()
                ->get()
        ]);
    }
}
