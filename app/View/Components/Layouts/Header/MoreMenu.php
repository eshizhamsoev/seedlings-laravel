<?php

namespace App\View\Components\Layouts\Header;

use Illuminate\View\Component;

class MoreMenu extends Component
{
    public function render()
    {
        return view('components.layouts.header.more-menu');
    }
}
