<?php

namespace App\View\Components\Core;

use App\Enums\EntityType;
use App\Models\Website\Url;
use App\View\Components\Entities;
use Illuminate\View\Component;

class Renderer extends Component
{
    private Url $url;

    private const RENDERERS = [
        EntityType::PAGE => Entities\Page::class,
        EntityType::CATEGORY => Entities\Category::class,
        EntityType::PRODUCT => Entities\Product::class,
        EntityType::ARTICLE => Entities\Article::class
    ];

    public function __construct(Url $url)
    {
        $this->url = $url;
    }

    public function render()
    {
        if (!$this->url->entity_type) {
            return '';
        }
        $rendererComponent = self::RENDERERS[$this->url->entity_type] ?? null;

        if (!$rendererComponent) {
            return '';
        }
        return (new $rendererComponent($this->url->entity))->render();
    }
}
