<?php

namespace App\Services\Cart;

use App\Models\Data\Product\Product;
use Gloudemans\Shoppingcart\CartItem;

class ItemInfo
{
    private CartItem $cartItem;
    private Product $product;

    public function __construct(
        CartItem $cartItem
    ) {
        $this->cartItem = $cartItem;
        $this->product = $cartItem->model->product;
    }

    public function getPrice(): float
    {
        return $this->cartItem->price;
    }

    public function getOldPrice(): ?float
    {
        return $this->cartItem->model->old_price;
    }

    public function getOffer()
    {
        return $this->cartItem->model;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getCount(): int
    {
        return $this->cartItem->qty;
    }

    public function getTotal(): float
    {
        return $this->cartItem->total;
    }

    public function getName()
    {
        return $this->cartItem->name;
    }

    public function getKey()
    {
        return $this->cartItem->rowId;
    }
}
