<?php

namespace App\Services\Cart;

class Cart
{
    /**
     * @return ItemInfo
     */
    private array $items;
    private int $count;
    private float $total;
    private float $weight;

    public function __construct(array $items, int $count, float $total, float $weight)
    {
        $this->items = $items;
        $this->count = $count;
        $this->total = $total;
        $this->weight = $weight;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @return ItemInfo
     */
    public function getItems()
    {
        return $this->items;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }
}
