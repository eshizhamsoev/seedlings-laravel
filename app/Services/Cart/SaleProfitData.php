<?php

namespace App\Services\Cart;

class SaleProfitData
{
    private float $total;
    private float $percent;

    public function __construct(float $total, float $percent)
    {
        $this->total = $total;
        $this->percent = $percent;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }
}
