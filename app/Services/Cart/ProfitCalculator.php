<?php

namespace App\Services\Cart;

class ProfitCalculator
{
    public function calculate(?float $currentPrice, ?float $oldPrice): ?SaleProfitData
    {
        if (!$currentPrice || !$oldPrice) {
            return null;
        }

        if ($currentPrice >= $oldPrice) {
            return null;
        }

        $total = $oldPrice - $currentPrice;
        $percent = round($total / $oldPrice * 100);

        return new SaleProfitData($total, $percent);
    }
}
