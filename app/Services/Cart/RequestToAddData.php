<?php

namespace App\Services\Cart;

use Gloudemans\Shoppingcart\Contracts\Buyable;

class RequestToAddData
{
    private Buyable $buyable;
    private int $quantity;

    public function __construct(Buyable $buyable, int $quantity)
    {
        $this->buyable = $buyable;
        $this->quantity = $quantity;
    }

    /**
     * @return Buyable
     */
    public function getBuyable(): Buyable
    {
        return $this->buyable;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
