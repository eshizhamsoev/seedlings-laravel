<?php

namespace App\Services\Cart;

use Gloudemans\Shoppingcart\Cart as Shoppingcart;
use Gloudemans\Shoppingcart\CartItem;

class CartService
{
    private Shoppingcart $cart;

    public function __construct(Shoppingcart $cart)
    {
        $this->cart = $cart;
    }

    public function getCount(): int
    {
        return $this->cart->count();
    }

    public function getTotal(): float
    {
        return $this->cart->totalFloat();
    }

    public function getItems()
    {
        return $this->cart
            ->content()
            ->map(fn(CartItem $cartItem) => new ItemInfo($cartItem));
    }

    public function addProduct(RequestToAddData $addData)
    {
        $this->cart->add($addData->getBuyable(), $addData->getQuantity());
    }

    public function updateProduct(string $rowId, int $count)
    {
        $this->cart->update($rowId, $count);
    }

    public function getCart(): Cart
    {
        return new Cart(
            $this->getItems()->all(),
            $this->getCount(),
            $this->getTotal(),
            $this->cart->weightFloat()
        );
    }
}
