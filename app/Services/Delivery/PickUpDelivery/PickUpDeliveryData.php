<?php

namespace App\Services\Delivery\PickUpDelivery;

class PickUpDeliveryData
{
    private string $postalCode;
    private float $weight;

    public function __construct(string $postCode, float $weight)
    {
        $this->postalCode = $postCode;
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }
}
