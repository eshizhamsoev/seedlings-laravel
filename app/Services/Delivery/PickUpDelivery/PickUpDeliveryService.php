<?php

namespace App\Services\Delivery\PickUpDelivery;

use AntistressStore\CdekSDK2\CdekClientV2;
use AntistressStore\CdekSDK2\Entity\Requests\Location;
use AntistressStore\CdekSDK2\Entity\Requests\Tariff;
use AntistressStore\CdekSDK2\Exceptions\CdekV2RequestException;
use App\Services\Delivery\DeliveryCalculation;
use App\Services\Delivery\ToDoorDelivery\ToDoorCalculation;
use Carbon\Carbon;

class PickUpDeliveryService
{
    private CdekClientV2 $client;

    public function __construct(CdekClientV2 $cdekClientV2)
    {
        $this->client = $cdekClientV2;
    }

    public function calculate(PickUpDeliveryData $data): ?DeliveryCalculation
    {

        try {

            $tariff = (new Tariff())
                ->setTariffCode(366) // Указывает код тарифа для расчета
                ->setFromLocation((new Location())->setCity('Москва')->setAddress('Народного ополчения, 14к2'))
                ->setToLocation((new Location())->setPostalCode($data->getPostalCode()))
                ->setPackageWeight($data->getWeight());

            $tariff_response = $this->client->calculateTariff($tariff); // TariffResponse
            return new ToDoorCalculation(
                $tariff_response->getTotalSum(),
                Carbon::now()->addWeekdays($tariff_response->getPeriodMin()),
                Carbon::now()->addWeekdays($tariff_response->getPeriodMax())
            );

        } catch (CdekV2RequestException $e) {
            return null;
        }
    }
}
