<?php

namespace App\Services\Delivery\PickUpDelivery;

class PickUpPoint
{
    private string $name;
    private string $city;
    private string $address;
    private string $addressComment;
    private string $workTime;
    private float $lat;
    private float $lng;
    private array $images;

    public function __construct(
        string $name,
        string $city,
        string $address,
        string $addressComment,
        string $workTime,
        float $lat,
        float $lng,
        array $images
    ) {
        $this->name = $name;
        $this->city = $city;
        $this->address = $address;
        $this->addressComment = $addressComment;
        $this->workTime = $workTime;
        $this->lat = $lat;
        $this->lng = $lng;
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getAddressComment(): string
    {
        return $this->addressComment;
    }

    /**
     * @return string
     */
    public function getWorkTime(): string
    {
        return $this->workTime;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }
}
