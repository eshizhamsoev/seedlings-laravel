<?php

namespace App\Services\Delivery;

use App\Enums\Order\DeliveryType;
use App\Services\AddressService\AddressDecoder;
use App\Services\Cart\Cart;
use App\Services\Delivery\PickUpDelivery\PickUpDeliveryData;
use App\Services\Delivery\PickUpDelivery\PickUpDeliveryService;
use App\Services\Delivery\ToDoorDelivery\ToDoorDeliveryData;
use App\Services\Delivery\ToDoorDelivery\ToDoorDeliveryService;
use App\Services\Ordering\Data\OrderData;

class DeliveryService
{
    private ToDoorDeliveryService $toDoorDeliveryService;
    private AddressDecoder $addressDecoder;
    private PickUpDeliveryService $pickUpDeliveryService;

    public function __construct(
        ToDoorDeliveryService $deliveryService,
        PickUpDeliveryService $pickUpDeliveryService,
        AddressDecoder $addressDecoder
    ) {
        $this->toDoorDeliveryService = $deliveryService;
        $this->addressDecoder = $addressDecoder;
        $this->pickUpDeliveryService = $pickUpDeliveryService;
    }

    public function process(OrderData $orderData, Cart $cart): ?DeliveryCalculation
    {
        if ($orderData->getDeliveryType()->is(DeliveryType::TO_DOOR)) {
            if (!$orderData->getAddress()) {
                return null;
            }

            $decodedAddress = $this->addressDecoder->decode($orderData->getAddress());
            if (!$decodedAddress) {
                // TODO: add message that address should be concrete
                return null;
            }

            return $this->toDoorDeliveryService->calculate(
                new ToDoorDeliveryData($decodedAddress, $cart)
            );
        }
        if ($orderData->getDeliveryType()->is(DeliveryType::PICKUP)) {

            if (!$orderData->getPickUpPoint()) {
                return null;
            }

            return $this->pickUpDeliveryService->calculate(
                new PickUpDeliveryData(
                    $orderData->getPickUpPoint()->postal_code, $cart->getWeight() ?: 100
                )
            );
        }
        return null;
    }
}
