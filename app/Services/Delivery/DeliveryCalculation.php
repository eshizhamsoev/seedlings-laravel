<?php

namespace App\Services\Delivery;

use Carbon\Carbon;

interface DeliveryCalculation
{

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return Carbon
     */
    public function getStartDate();

    /**
     * @return Carbon
     */
    public function getEndDate();
}
