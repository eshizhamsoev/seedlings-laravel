<?php

namespace App\Services\Delivery\ToDoorDelivery;

use App\Services\AddressService\AddressData;
use App\Services\Cart\Cart;

class ToDoorDeliveryData
{
    private AddressData $address;
    private Cart $cart;

    public function __construct(AddressData $address, Cart $cart)
    {
        $this->address = $address;
        $this->cart = $cart;
    }

    public function getWeight(): float
    {
        return $this->cart->getWeight();
    }

    public function getAddress(): AddressData
    {
        return $this->address;
    }
}
