<?php

namespace App\Services\Delivery\ToDoorDelivery;

use App\Services\Delivery\DeliveryCalculation;
use Carbon\Carbon;

class ToDoorCalculation implements DeliveryCalculation
{
    private float $price;
    private Carbon $startDate;
    private Carbon $endDate;

    public function __construct(float $price, Carbon $startDate, Carbon $endDate)
    {
        $this->price = $price;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return Carbon
     */
    public function getStartDate(): Carbon
    {
        return $this->startDate;
    }

    /**
     * @return Carbon
     */
    public function getEndDate(): Carbon
    {
        return $this->endDate;
    }


}
