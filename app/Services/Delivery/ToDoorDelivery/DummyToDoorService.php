<?php

namespace App\Services\Delivery\ToDoorDelivery;

use Carbon\Carbon;

class DummyToDoorService implements ToDoorDeliveryService
{

    public function calculate(ToDoorDeliveryData $data): ToDoorCalculation
    {
        return new ToDoorCalculation(
            100 + $data->getWeight() * 100,
            Carbon::now()->addWeekdays(5),
            Carbon::now()->addWeekdays(7)
        );
    }
}
