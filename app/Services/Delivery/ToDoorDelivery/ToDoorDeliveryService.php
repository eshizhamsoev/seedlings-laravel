<?php

namespace App\Services\Delivery\ToDoorDelivery;

interface ToDoorDeliveryService
{
    public function calculate(ToDoorDeliveryData $data): ?ToDoorCalculation;
}
