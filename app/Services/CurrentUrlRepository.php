<?php

namespace App\Services;

use App\Models\Website\Url;

class CurrentUrlRepository
{
    private Url $currentUrl;

    public function init(Url $currentUrl)
    {
        $this->currentUrl = $currentUrl;
    }

    public function getCurrentUrl(): Url
    {
        return $this->currentUrl;
    }
}
