<?php

namespace App\Services\Settings;

use App\Enums\Website\Settings\CodePosition;
use App\Enums\Website\Settings\SpecialPage;
use App\Models\Website\Page;
use Illuminate\Support\Collection;

class SettingHelper
{
    private ?Collection $pages = null;
    private ?array $codePositions = null;

    public function __construct()
    {
    }

    private function getPages()
    {
        $settings = nova_get_settings(SpecialPage::getValues());
        $pages = Page::with('url')
            ->whereIn('id', array_values($settings))
            ->get()
            ->mapWithKeys(fn($item) => [
                $item->id => $item
            ]);
        return collect($settings)->map(fn($item) => $pages[$item] ?? null);
    }

    public function getPage(SpecialPage $page): ?Page
    {
        if (!$this->pages) {
            $this->pages = $this->getPages();
        }
        return $this->pages->get($page->value);
    }

    private function getCodePositions()
    {
        return nova_get_settings(CodePosition::getValues());
    }

    public function getCodePosition(CodePosition $codePosition): ?string
    {

        if (!$this->pages) {
            $this->codePositions = $this->getCodePositions();
        }
        return $this->codePositions[$codePosition->value] ?? null;
    }
}
