<?php

namespace App\Services\AddressService;

interface AddressDecoder
{
    public function decode(string $address);
}
