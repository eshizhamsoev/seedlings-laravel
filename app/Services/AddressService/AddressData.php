<?php

namespace App\Services\AddressService;

interface AddressData
{
    public function getRawAddress(): string;

    public function getRawData(): array;

    public function getCountry(): ?string;

    public function getCountryIsoCode(): ?string;

    public function getPostalCode(): ?string;

    public function getRegion(): ?string;

    public function getArea(): ?string;

    public function getCity(): ?string;

    public function getCityDistrict(): ?string;

    public function getSettlement(): ?string;

    public function getStreet(): ?string;

    public function getHouse(): ?string;

    public function getBlock(): ?string;

    public function getFlat(): ?string;

    public function getKladrId(): ?string;

    public function getGeoLat(): ?float;
    public function getGeoLng(): ?float;

}
