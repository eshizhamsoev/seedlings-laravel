<?php

namespace App\Services\AddressService\DadataDecoder;

use App\Services\AddressService\AddressData;

class DadataAddressData implements AddressData
{

    private string $address;
    private array $dataFields;

    public function __construct(string $address, array $dataFields)
    {
        $this->address = $address;
        $this->dataFields = $dataFields;
    }

    public function getRawAddress(): string
    {
        return $this->address;
    }

    public function getRawData(): array
    {
        return  $this->dataFields;
    }

    public function getCountry(): ?string
    {
        return $this->dataFields['country'] ?? null;
    }

    public function getCountryIsoCode(): ?string
    {
        return $this->dataFields['country_iso_code'] ?? null;
    }

    public function getPostalCode(): ?string
    {
        return $this->dataFields['country_iso_code'] ?? null;
    }

    public function getRegion(): ?string
    {
        return $this->dataFields['region'] ?? null;
    }

    public function getArea(): ?string
    {
        return $this->dataFields['area'] ?? null;
    }

    public function getCity(): ?string
    {
        return $this->dataFields['city'] ?? null;
    }

    public function getCityDistrict(): ?string
    {
        return $this->dataFields['city_district'] ?? null;
    }

    public function getSettlement(): ?string
    {
        return $this->dataFields['settlement'] ?? null;
    }

    public function getStreet(): ?string
    {
        return $this->dataFields['street'] ?? null;
    }

    public function getHouse(): ?string
    {
        return $this->dataFields['house'] ?? null;
    }

    public function getBlock(): ?string
    {
        return $this->dataFields['block'] ?? null;
    }

    public function getFlat(): ?string
    {
        return $this->dataFields['flat'] ?? null;
    }

    public function getKladrId(): ?string
    {
        return $this->dataFields['kladr_id'] ?? null;
    }

    public function getGeoLat(): ?float
    {
        return $this->dataFields['geo_lat'] ?? null;
    }

    public function getGeoLng(): ?float
    {
        return $this->dataFields['geo_lon'] ?? null;
    }
}
