<?php

namespace App\Services\AddressService\DadataDecoder;

use App\Services\AddressService\AddressDecoder;
use Dadata\DadataClient;

class DadataDecoder implements AddressDecoder
{

    private DadataClient $client;

    public function __construct(DadataClient $client)
    {
        $this->client = $client;
    }

    public function decode(string $address): ?DadataAddressData
    {
        $suggestions = $this->client->suggest('address', $address, 1);
        if(!$suggestions){
            return null;
        }
        $suggestion = $suggestions[0];
        if(!$suggestion || $suggestion['value'] !== $address){
            return null;
        }

        return new DadataAddressData($suggestion['value'], $suggestion['data'] ?? []);
    }
}
