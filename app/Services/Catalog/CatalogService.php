<?php

namespace App\Services\Catalog;

use App\Enums\Catalog\OrderAscending;
use App\Enums\Catalog\OrderField;
use App\Models\Data\Product\Offers\SimpleOffer;
use App\Models\Data\Product\Offers\NamedOffer;
use App\Models\Data\Product\Product;

class CatalogService
{
    private ProductSearchQueryFactory $productSearchQueryFactory;

    public function __construct(ProductSearchQueryFactory $productSearchQueryFactory)
    {
        $this->productSearchQueryFactory = $productSearchQueryFactory;
    }

    public function getQuery(
        CatalogQuerySettings $catalogQuerySettings
    ) {
        $query = Product::searchQuery(
            $this->productSearchQueryFactory->getQuery($catalogQuerySettings)
        );
        if ($catalogQuerySettings->getOrderField()) {
            $query->sort(
                $catalogQuerySettings->getOrderField()->value,
                $catalogQuerySettings->getOrderAscending()->value
            );
        } else {
            $query->sort(OrderField::PRIORITY, 'desc');
        }
        return $query;
    }
}
