<?php

namespace App\Services\Catalog;

use App\Enums\Catalog\OrderAscending;
use App\Enums\Catalog\OrderField;

class CatalogQuerySettings
{
    private ?OrderField $orderField = null;
    private ?OrderAscending $orderAscending = null;

    private ?string $searchQuery = null;
    private ?array $categoryIds = null;
    private ?array $productIds = null;
    private ?bool $onlySale = null;
    private ?array $labels = null;
    private bool $useProductIdsForOrder = false;


    /**
     * @return OrderField|null
     */
    public function getOrderField(): ?OrderField
    {
        return $this->orderField;
    }

    /**
     * @param  OrderField|null  $orderField
     */
    public function setOrderField(?OrderField $orderField): void
    {
        $this->orderField = $orderField;
    }

    /**
     * @return OrderAscending|null
     */
    public function getOrderAscending(): ?OrderAscending
    {
        return $this->orderAscending;
    }

    /**
     * @param  OrderAscending|null  $orderAscending
     */
    public function setOrderAscending(?OrderAscending $orderAscending): void
    {
        $this->orderAscending = $orderAscending;
    }

    /**
     * @return array|null
     */
    public function getCategoryIds(): ?array
    {
        return $this->categoryIds;
    }

    /**
     * @param  array|null  $categoryIds
     */
    public function setCategoryIds(?array $categoryIds): void
    {
        $this->categoryIds = $categoryIds;
    }

    /**
     * @return array|null
     */
    public function getProductIds(): ?array
    {
        return $this->productIds;
    }

    /**
     * @param  array|null  $productIds
     */
    public function setProductIds(array $productIds, bool $useForOrder = false): void
    {
        if(count($productIds) === 0){
            throw new \InvalidArgumentException('Empty product ids are not allowed!');
        }
        $this->productIds = $productIds;
        $this->useProductIdsForOrder = $useForOrder;
    }

    /**
     * @return bool|null
     */
    public function getOnlySale(): ?bool
    {
        return $this->onlySale;
    }

    /**
     * @param  bool|null  $onlySale
     */
    public function setOnlySale(?bool $onlySale): void
    {
        $this->onlySale = $onlySale;
    }

    /**
     * @return array|null
     */
    public function getLabels(): ?array
    {
        return $this->labels;
    }

    /**
     * @param  array|null  $labels
     */
    public function setLabels(?array $labels): void
    {
        $this->labels = $labels;
    }

    /**
     * @return bool
     */
    public function isUseProductIdsForOrder(): bool
    {
        return $this->useProductIdsForOrder;
    }

    /**
     * @return string|null
     */
    public function getSearchQuery(): ?string
    {
        return $this->searchQuery;
    }

    /**
     * @param  string|null  $searchQuery
     */
    public function setSearchQuery(?string $searchQuery): void
    {
        $this->searchQuery = $searchQuery;
    }
}
