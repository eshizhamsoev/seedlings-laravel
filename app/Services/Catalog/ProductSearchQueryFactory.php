<?php

namespace App\Services\Catalog;

use App\Services\Catalog\CatalogQuerySettings;
use ElasticScoutDriverPlus\Builders\QueryBuilderInterface;
use ElasticScoutDriverPlus\Support\Query;

class ProductSearchQueryFactory
{
    public function getQuery(CatalogQuerySettings $settings): QueryBuilderInterface
    {
        $query = Query::bool();

        if (!is_null($settings->getCategoryIds())) {
            $query->must(
                Query::terms()
                    ->field('categoryId')
                    ->values($settings->getCategoryIds())
            );
        }

        if (!is_null($settings->getProductIds())) {
            $query->must(
                Query::ids()->values($settings->getProductIds())
            );
        }

        if (!is_null($settings->getSearchQuery())) {
            $query->must(
                Query::fuzzy()
                    ->field('name')
                    ->value($settings->getSearchQuery())
            );
        }
        return $query;
    }
}
