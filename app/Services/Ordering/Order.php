<?php

namespace App\Services\Ordering;

use App\Services\AddressService\AddressData;
use App\Services\Cart\Cart;
use App\Services\Cart\ItemInfo;
use App\Services\Delivery\DeliveryCalculation;
use App\Services\Ordering\Data\OrderData;


class Order
{
    private OrderData $orderData;
    private Cart $cart;
    private ?DeliveryCalculation $deliveryCalculation;
    private ?AddressData $decodedAddress;

    public function __construct(
        Cart $cart,
        OrderData $orderData,
        ?AddressData $decodedAddress = null,
        ?DeliveryCalculation $deliveryCalculation = null
    ) {
        $this->orderData = $orderData;
        $this->cart = $cart;
        $this->deliveryCalculation = $deliveryCalculation;
        $this->decodedAddress = $decodedAddress;
    }

    /**
     * @return OrderData
     */
    public function getOrderData(): OrderData
    {
        return $this->orderData;
    }

    /**
     * @return ItemInfo[]
     */
    public function getItems(): array
    {
        return $this->cart->getItems();
    }

    /**
     * @return DeliveryCalculation|null
     */
    public function getDeliveryCalculation(): ?DeliveryCalculation
    {
        return $this->deliveryCalculation;
    }


    public function getTotal(): float
    {
        $calculation = $this->getDeliveryCalculation();
        return $this->cart->getTotal() + ($calculation ? $calculation->getPrice() : 0);
    }

    static function empty(Cart $cart): self
    {
        return new self($cart, OrderData::empty());
    }

    /**
     * @return AddressData|null
     */
    public function getDecodedAddress(): ?AddressData
    {
        return $this->decodedAddress;
    }

    public function canBeFinished(): bool
    {
        $customerData = $this->orderData->getCustomerData();
        return $this->deliveryCalculation
            && $customerData->getPhone()
            && $customerData->getEmail()
            && $customerData->getName();
    }
}
