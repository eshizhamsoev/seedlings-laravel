<?php

namespace App\Services\Ordering;

use App\Models\Data\Order\Order as OrderModel;
use App\Models\Data\Order\OrderItem;
use App\Services\AddressService\AddressDecoder;
use App\Services\Cart\CartService;
use App\Services\Delivery\DeliveryService;
use App\Services\Ordering\Data\OrderData;

class OrderingService
{
    private OrderingStorage $orderingStorage;
    private CartService $cartService;
    private DeliveryService $deliveryService;
    private AddressDecoder $addressDecoder;

    public function __construct(
        OrderingStorage $orderingStorage,
        CartService $cartService,
        DeliveryService $deliveryService,
        AddressDecoder $addressDecoder
    ) {
        $this->orderingStorage = $orderingStorage;
        $this->cartService = $cartService;
        $this->deliveryService = $deliveryService;
        $this->addressDecoder = $addressDecoder;
    }

    public function updateData(OrderData $orderData)
    {
        $this->orderingStorage->save($orderData);
    }

    public function getOrder(): Order
    {
        $cart = $this->cartService->getCart();
        $orderData = $this->orderingStorage->getOrder();
        return $orderData ? new Order(
            $cart,
            $orderData,
            $orderData->getAddress() ? $this->addressDecoder->decode($orderData->getAddress()) : null,
            $this->deliveryService->process($orderData, $cart)
        ) : Order::empty($cart);
    }

    public function finish(): int
    {
        $orderModel = new OrderModel();

        \DB::transaction(function () use ($orderModel) {

            $order = $this->getOrder();
            if (!$order->canBeFinished()) {
                throw new \LogicException('Order could not be finished');
            }
            $orderData = $order->getOrderData();
            $customerData = $orderData->getCustomerData();
            $orderModel->phone = $customerData->getPhone();
            $orderModel->email = $customerData->getEmail();
            $orderModel->name = $customerData->getName();
            $orderModel->total = $order->getTotal();
            $orderModel->address = $orderData->getAddress();
            $orderModel->delivery_type = $orderData->getDeliveryType();
            $orderModel->payment_method = $orderData->getPaymentMethod();
            $orderModel->save();

            foreach ($order->getItems() as $item) {
                $orderProductModel = new OrderItem();
                $orderProductModel->order()->associate($orderModel);
                $orderProductModel->product()->associate($item->getProduct());
                $orderProductModel->offer()->associate($item->getOffer());
                $orderProductModel->price = $item->getPrice();
                $orderProductModel->old_price = $item->getOldPrice();
                $orderProductModel->name = $item->getName();
                $orderProductModel->count = $item->getCount();
                $orderProductModel->save();
            }
        });

        return $orderModel->id;
    }
}
