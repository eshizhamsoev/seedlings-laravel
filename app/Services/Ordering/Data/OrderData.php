<?php

namespace App\Services\Ordering\Data;

use App\Enums\Order\DeliveryType;
use App\Enums\Order\PaymentMethod;
use App\Models\Data\Delivery\PickUpPoint;
use App\Services\Ordering\Data\CustomerData;

class OrderData
{
    private CustomerData $customerData;
    private ?string $comment = null;
    private DeliveryType $deliveryType;
    private ?string $address = null;
    private ?PickUpPoint $pickUpPoint = null;
    private PaymentMethod $paymentMethod;

    public function __construct(
        CustomerData $customerData,
        DeliveryType $deliveryType,
        PaymentMethod $paymentMethod
    )
    {
        $this->customerData = $customerData;
        $this->deliveryType = $deliveryType;
        $this->paymentMethod = $paymentMethod;
    }

    public function getCustomerData(): CustomerData
    {
        return $this->customerData;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param  string|null  $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return DeliveryType
     */
    public function getDeliveryType(): DeliveryType
    {
        return $this->deliveryType;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param  string|null  $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    static public function empty(): self
    {
        return new self(
            new CustomerData(null, null, null),
            DeliveryType::fromValue(DeliveryType::TO_DOOR),
            PaymentMethod::fromValue(PaymentMethod::ONLINE)
        );
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return PickUpPoint|null
     */
    public function getPickUpPoint(): ?PickUpPoint
    {
        return $this->pickUpPoint;
    }

    /**
     * @param  PickUpPoint|null  $pickUpPoint
     */
    public function setPickUpPoint(?PickUpPoint $pickUpPoint): void
    {
        $this->pickUpPoint = $pickUpPoint;
    }


}
