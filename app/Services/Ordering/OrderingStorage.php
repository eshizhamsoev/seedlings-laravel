<?php

namespace App\Services\Ordering;

use App\Services\Ordering\Data\OrderData;
use Illuminate\Contracts\Session\Session;

class OrderingStorage
{
    const SESSION_KEY = 'order';
    private Session $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function save(OrderData $orderData)
    {
        $this->session->put(self::SESSION_KEY, $orderData);
    }

    public function getOrder(): ?OrderData
    {
        $data = $this->session->get(self::SESSION_KEY) ?? null;
        if ($data instanceof OrderData) {
            return $data;
        }
        return null;

    }
}
