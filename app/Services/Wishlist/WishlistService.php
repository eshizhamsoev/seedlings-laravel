<?php

namespace App\Services\Wishlist;

use App\Models\Data\Product\Product;
use Illuminate\Contracts\Session\Session;

class WishlistService
{

    const STORING_KEY = 'wishlist';

    /**
     * @var int[]
     */
    private array $items = [];
    private Session $session;

    // TODO implement storing for User
    public function __construct(Session $session)
    {
        $this->session = $session;
        $this->items = $this->getItems();
    }

    /**
     * @return int[]
     */
    public function getItems(): array
    {
        $items = $this->session->get(self::STORING_KEY) ?? [];
        return array_filter($items, fn($item) => is_int($item));
    }

    public function toggle(Product $product): void
    {
        if ($this->has($product)) {
            $this->remove($product);
        } else {
            $this->add($product);
        }
    }

    public function add(Product $product): void
    {
        if (!$this->has($product)) {
            $this->items[] = $product->id;
            $this->saveState();
        }
    }

    public function remove(Product $product): void
    {
        if ($this->has($product)) {
            $this->items = array_filter($this->items ?? [], fn(int $id) => $id !== $product->id);
            $this->saveState();
        }
    }

    private function saveState()
    {
        $this->session->put(self::STORING_KEY, $this->items);
    }


    public function has(Product $product): bool
    {
        return in_array($product->id, $this->items);
    }
}
