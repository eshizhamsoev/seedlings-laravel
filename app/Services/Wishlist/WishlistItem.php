<?php

namespace App\Services\Wishlist;

use App\Models\Data\Product\Product;
use Gloudemans\Shoppingcart\CartItem;

class WishlistItem
{
    private CartItem $cartItem;
    private ?Product $product;

    public function __construct(
        CartItem $cartItem
    ) {
        $this->cartItem = $cartItem;
        $this->product = $cartItem->model->product ?? null;
    }

    public function getPrice(): ?float
    {
        return $this->cartItem->model->price ?? null;
    }

    public function getOldPrice(): ?float
    {
        return $this->cartItem->model->old_price ?? null;
    }

    public function getOffer()
    {
        return $this->cartItem->model ?? null;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function getName()
    {
        return $this->cartItem->name;
    }

    public function getKey()
    {
        return $this->cartItem->rowId;
    }
}
