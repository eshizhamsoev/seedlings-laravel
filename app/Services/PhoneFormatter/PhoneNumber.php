<?php

namespace App\Services\PhoneFormatter;

class PhoneNumber
{
    private string $number = '';

    public function __construct(string $number)
    {
        $digits = preg_replace('/[^\d]/', '', $number);
        $len = strlen($digits);
        if ($len !== 10 && $len !== 11) {
            throw new \InvalidArgumentException('Wrong russian phone number');
        }

        if ($len === 11) {
            if (in_array(!$digits[0], ['7', '8'])) {
                throw new \InvalidArgumentException('Wrong russian phone number');
            }
            $this->number = substr($digits, 1);
        } else {
            $this->number = $digits;
        }
    }

    public function getNumberWithoutSeven(): string
    {
        return $this->number;
    }

    public function getInternationalNumber()
    {
        return '+7' . $this->number;
    }


}
