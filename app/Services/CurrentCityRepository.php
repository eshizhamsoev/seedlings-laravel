<?php

namespace App\Services;

use App\Models\Data\City;

class CurrentCityRepository
{
    private City $currentCity;

    public function init(City $city)
    {
        $this->currentCity = $city;
    }

    public function getCurrentCity(): City
    {
        return $this->currentCity;
    }
}
