<?php


namespace App\Services\UrlGenerator;


use App\Models\Website\Url;

interface Urlable
{
    public function getStringForUrl(): string;

    public function getParentUrl(): ?Url;
}
