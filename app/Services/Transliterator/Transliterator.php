<?php


namespace App\Services\Transliterator;

class Transliterator
{
    private \ElForastero\Transliterate\Transliterator $transliterator;

    public function __construct(\ElForastero\Transliterate\Transliterator $transliterator)
    {
        $this->transliterator = $transliterator;
    }

    public function transliterate($string): string
    {
        return $this->transliterator->slugify($string);
    }
}
