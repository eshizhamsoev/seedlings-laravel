<?php

namespace App\Services\PageRenderable;

use App\Enums\Website\Layout;

interface HasLayout
{
    public function getLayout(): Layout;
}
