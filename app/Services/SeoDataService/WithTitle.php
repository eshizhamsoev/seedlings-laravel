<?php

namespace App\Services\SeoDataService;

interface WithTitle
{
    public function getTitle(): string;
}
