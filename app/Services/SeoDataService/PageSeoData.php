<?php

namespace App\Services\SeoDataService;

class PageSeoData
{
    private ?string $metaTitle = null;
    private ?string $metaDescription = null;
    private ?string $metaKeywords = null;
    private ?string $ogImage = null;
    private ?string $ogTitle = null;
    private ?string $ogUrl = null;
    private ?string $ogDescription = null;
    private ?string $canonical = null;

    /**
     * @return string
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param  string|null  $metaDescription
     * @return PageSeoData
     */
    public function setMetaDescription(?string $metaDescription): PageSeoData
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param  string|null  $metaKeywords
     * @return PageSeoData
     */
    public function setMetaKeywords(?string $metaKeywords): PageSeoData
    {
        $this->metaKeywords = $metaKeywords;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgImage(): ?string
    {
        return $this->ogImage;
    }

    /**
     * @param  string|null  $ogImage
     * @return PageSeoData
     */
    public function setOgImage(?string $ogImage): PageSeoData
    {
        $this->ogImage = $ogImage;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgTitle(): ?string
    {
        return $this->ogTitle;
    }

    /**
     * @param  string|null  $ogTitle
     * @return PageSeoData
     */
    public function setOgTitle(?string $ogTitle): PageSeoData
    {
        $this->ogTitle = $ogTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgDescription(): ?string
    {
        return $this->ogDescription;
    }

    /**
     * @param  string|null  $ogDescription
     * @return PageSeoData
     */
    public function setOgDescription(?string $ogDescription): PageSeoData
    {
        $this->ogDescription = $ogDescription;
        return $this;
    }

    /**
     * @param  ?string  $metaTitle
     * @return PageSeoData
     */
    public function setMetaTitle(?string $metaTitle): PageSeoData
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgUrl(): ?string
    {
        return $this->ogUrl;
    }

    /**
     * @param  string|null  $ogUrl
     */
    public function setOgUrl(?string $ogUrl): void
    {
        $this->ogUrl = $ogUrl;
    }

    /**
     * @return string|null
     */
    public function getCanonical(): ?string
    {
        return $this->canonical;
    }

    /**
     * @param  string|null  $canonical
     */
    public function setCanonical(?string $canonical): void
    {
        $this->canonical = $canonical;
    }

}
