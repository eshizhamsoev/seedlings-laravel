<?php

namespace App\Services\SeoDataService;

use App\Models\Website\SeoData;
use App\Models\Website\Url;
use App\Services\CurrentCityRepository;
use Illuminate\Support\Facades\Storage;

class SeoDataService
{
    private CurrentCityRepository $currentCityRepository;
    private $disk;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
        $this->disk = Storage::disk('public');
    }

    public function getSeoData(Url $url): PageSeoData
    {
        $city = $this->currentCityRepository->getCurrentCity();
        $seoData = $url->seoData()->byCity($city)->first();

        $title = $seoData && $seoData->meta_title ? $seoData->meta_title : $this->getTitle($url);

        $pageSeoData = new PageSeoData();
        $pageSeoData->setMetaTitle($title);
        $pageSeoData->setOgUrl($this->getOgUrl($url, $seoData));

        if ($seoData) {
            $pageSeoData->setMetaDescription($seoData->meta_description);
            $pageSeoData->setMetaKeywords($seoData->meta_keywords);
            $pageSeoData->setOgTitle($seoData->og_title);
            if ($seoData->og_image && $this->disk->exists($seoData->og_image)) {
                $pageSeoData->setOgImage($this->disk->url($seoData->og_image));
            }
            $pageSeoData->setOgDescription($seoData->og_description);
            if ($seoData->canonical) {
                $pageSeoData->setCanonical(\url($seoData->canonical));
            }
        }

        return $pageSeoData;
    }

    private function getTitle(Url $url): ?string
    {
        if ($url->entity instanceof WithTitle) {
            return $url->entity->getTitle();
        }
        return null;
    }

    private function getOgUrl(Url $url, ?SeoData $seoData)
    {
        $ogUrl = $seoData && $seoData->og_url ? $seoData->og_url : $url->url;

        return \url($ogUrl);
    }
}
