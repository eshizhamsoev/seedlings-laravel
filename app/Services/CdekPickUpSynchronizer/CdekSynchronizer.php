<?php

namespace App\Services\CdekPickUpSynchronizer;

use AntistressStore\CdekSDK2\CdekClientV2;
use AntistressStore\CdekSDK2\Entity\Requests\DeliveryPoints;
use App\Enums\Order\PickUpPointType;
use App\Models\Data\Delivery\PickUpPoint;

class CdekSynchronizer
{
    private CdekClientV2 $cdekClient;

    public function __construct(CdekClientV2 $cdekClientV2)
    {
        $this->cdekClient = $cdekClientV2;
    }

    public function synchronize()
    {
        $dbPoints = PickUpPoint::where('type', PickUpPointType::CDEK)
            ->get()
            ->mapWithKeys(fn($item) => [
                $item->code => $item
            ]);

        $points = $this->cdekClient->getDeliveryPoints(
            (new DeliveryPoints())
                ->setCountryCode('RU')
                ->setMaxWeight(1)
                ->setHaveCashless(true)
                ->setHaveCash(true)
        );
        $updated = [];
        foreach ($points as $point) {
            $dbPoint = $dbPoints->get($point->getCode());
            if (!$dbPoint) {
                $dbPoint = new PickUpPoint();
                $dbPoint->code = $point->getCode();
                $dbPoint->type = PickUpPointType::CDEK;
            }

            $dbPoint->name = $point->getName();
            $dbPoint->city = $point->getLocation()->getCity();
            $dbPoint->address = $point->getLocation()->getAddress();
            $dbPoint->address_comment = $point->getAddressComment();
            $dbPoint->lat = $point->getLocation()->getLatitude();
            $dbPoint->lng = $point->getLocation()->getLongitude();

//            $dbPoint->address_full = $point->getLocation()->getAddressFull();
            $dbPoint->postal_code = $point->getLocation()->getPostalCode();

            $dbPoint->work_time = $point->getWorkTime();
            $dbPoint->max_weight = $point->getWeightMax();
            $dbPoint->status = false;

            $dimensions = $point->getDimensions();
            if ($dimensions) {
                $dbPoint->width = $dimensions['width'];
                $dbPoint->height = $dimensions['height'];
                $dbPoint->depth = $dimensions['depth'];
            }

            if ($dbPoint->isDirty()) {
                $dbPoint->save();
            }
            $updated[] = $dbPoint->id;
        }


        PickUpPoint::where('type', PickUpPointType::CDEK)
            ->whereNotIn('id', $updated)
            ->update([
                'status' => false
            ]);
    }
}
