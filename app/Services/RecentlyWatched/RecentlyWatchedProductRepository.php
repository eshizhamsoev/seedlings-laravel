<?php

namespace App\Services\RecentlyWatched;

use App\Models\Data\Product\Product;

class RecentlyWatchedProductRepository
{
    const SESSION_KEY = 'recently_watched_products';

    public function addProduct(Product $product)
    {
        $productIds = session(self::SESSION_KEY, []);
        $productIds[] = $product->id;
        session()->put(self::SESSION_KEY, $productIds);
    }

    public function getProductIds(?int $limit = null, ?int $excludeId = null): array
    {
        $productIds = session(self::SESSION_KEY);
        if (!$productIds) {
            return [];
        }
        if ($excludeId) {
            $productIds = array_diff($productIds, [$excludeId]);
        }
        if ($limit) {
            $productIds = array_slice($productIds, -$limit);
        }

        return $productIds;

    }
}
