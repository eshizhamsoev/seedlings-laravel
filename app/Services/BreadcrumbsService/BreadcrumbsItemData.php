<?php

namespace App\Services\BreadcrumbsService;

use App\Models\Website\Url;

class BreadcrumbsItemData
{
    private string $name;
    private Url $url;

    public function __construct(string $name, Url $url)
    {
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->url->url;
    }


}
