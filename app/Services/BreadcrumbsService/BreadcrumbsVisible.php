<?php

namespace App\Services\BreadcrumbsService;

use App\Services\UrlGenerator\Urlable;

interface BreadcrumbsVisible extends Urlable
{
    public function getNameForBreadcrumbs(): string;
}
