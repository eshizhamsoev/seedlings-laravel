<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

/**
 * @method static static CITY_LIST()
 * @method static static FANCY_LOGIN()
 * @method static static FANCY_REGISTRATION()
 * @method static static FANCY_RECOVERY()
 * @method static static ADD_TO_CART()
 */
class ModalType extends Enum
{
    public const CITY_LIST = 'city-list';
    public const FANCY_LOGIN = 'fancy-login';
    public const FANCY_REGISTRATION = 'fancy-registration';
    public const FANCY_RECOVERY = 'fancy-recovery';
    public const ADD_TO_CART = 'add-to-cart';
}
