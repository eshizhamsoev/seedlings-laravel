<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

/**
 * @method static static COMMON()
 * @method static static INNER()
 * @method static static HEADLESS()
 */
class Layout extends Enum
{
    const COMMON = 'common';
    const INNER = 'inner';
    const HEADLESS = 'headless';
}
