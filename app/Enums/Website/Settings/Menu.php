<?php

namespace App\Enums\Website\Settings;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static TOP()
 * @method static static TOP_MORE()
 * @method static static FOOTER()
 * @method static static PAGE_BOTTOM()
 */
class Menu extends Enum implements LocalizedEnum
{
    const TOP = 'menu_top';
    const TOP_MORE = 'menu_top_more';
    const FOOTER = 'menu_footer';
    const PAGE_BOTTOM = 'menu_page_bottom';
}
