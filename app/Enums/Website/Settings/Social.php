<?php

namespace App\Enums\Website\Settings;

use App\Enums\Website\SvgIconName;
use BenSampo\Enum\Enum;

/**
 * @method static static YOUTUBE()
 * @method static static INSTAGRAM()
 * @method static static FACEBOOK()
 * @method static static VK()
 * @method static static TIKTOK()
 */
class Social extends Enum
{
    public const YOUTUBE = 'social_youtube';
    public const INSTAGRAM = 'social_instagram';
    public const FACEBOOK = 'social_facebook';
    public const VK = 'social_vk';
    public const TIKTOK = 'social_tiktok';

    public function getIcon(): SvgIconName
    {
        switch ($this->value){
            case self::YOUTUBE:
                return SvgIconName::YOUTUBE();
            case self::INSTAGRAM:
                return SvgIconName::INSTAGRAM();
            case self::FACEBOOK:
                return SvgIconName::FACEBOOK();
            case self::VK:
                return SvgIconName::VK();
            case self::TIKTOK:
                return SvgIconName::TIK_TOK();
        }
        throw new \LogicException('Unknown social type');
    }
}
