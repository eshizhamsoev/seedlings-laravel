<?php

namespace App\Enums\Website\Settings;

use BenSampo\Enum\Enum;

class CodePosition extends Enum
{
    public const HEAD = 'head';
    public const BODY_START = 'body-start';
    public const BODY_END = 'body-end';

}
