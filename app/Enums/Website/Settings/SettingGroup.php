<?php

namespace App\Enums\Website\Settings;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static PAGES()
 * @method static static MENU()
 * @method static static SOCIAL()
 */
class SettingGroup extends Enum implements LocalizedEnum
{
    public const PAGES = 'pages';
    public const MENU = 'menu';
    public const SOCIAL = 'social';
    public const CODE_POSITIONS = 'code-positions';
}
