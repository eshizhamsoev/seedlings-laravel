<?php

namespace App\Enums\Website\Settings;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static PRIVACY_POLICY()
 * @method static static CART()
 * @method static static CHECKOUT()
 * @method static static LOGIN()
 * @method static static ACCOUNT()
 * @method static static CATALOG()
 * @method static static CONTACTS()
 * @method static static BLOG()
 */
class SpecialPage extends Enum implements LocalizedEnum
{
    const PRIVACY_POLICY = 'page_privacy_policy';
    const CART = 'page_cart';
    const WISHLIST = 'page_wishlist';
    const CHECKOUT = 'page_checkout';
    const LOGIN = 'page_login';
    const ACCOUNT = 'page_account';
    const SEARCH = 'page_search';
    const CATALOG = 'page_catalog';
    const CONTACTS = 'page_contacts';
    const BLOG = 'page_blog';
}
