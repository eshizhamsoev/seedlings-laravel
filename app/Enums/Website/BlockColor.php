<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

/**
 * @method static static GREEN()
 * @method static static GRAY()
 */
class BlockColor extends Enum
{
    public const GREEN = 'green';
    public const GRAY = 'grey';
}
