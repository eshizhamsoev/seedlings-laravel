<?php

namespace App\Enums\Website;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static CATALOG()
 * @method static static FEATURED_CATEGORIES()
 * @method static static PRODUCT_SLIDER()
 * @method static static PRODUCT_GRID()
 * @method static static REVIEWS()
 * @method static static HTML_CODE()
 * @method static static SUBSCRIBE()
 * @method static static CART()
 * @method static static CHECKOUT()
 * @method static static BLOG()
 */
class BlockType extends Enum implements LocalizedEnum
{
    public const CATALOG = 'catalog';
    public const FEATURED_CATEGORIES = 'featured-categories';
    public const PRODUCT_SLIDER = 'product-slider';
    public const PRODUCT_GRID = 'product-grid';
    public const REVIEWS = 'reviews';
    public const HTML_CODE = 'html-code';
    public const SUBSCRIBE = 'subscribe';
    public const CART = 'cart';
    public const CHECKOUT = 'checkout';
    public const BLOG = 'blog';
    public const NEWS_SLIDER = 'news-slider';
    public const WISHLIST = 'wishlist';
    public const BANNER = 'banner';
}
