<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;
use hanneskod\classtools\Exception\LogicException;

/**
 * @method static static EMPTY_CART()
 * @method static static BOXES()
 * @method static static ERROR_404()
 */
class InformerImage extends Enum
{
    public const EMPTY_CART = 'empty-cart';
    public const BOXES = 'boxes';
    public const ERROR_404 = 'error-404';


    public function getImage(): array
    {
        switch ($this->value) {
            case self::EMPTY_CART:
                return [
                    'src' => asset('/static/images/common/informer/empty-cart.png'),
                    'width' => 616,
                    'height' => 362
                ];
            case self::BOXES:
                return [
                    'src' => asset('/static/images/common/informer/boxes.png'),
                    'width' => 372,
                    'height' => 342
                ];
            case self::ERROR_404:
                return [
                    'src' => asset('/static/images/common/informer/404.png'),
                    'width' => 649,
                    'height' => 347
                ];
        }
        throw new LogicException('Not existing image');
    }
}
