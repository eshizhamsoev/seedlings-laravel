<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

class AttributeType extends Enum
{
    public const TEXT = 'text';
    public const ENUM = 'enum';
    public const NUMBER = 'number';
}
