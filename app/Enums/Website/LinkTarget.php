<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

/**
 * @method static static BLANK()
 * @method static static SELF()
 */
class LinkTarget extends Enum
{
    public const BLANK = '_blank';
    public const SELF = '_self';
}
