<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;


/**
 * @method static static TRIANGLE()
 * @method static static BTN_MENU()
 * @method static static SEARCH()
 * @method static static HEART()
 * @method static static USER()
 * @method static static BASKET()
 * @method static static CARBON()
 * @method static static CLOCK()
 * @method static static MAIL()
 * @method static static PHONE()
 * @method static static ARROW_DOWN()
 * @method static static ARROW()
 * @method static static FILTER_FILLED()
 * @method static static BTN_FOUR()
 * @method static static BTN_TWO()
 * @method static static GRAPH()
 * @method static static STAR()
 * @method static static DELETE()
 * @method static static PLUS()
 * @method static static MINUS()
 * @method static static BOX()
 * @method static static CHECK()
 * @method static static CHECK_NOT()
 * @method static static CREDIT_CARD()
 * @method static static SHARE()
 * @method static static CART()
 * @method static static ARROW_BACK()
 * @method static static LOCATION()
 * @method static static ANT_PHONE()
 * @method static static HEART_FILLED()
 * @method static static PLAY()
 * @method static static BOX_OPEN()
 * @method static static CREDIT_CARD_FRONT()
 * @method static static DOOR()
 * @method static static OFFICE_BUILDING()
 * @method static static BOOK()
 * @method static static EDIT()
 * @method static static CHECK_MARK_DONE()
 * @method static static CHECK_MARK_FAIL()
 * @method static static CHECK_MARK_UNKNOWN()
 * @method static static ARROW_LONG()
 * @method static static NUMBER()
 * @method static static SALE()
 * @method static static USER_FILL()
 * @method static static GOOGLE()
 * @method static static YANDEX()
 * @method static static ADDRESS()
 * @method static static EXIT()
 * @method static static HISTORY()
 * @method static static PASSWORD()
 * @method static static SALE_LABEL()
 * @method static static SHARE_OTHER()
 * @method static static TELEGRAM()
 * @method static static TELEPHONE()
 * @method static static VIBER()
 * @method static static WHATSAPP()
 * @method static static CLOSE()
 * @method static static TIK_TOK()
 * @method static static VK()
 * @method static static YOUTUBE()
 * @method static static FACEBOOK()
 * @method static static INSTAGRAM()
 */
class SvgIconName extends Enum
{
    const TRIANGLE = 'triangle';
    const BTN_MENU = 'btn-menu';
    const SEARCH = 'search';
    const HEART = 'heart';
    const USER = 'user';
    const BASKET = 'basket';
    const CARBON = 'carbon';
    const CLOCK = 'clock';
    const MAIL = 'mail';
    const PHONE = 'phone';
    const ARROW_DOWN = 'arrow-down';
    const ARROW = 'arrow';
    const FILTER_FILLED = 'filter-filled';
    const BTN_FOUR = 'btn-four';
    const BTN_TWO = 'btn-two';
    const GRAPH = 'graph';
    const STAR = 'star';
    const DELETE = 'delete';
    const PLUS = 'plus';
    const MINUS = 'minus';
    const BOX = 'box';
    const CHECK = 'check';
    const CHECK_NOT = 'check-not';
    const CREDIT_CARD = 'credit-card';
    const SHARE = 'share';
    const CART = 'cart';
    const ARROW_BACK = 'arrow-back';
    const LOCATION = 'location';
    const ANT_PHONE = 'ant-phone';
    const HEART_FILLED = 'heart-filled';
    const PLAY = 'play';
    const BOX_OPEN = 'box-open';
    const CREDIT_CARD_FRONT = 'credit-card-front';
    const DOOR = 'door';
    const OFFICE_BUILDING = 'office-building';
    const BOOK = 'book';
    const EDIT = 'edit';
    const CHECK_MARK_DONE = 'check-mark-done';
    const CHECK_MARK_FAIL = 'check-mark-fail';
    const CHECK_MARK_UNKNOWN = 'check-mark-unknown';
    const ARROW_LONG = 'arrow-long';
    const NUMBER = 'number';
    const SALE = 'sale';
    const USER_FILL = 'user-fill';
    const GOOGLE = 'google';
    const YANDEX = 'yandex';
    const ADDRESS = 'address';
    const EXIT = 'exit';
    const HISTORY = 'history';
    const PASSWORD = 'password';
    const SALE_LABEL = 'sale-label';
    const SHARE_OTHER = 'share-other';
    const TELEGRAM = 'telegram';
    const TELEPHONE = 'telephone';
    const VIBER = 'viber';
    const WHATSAPP = 'whatsapp';
    const CLOSE = 'close';

    const TIK_TOK = 'tik-tok';
    const VK = 'vk';
    const YOUTUBE = 'youtube';
    const FACEBOOK = 'facebook';
    const INSTAGRAM = 'instagram';


    public function getRewrite(): ?string
    {
        switch ($this->value) {
            case self::INSTAGRAM:
                return asset('/static/images/common/instagram.svg');
            default:
                return null;
        }
    }
}
