<?php

namespace App\Enums\Website\Forms;

use BenSampo\Enum\Enum;

/**
 * @method static static TEXT()
 * @method static static NUMBER()
 * @method static static EMAIL()
 * @method static static TEL()
 * @method static static PASSWORD()
 */
class TextFieldType extends Enum
{
    public const TEXT = 'text';
    public const NUMBER = 'number';
    public const EMAIL = 'email';
    public const TEL = 'tel';
    public const PASSWORD = 'password';
}
