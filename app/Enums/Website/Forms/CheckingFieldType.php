<?php

namespace App\Enums\Website\Forms;

use BenSampo\Enum\Enum;

/**
 * @method static static CHECKBOX()
 * @method static static RADIO()
 * @method static static TEXT()
 * @method static static NUMBER()
 */
class CheckingFieldType extends Enum
{
    public const CHECKBOX = 'checkbox';
    public const RADIO = 'radio';
}
