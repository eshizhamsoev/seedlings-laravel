<?php

namespace App\Enums\Catalog;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static SIMPLE()
 * @method static static VARIANT()
 */
class OfferType extends Enum implements LocalizedEnum
{
    public const SIMPLE = 'simple';
    public const VARIANT = 'variant';

}
