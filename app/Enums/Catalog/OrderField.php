<?php

namespace App\Enums\Catalog;

use BenSampo\Enum\Enum;

/**
 * @method static static PRICE()
 */
class OrderField extends Enum
{
    public const PRICE = 'minPrice';
    public const PRIORITY = 'priority';
}
