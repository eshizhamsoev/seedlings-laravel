<?php

namespace App\Enums\Catalog;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static SPECIAL()
 * @method static static NEW()
 * @method static static BESTSELLER()
 */
class ProductLabel extends Enum implements LocalizedEnum
{
    public const SPECIAL = 'special';
    public const NEW = 'new';
    public const BESTSELLER = 'bestseller';

}
