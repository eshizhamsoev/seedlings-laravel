<?php

namespace App\Enums\Catalog;

use BenSampo\Enum\Enum;

/**
 * @method static static ASC()
 * @method static static DESC()
 */
class OrderAscending extends Enum
{
    public const ASC = 'asc';
    public const DESC = 'desc';
}
