<?php

namespace App\Enums\Catalog;

use BenSampo\Enum\Enum;

/**
 * @method static static GRID()
 * @method static static LIST()
 */
class ProductsView extends Enum
{
    public const GRID = 'grid';
    public const LIST = 'list';
}
