<?php

namespace App\Enums\Catalog;

use BenSampo\Enum\Enum;

/**
 * @method static static FIELD()
 * @method static static ASCENDING()
 */
class OrderParamName extends Enum
{
    public const FIELD = 'order_field';
    public const ASCENDING = 'order_asc';
}
