<?php

namespace App\Enums\Order;

use BenSampo\Enum\Enum;

/**
 * @method static static ONLINE()
 * @method static static ON_PLACE()
 */
class PaymentMethod extends Enum
{
    public const ONLINE = 'online';
    public const ON_PLACE = 'on-place';
}
