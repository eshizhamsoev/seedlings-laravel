<?php

namespace App\Enums\Order;

use BenSampo\Enum\Enum;

/**
 * @method static static TO_DOOR()
 * @method static static PICKUP()
 */
class DeliveryType extends Enum
{
    public const TO_DOOR = 'to-door';
    public const PICKUP = 'pickup';
}
