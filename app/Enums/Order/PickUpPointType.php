<?php

namespace App\Enums\Order;

use BenSampo\Enum\Enum;

/**
 * @method static static CDEK()
 */
class PickUpPointType extends Enum
{
    public const CDEK = 'cdek';
}
