<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static PAGE()
 * @method static static CATEGORY()
 * @method static static PRODUCT()
 * @method static static ARTICLE()
 */
final class EntityType extends Enum implements LocalizedEnum
{
    const PAGE = 'page';
    const CATEGORY = 'category';
    const PRODUCT = 'product';
    const ARTICLE = 'article';
}
