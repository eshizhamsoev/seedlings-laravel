<?php

namespace App\Events;

use App\Models\Website\Url;
use Illuminate\Foundation\Events\Dispatchable;

class ViewEvent
{
    use Dispatchable;

    private Url $url;

    public function __construct(Url $url)
    {
        $this->url = $url;
    }

    /**
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }
}
