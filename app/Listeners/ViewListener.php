<?php

namespace App\Listeners;

use App\Events\ViewEvent;
use App\Models\Data\Product\Product;
use App\Services\RecentlyWatched\RecentlyWatchedProductRepository;

class ViewListener
{
    private RecentlyWatchedProductRepository $recentlyWatchedProductRepository;

    public function __construct(RecentlyWatchedProductRepository $recentlyWatchedProductRepository)
    {
        $this->recentlyWatchedProductRepository = $recentlyWatchedProductRepository;
    }

    public function handle(ViewEvent $event)
    {
        if ($event->getUrl()->entity instanceof Product) {
            $this->recentlyWatchedProductRepository->addProduct($event->getUrl()->entity);
        }
    }
}
