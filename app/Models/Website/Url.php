<?php

namespace App\Models\Website;

use App\Models\Data\City;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    use HasFactory;

    public function entity()
    {
        return $this->morphTo();
    }

    public function seoData()
    {
        return $this->hasMany(SeoData::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class, 'url_to_city');
    }
}
