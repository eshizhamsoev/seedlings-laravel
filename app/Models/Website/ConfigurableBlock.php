<?php

namespace App\Models\Website;

use App\Enums\Website\BlockType;
use Illuminate\Database\Eloquent\Model;

class ConfigurableBlock extends Model
{
    protected $casts = [
        'type' => BlockType::class,
        'settings' => 'json'
    ];

    public function pages()
    {
        return $this->belongsToMany(Page::class, 'page_blocks');
    }
}
