<?php

namespace App\Models\Website\Banner;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    const DIMENSION_NAME = 'name';
    const DIMENSION_WIDTH = 'width';
    const DIMENSION_HEIGHT = 'height';
    const DIMENSION_MINIMUM_SCREEN_WIDTH = 'minimum_screen_width';

    protected $casts = [
        'dimensions' => 'json'
    ];

    public function items()
    {
        return $this->hasMany(BannerItem::class);
    }

    /**
     * @return DimensionData[]
     */
    public function getDimensions(): array
    {
        $data = $this->dimensions;
        if (!$data) {
            return [];
        }

        $items = [];
        foreach ($data as $item) {
            $dimensionName = $item[self::DIMENSION_NAME] ?? null;
            $dimensionWidth = $item[self::DIMENSION_WIDTH] ?? null;
            $dimensionHeight = $item[self::DIMENSION_HEIGHT] ?? null;
            $dimensionMinimumScreenWidth = $item[self::DIMENSION_MINIMUM_SCREEN_WIDTH] ?? null;
            if (
                !$dimensionName
                || !$dimensionWidth
                || !$dimensionHeight
                || is_null($dimensionMinimumScreenWidth)
            ) {
                continue;
            }
            $items[] = new DimensionData(
                $dimensionName,
                $dimensionWidth,
                $dimensionHeight,
                $dimensionMinimumScreenWidth
            );
        }

        return $items;
    }
}
