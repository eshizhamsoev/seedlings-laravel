<?php

namespace App\Models\Website\Banner;

class DimensionData
{
    private string $name;
    private int $width;
    private int $height;
    private int $minimumScreenWidth;

    public function __construct(string $name, int $width, int $height, int $minimumScreenWidth)
    {
        $this->name = $name;
        $this->width = $width;
        $this->height = $height;
        $this->minimumScreenWidth = $minimumScreenWidth;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getMinimumScreenWidth(): int
    {
        return $this->minimumScreenWidth;
    }
}
