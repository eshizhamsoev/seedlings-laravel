<?php

namespace App\Models\Website\Banner;

use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class BannerItem extends Model implements HasMedia
{
    use InteractsWithMedia, CroppedResponsiveImage;

    public function banner()
    {
        return $this->belongsTo(Banner::class);
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $banner = $media->model->banner ?? null;
        if($banner instanceof Banner) {
            foreach ($banner->getDimensions() as $dimension) {
                $conversion = $this
                    ->addMediaConversion($dimension->getName())
                    ->performOnCollections($dimension->getName());

                if (!$media->hasGeneratedConversion($dimension->getName())) {
                    $this->responsiveCrop(
                        $conversion,
                        $media,
                        $dimension->getName(),
                        $dimension->getWidth(),
                        $dimension->getHeight()
                    );
                }
            }
        }
    }


    public function registerMediaCollections(): void
    {
        if($this->banner) {
            foreach ($this->banner->getDimensions() as $dimension) {
                $this->addMediaCollection($dimension->getName())->singleFile();
            }
        }
    }
}
