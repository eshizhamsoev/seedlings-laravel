<?php

namespace App\Models\Website;

use App\Enums\Website\Layout;
use App\Services\BreadcrumbsService\BreadcrumbsVisible;
use App\Services\PageRenderable\HasLayout;
use App\Services\SeoDataService\WithTitle;
use App\Services\UrlGenerator\Urlable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model implements Urlable, BreadcrumbsVisible, WithTitle, HasLayout
{
    use HasFactory;

    protected $casts = [
        'layout' => Layout::class
    ];

    public function parent()
    {
        return $this->belongsTo(Page::class);
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function blocks()
    {
        return $this->belongsToMany(ConfigurableBlock::class, 'page_blocks')
            ->orderBy('page_blocks.sort_order')
            ->withPivot(['sort_order', 'color'])
            ->using(PageBlock::class);
    }

//    public function page_content()
//    {
//        return $this->hasOne(PageContent::class);
//    }

    public function getStringForUrl(): string
    {
        return $this->name;
    }

    public function getParentUrl(): ?Url
    {
        return $this->parent ? $this->parent->url : null;
    }

    public function getNameForBreadcrumbs(): string
    {
        return $this->name;
    }

    public function getTitle(): string
    {
        return $this->name;
    }

    public function getLayout(): Layout
    {
        return $this->layout;
    }
}
