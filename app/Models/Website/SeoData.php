<?php

namespace App\Models\Website;

use App\Models\Data\City;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoData extends Model
{
    use HasFactory;

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function url()
    {
        return $this->belongsTo(Url::class);
    }

    public function scopeByCity($query, City $city)
    {
        return $query->where('city_id', $city->id);
    }
}
