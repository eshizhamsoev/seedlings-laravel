<?php

namespace App\Models\Data\Product\Attributes;

use App\Enums\Website\AttributeType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $casts = [
        'attribute_settings' => 'json',
        'elastic_settings' => 'json',
        'type' => AttributeType::class
    ];

    public function scopeFilterable(Builder $query)
    {
        return $query->where('filterable', true);
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }
}
