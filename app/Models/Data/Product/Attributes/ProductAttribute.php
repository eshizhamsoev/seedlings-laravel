<?php

namespace App\Models\Data\Product\Attributes;

use App\Models\Data\Product\Product;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    public $table = 'product_attributes';
    public $incrementing = true;

    public function attributeValue()
    {
        return $this->belongsTo(AttributeValue::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function getValue()
    {
        return $this->attributeValue ? $this->attributeValue->value : $this->value;
    }
}
