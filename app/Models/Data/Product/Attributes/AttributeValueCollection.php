<?php

namespace App\Models\Data\Product\Attributes;

use Illuminate\Database\Eloquent\Model;

class AttributeValueCollection extends Model
{
    public function values()
    {
        return $this->hasMany(AttributeValue::class, 'attribute_value_collection_id');
    }
}
