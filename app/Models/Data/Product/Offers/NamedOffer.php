<?php

namespace App\Models\Data\Product\Offers;

use App\Models\Data\Product\ProductTypes\VariantProduct;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class NamedOffer extends Model implements Buyable
{
    public function product()
    {
        return $this->belongsTo(VariantProduct::class);
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->product->name . ' ' . $this->name;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    public function getBuyableWeight($options = null)
    {
        return 0;
    }
}
