<?php

namespace App\Models\Data\Product\Offers;

use App\Models\Data\Product\ProductTypes\SimpleProduct;
use App\Services\Cart\ProfitCalculator;
use App\Services\Cart\SaleProfitData;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class SimpleOffer extends Model implements Buyable
{
    public function product()
    {
        return $this->belongsTo(SimpleProduct::class);
    }

    public function getSaleProfit(): ?SaleProfitData
    {
        $calc = new ProfitCalculator();
        return $calc->calculate($this->price, $this->old_price);
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->product->name . ' ' . $this->name;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    public function getBuyableWeight($options = null)
    {
        return 0;
    }
}
