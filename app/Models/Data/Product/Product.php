<?php

namespace App\Models\Data\Product;

use App\Enums\Catalog\OfferType;
use App\Enums\Catalog\ProductLabel;
use App\Enums\Website\Layout;
use App\Models\Data\Category\Category;
use App\Models\Data\Product\Attributes\Attribute;
use App\Models\Data\Product\Attributes\ProductAttribute;
use App\Models\Data\Product\ProductTypes\SimpleProduct;
use App\Models\Data\Product\ProductTypes\VariantProduct;
use App\Models\Data\Review\Review;
use App\Models\Website\Url;
use App\Services\PageRenderable\HasLayout;
use App\Services\UrlGenerator\Urlable;
use App\Support\CroppedResponsiveImage;
use ElasticScoutDriverPlus\Searchable;
use Illuminate\Database\Eloquent\Model;
use Parental\HasChildren;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Product extends Model implements HasMedia, Urlable, HasLayout
{
    use Searchable;
    use HasChildren;
    use InteractsWithMedia;
    use CroppedResponsiveImage;

    public const IMAGE_COLLECTION_MAIN = 'main';

    public const IMAGE_CONVERSION_DETAILS_LARGE = 'details';
    public const IMAGE_CONVERSION_DETAILS_POPUP = 'popup';

    protected $childTypes = [
        OfferType::SIMPLE => SimpleProduct::class,
        OfferType::VARIANT => VariantProduct::class,
    ];

    protected $casts = [
        'label' => ProductLabel::class
    ];

    protected $fillable = ['type'];

    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class)
            ->with(['attribute', 'attributeValue']);
    }

    public function filterableAttributes()
    {
        return $this->attributes()
            ->whereHas('attribute', fn($query) => $query->filterable());
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_products');
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function reviews()
    {
        return $this->belongsToMany(Review::class, 'product_review');
    }

    public function getStringForUrl(): string
    {
        return $this->id . ' ' . $this->name;
    }

    public function getParentUrl(): ?Url
    {
        return null;
    }

    public function relatedProducts()
    {
        return $this->belongsToMany(Product::class, 'related_products', 'product_id', 'related_id');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_DETAILS_LARGE)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_MAIN, 480, 640);


        $this->addMediaConversion(self::IMAGE_CONVERSION_DETAILS_POPUP)
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();

    }

    public function getLayout(): Layout
    {
        return Layout::COMMON();
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_MAIN);
    }

    public function toSearchableArray()
    {
        $attributes = $this->filterableAttributes->mapWithKeys(fn(ProductAttribute $attribute) => [
            $attribute->id => $attribute->getValue()
        ]);
        return array_merge([
            'id' => $this->id,
            'name' => $this->name,
            'minPrice' => $this->offer->price,
            'filterAttributes' => $attributes,
            'categoryId' => $this->categories->pluck('id'),
        ]);
    }

    public function searchableWith()
    {
        return ['categories', 'filterableAttributes'];
    }
}
