<?php

namespace App\Models\Data\Product\ProductTypes;

use App\Models\Data\Product\Offers\NamedOffer;
use App\Models\Data\Product\Product;
use Parental\HasParent;

class VariantProduct extends Product
{
    use HasParent;

    public function offers()
    {
        return $this->hasMany(NamedOffer::class, 'product_id')->orderBy('price');
    }

    public function getPriceTextAttribute(): string
    {
        $offer = $this->offers->first();
        return $offer ? 'от ' . $offer->price : '';
    }
}
