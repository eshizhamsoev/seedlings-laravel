<?php

namespace App\Models\Data\Product\ProductTypes;

use App\Models\Data\Product\Attributes\Attribute;
use App\Models\Data\Product\Offers\SimpleOffer;
use App\Models\Data\Product\Product;
use Parental\HasParent;

class SimpleProduct extends Product
{
    use HasParent;

    public function offer()
    {
        return $this->hasOne(SimpleOffer::class, 'product_id');
    }

    public function getPriceTextAttribute(): string
    {
        return $this->offer ? $this->offer->price : '';
    }



    public function toSearchableArray()
    {
        return array_merge(parent::toSearchableArray(), [
            'minPrice' => $this->offer->price,
        ]);
    }
}
