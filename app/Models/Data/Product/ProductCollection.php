<?php

namespace App\Models\Data\Product;

use App\Services\Catalog\CatalogQuerySettings;
use Illuminate\Database\Eloquent\Model;

class ProductCollection extends Model
{
    public const PRODUCTS_FIELD = 'products';
    public const CATEGORIES_FIELD = 'categories';
    public const ONLY_SALE_FIELD = 'only_sale_field';
    public const LABELS_FIELD = 'labels_field';

    public $casts = [
        'settings' => 'json'
    ];

    public function getSettings(): CatalogQuerySettings
    {
        $settings = new CatalogQuerySettings();
        if (!$this->settings) {
            return $settings;
        }
        if (!empty($this->settings[self::CATEGORIES_FIELD])) {
            $settings->setCategoryIds($this->settings[self::CATEGORIES_FIELD]);
        }
        if (!empty($this->settings[self::PRODUCTS_FIELD])) {
            $settings->setProductIds($this->settings[self::PRODUCTS_FIELD]);
        }
        if (!empty($this->settings[self::LABELS_FIELD])) {
            $settings->setLabels($this->settings[self::LABELS_FIELD]);
        }
        if (!empty($this->settings[self::ONLY_SALE_FIELD])) {
            $settings->setOnlySale($this->settings[self::ONLY_SALE_FIELD]);
        }
        return $settings;
    }
}
