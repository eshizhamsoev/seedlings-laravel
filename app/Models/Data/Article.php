<?php

namespace App\Models\Data;

use App\Enums\Website\Layout;
use App\Enums\Website\Settings\SpecialPage;
use App\GlobalScopes\IsEnabledScope;
use App\Models\Website\Page;
use App\Models\Website\Url;
use App\Services\BreadcrumbsService\BreadcrumbsVisible;
use App\Services\UrlGenerator\Urlable;
use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Parental\HasChildren;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Article extends Model implements HasMedia, Urlable, BreadcrumbsVisible
{
    use HasFactory;
    use HasChildren;
    use InteractsWithMedia;
    use CroppedResponsiveImage;

    protected $casts = [
        'publication_date' => 'date'
    ];

    public const IMAGE_COLLECTION_MAIN = 'main';

    public const IMAGE_CONVERSION_CARD = 'card';

    public const IMAGE_CONVERSION_SLIDE = 'slide';

    protected static function booted()
    {
        static::addGlobalScope(new IsEnabledScope);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_CARD)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_MAIN, 370, 274);

        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_SLIDE)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_MAIN, 270, 200);
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_MAIN);
    }

    public function getLayout(): Layout
    {
        return Layout::COMMON();
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function getStringForUrl(): string
    {
        return $this->id . ' ' . $this->title;
    }

    public function getParentUrl(): ?Url
    {
        return Page::findOrFail(nova_get_setting(SpecialPage::BLOG))->url;
    }

    public function getNameForBreadcrumbs(): string
    {
        return $this->title;
    }
}
