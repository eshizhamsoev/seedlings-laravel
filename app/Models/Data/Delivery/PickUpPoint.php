<?php

namespace App\Models\Data\Delivery;

use App\Enums\Order\PickUpPointType;
use Illuminate\Database\Eloquent\Model;

class PickUpPoint extends Model
{
    protected $casts = [
        'type' => PickUpPointType::class
    ];
}
