<?php

namespace App\Models\Data\Review;

use App\Models\Data\Product\Product;
use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Review extends Model implements HasMedia
{
    const IMAGE_AUTHOR_COLLECTION = 'author';
    const IMAGE_AUTHOR_MAIN_CONVERSION = 'main';

    use InteractsWithMedia;
    use CroppedResponsiveImage;

    protected $casts = [
        'date' => 'date'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $conversion = $this
            ->addMediaConversion(self::IMAGE_AUTHOR_MAIN_CONVERSION)
            ->performOnCollections(self::IMAGE_AUTHOR_COLLECTION);
        $this->responsiveCrop($conversion, $media, self::IMAGE_AUTHOR_COLLECTION, 65, 65);

    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_AUTHOR_COLLECTION)
            ->singleFile();
    }

    public function answer()
    {
        return $this->hasOne(ReviewAnswer::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_review');
    }
}
