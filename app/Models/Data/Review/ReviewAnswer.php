<?php

namespace App\Models\Data\Review;

use Illuminate\Database\Eloquent\Model;

class ReviewAnswer extends Model
{
    protected $casts = [
        'date' => 'date'
    ];
}
