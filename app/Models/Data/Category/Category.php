<?php

namespace App\Models\Data\Category;

use App\Enums\Website\Layout;
use App\Models\Data\Product\Product;
use App\Models\Website\Url;
use App\Services\BreadcrumbsService\BreadcrumbsVisible;
use App\Services\PageRenderable\HasLayout;
use App\Services\UrlGenerator\Urlable;
use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Novius\LaravelNovaOrderNestedsetField\Traits\Orderable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Category extends Model implements Urlable, BreadcrumbsVisible, HasMedia, HasLayout
{
    use NodeTrait;
    use Orderable;
    use InteractsWithMedia;
    use CroppedResponsiveImage;

    public const IMAGE_COLLECTION_MAIN = 'main';
    public const IMAGE_CONVERSION_LIST = 'list';

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_products');
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function getStringForUrl(): string
    {
        return $this->name;
    }

    public function getParentUrl(): ?Url
    {
        if (!$this->parent) {
            return null;
        }

        return $this->parent->url;
    }

    public function getLayout(): Layout
    {
        return Layout::COMMON();
    }

    public function getNameForBreadcrumbs(): string
    {
        return $this->name;
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_LIST)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_MAIN, 370, 275);

    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_MAIN)
            ->singleFile();
    }
}
