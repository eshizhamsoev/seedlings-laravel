<?php

namespace App\Models\Data\Order;

use App\Models\Data\Product\Product;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $casts = [
        'additional_params' => 'json'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function offer()
    {
        return $this->morphTo();
    }
}
