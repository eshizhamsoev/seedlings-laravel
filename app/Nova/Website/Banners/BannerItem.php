<?php

namespace App\Nova\Website\Banners;

use App\Enums\Website\BlockColor;
use App\Enums\Website\Layout;
use App\Models\Data\Product\Product as ProductModel;
use App\Models\Website\Banner\BannerItem as BannerItemModel;
use App\Nova\Resource;
use App\Nova\Website\ConfigurableNovaBlock;
use App\Nova\Website\Page;
use App\Nova\Website\Url;
use App\View\Data\Common\MenuItemData;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use R64\NovaFields\Row;
use SimpleSquid\Nova\Fields\Enum\Enum;

class BannerItem extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = BannerItemModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $group = 'Website';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $images = [];
        $banner = $this->model()->banner;
        if ($banner) {
            foreach ($banner->getDimensions() as $dimension) {
                $images[] = Images::make($dimension->getName(), $dimension->getName())
                    ->conversionOnIndexView($dimension->getName())
                    ->conversionOnDetailView($dimension->getName());
            }
        }

        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->required(),
            Markdown::make('Description')->required(),
            ...$images
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
