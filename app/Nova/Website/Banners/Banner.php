<?php

namespace App\Nova\Website\Banners;

use App\Models\Website\Banner\Banner as BannerModel;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use R64\NovaFields\Number;
use R64\NovaFields\Row;

class Banner extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = BannerModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $group = 'Website';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->required(),
            Row::make('Dimensions', [
                \R64\NovaFields\Text::make(BannerModel::DIMENSION_NAME),
                Number::make(BannerModel::DIMENSION_WIDTH),
                Number::make(BannerModel::DIMENSION_HEIGHT),
                Number::make(BannerModel::DIMENSION_MINIMUM_SCREEN_WIDTH),
            ])
                ->childConfig([
                    'fieldClasses' => 'w-full px-2 py-2',
                    'wrapperClasses' => 'w-full',
                    'hideLabelInForms' => true,
                ])
                ->fieldClasses('w-full px-8 py-6'),
            HasMany::make('Items', 'items', BannerItem::class)
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
