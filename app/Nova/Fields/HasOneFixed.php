<?php

namespace App\Nova\Fields;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ResourceRelationshipGuesser;
use Laravel\Nova\Nova;

class HasOneFixed extends HasOne
{
    public function __construct($name, $attribute = null, $resource = null)
    {
        Field::__construct($name, $attribute);
        $resource = $resource ?? ResourceRelationshipGuesser::guessResource($name);

        $this->resourceClass = $resource;
        $this->resourceName = $resource::uriKey();
        $this->hasOneRelationship = $this->attribute;
        $this->singularLabel = $resource::singularLabel();

        $this->filledCallback = function ($request) {
            $resource = Nova::resourceForKey($request->viaResource);


            if ($resource && $request->viaResourceId) {
                $parent = $resource::newModel()
                    ->find($request->viaResourceId);
                $parent->load($this->attribute);
                return optional($parent->{$this->attribute})->exists === true;
            }

            return false;
        };
    }
}
