<?php

namespace App\Nova\Data\Review;

use App\Models\Data\Review\Review as ReviewModel;
use App\Nova\Data\Catalog\Product;
use App\Nova\Resource;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Nikaia\Rating\Rating;
use function __;

class Review extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = ReviewModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'author';

    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'author',
        'role'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Author')->sortable()->required(),
            Images::make('Author avatar', ReviewModel::IMAGE_AUTHOR_COLLECTION)
                ->conversionOnForm(ReviewModel::IMAGE_AUTHOR_MAIN_CONVERSION)
                ->conversionOnDetailView(ReviewModel::IMAGE_AUTHOR_MAIN_CONVERSION)
                ->conversionOnIndexView(ReviewModel::IMAGE_AUTHOR_MAIN_CONVERSION)
                ->conversionOnPreview(ReviewModel::IMAGE_AUTHOR_MAIN_CONVERSION),
            Text::make('Role')->nullable(),
            Date::make('Date')->nullable(),
            Rating::make('Rating')->min(0)->max(5)->increment(1),
            Markdown::make('Text'),
            Number::make('Priority')->sortable()->default(0)->required(),


            HasOne::make('Answer', 'answer', ReviewAnswer::class),
            BelongsToMany::make('Products', 'products', Product::class)
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
