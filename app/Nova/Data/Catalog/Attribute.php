<?php

namespace App\Nova\Data\Catalog;

use App\Enums\Website\AttributeType;
use App\Models\Data\Product\Attributes\AttributeValueCollection;
use App\Nova\Resource;
use App\View\Data\Common\MenuItemData;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use R64\NovaFields\JSON;
use R64\NovaFields\Row;
use R64\NovaFields\Select;
use SimpleSquid\Nova\Fields\Enum\Enum;
use function __;

class Attribute extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Data\Product\Attributes\Attribute::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $columns = [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->required(),
            Boolean::make('Enabled'),
            Boolean::make('Filterable'),
            Enum::make('Type')->attach(AttributeType::class),
            HasMany::make('Product Attributes', 'productAttributes', ProductAttribute::class)
        ];

        $settingRows = [];
        if ($this->type instanceof AttributeType) {
            switch ($this->type) {
                case AttributeType::TEXT:
                    break;
                case AttributeType::ENUM:
                    $settingRows[] = Select::make('Attribute value collection', 'attribute_value_collection_id')
                        ->options(AttributeValueCollection::pluck('name', 'id'));
                    break;
                case AttributeType::NUMBER:
                    break;
            }
        }
        if ($settingRows) {
            $columns[] = JSON::make('Attribute settings', $settingRows, 'attribute_settings')->flatten();
        }
        return $columns;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
