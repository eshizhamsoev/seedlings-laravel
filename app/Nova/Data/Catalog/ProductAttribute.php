<?php

namespace App\Nova\Data\Catalog;

use App\Models\Data\Product\Attributes\ProductAttribute as ProductAttributeModel;
use App\Nova\RedirectToParentResourceTrait;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class ProductAttribute extends Resource
{
    use RedirectToParentResourceTrait;
    public static $perPageViaRelationship = 20;
    public static $displayInNavigation = false;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = ProductAttributeModel::class;



    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'value';
    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'value'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id'),
            BelongsTo::make('Attribute', 'attribute', Attribute::class)->sortable(),
            BelongsTo::make('Attribute value', 'attributeValue', AttributeValue::class),
            Text::make('Value')->required()->sortable(),
            BelongsTo::make('Product', 'product', Product::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
