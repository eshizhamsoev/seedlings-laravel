<?php

namespace App\Nova\Data\Catalog;

use App\Enums\Catalog\OfferType;
use App\Enums\Catalog\ProductLabel;
use App\Models\Data\Product\Product as ProductModel;
use App\Models\Data\Product\ProductCollection as ProductCollectionModel;
use App\Nova\Data\Review\Review;
use App\Nova\Fields\HasOneFixed;
use App\Nova\Resource;
use App\Nova\Website\Url;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use OptimistDigital\MultiselectField\Multiselect;
use R64\NovaFields\Boolean;
use R64\NovaFields\JSON;
use R64\NovaFields\Row;
use R64\NovaFields\Select;
use SimpleSquid\Nova\Fields\Enum\Enum;

class ProductCollection extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = ProductCollectionModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    public static $group = 'Data';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */

    public function fields(Request $request)
    {

        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->sortable()->required(),

            JSON::make('Settings', [
                Multiselect::make('Categories', ProductCollectionModel::CATEGORIES_FIELD)
                    ->asyncResource(
                        Category::class
                    )
                    ->nullable(),

                Multiselect::make('Products', ProductCollectionModel::PRODUCTS_FIELD)
                    ->asyncResource(
                        Product::class
                    )
                    ->nullable(),

                Boolean::make('Only sale', ProductCollectionModel::ONLY_SALE_FIELD)
                    ->default(false),

                Multiselect::make('Label', ProductCollectionModel::LABELS_FIELD)
                    ->options(ProductLabel::asSelectArray())
                    ->nullable()
            ])
                ->fillUsing(function (NovaRequest $request, $model, $attribute, $requestAttribute) {
                    $value = $request[$requestAttribute];
                    $model->$attribute = $value;
                })
                ->flatten(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
