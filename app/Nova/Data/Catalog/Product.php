<?php

namespace App\Nova\Data\Catalog;

use App\Enums\Catalog\OfferType;
use App\Enums\Catalog\ProductLabel;
use App\Models\Data\Product\Product as ProductModel;
use App\Nova\Data\Review\Review;
use App\Nova\Fields\HasOneFixed;
use App\Nova\Resource;
use App\Nova\Website\Url;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = ProductModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    public static $group = 'Data';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->sortable()->required(),
            Number::make('Priority')
                ->sortable()
                ->default(0)
                ->required(),

            Text::make('Price', 'price_text')->onlyOnIndex(),

            Enum::make('Type')
                ->attach(OfferType::class),

            Enum::make('Label')
                ->nullable(true)
                ->attach(ProductLabel::class),

            HasMany::make('Offers', 'offers', NamedOffer::class)
                ->showOnDetail($this->type === OfferType::VARIANT),

            HasOneFixed::make('Offer', 'offer', SimpleOffer::class)
                ->showOnDetail($this->type === OfferType::SIMPLE),

//            NovaDependencyContainer::make([
//                Number::make('Price')
//            ])
//                ->dependsOn('type', OfferType::SIMPLE)
//                ->onlyOnForms()->hideWhenUpdating(),

            Markdown::make('Short description'),
            Markdown::make('Description'),
            Markdown::make('How to plant'),
            Markdown::make('Recommendation'),

            BelongsToMany::make('Categories', 'categories', Category::class),

            BelongsToMany::make('Reviews', 'reviews', Review::class),
            BelongsToMany::make('Related products', 'relatedProducts', Product::class),

            HasMany::make('Attributes', 'attributes', ProductAttribute::class),
            MorphOne::make('Url', 'url', Url::class),
            Images::make('Images', ProductModel::IMAGE_COLLECTION_MAIN)
                ->conversionOnIndexView(ProductModel::IMAGE_CONVERSION_DETAILS_LARGE)
                ->conversionOnDetailView(ProductModel::IMAGE_CONVERSION_DETAILS_LARGE),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

//    protected static function fillFields(NovaRequest $request, $model, $fields)
//    {
//        // Get all of our user details data
//        $priceFields = $request->only(['price']);
//        $request->request->remove('price');
//        $result = parent::fillFields($request, $model, $fields);
//        if ($model->type === OfferType::SIMPLE) {
//
//            // Insert them in the details object after model has been saved.
//
//            $result[1][] = function () use ($priceFields, $model) {
//                $offer = $model->offer;
//                if (!$offer) {
//                    $offer = new \App\Models\Data\Product\Offers\SimpleOffer();
//                    $offer->product_id = $model->id;
//                }
//                $offer->price = $priceFields['price'];
//                $offer->save();
//            };
//        }
//
//        return $result;
//    }
}
