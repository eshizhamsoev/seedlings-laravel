<?php

namespace App\Nova\Data\Catalog;

use App\Models\Data\Category\Category as CategoryModel;
use App\Models\Data\Product\Product as ProductModel;
use App\Nova\Resource;
use App\Nova\Website\Url;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Kalnoy\Nestedset\NestedSet;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\Text;
use Novius\LaravelNovaOrderNestedsetField\OrderNestedsetField;

class Category extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = CategoryModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    public static $group = 'Data';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->sortable()->required(),
            Markdown::make('Top description')->nullable(),
            Markdown::make('Description')->nullable(),
            MorphOne::make('Url', 'url', Url::class),
            BelongsTo::make('Parent', 'parent', Category::class)->nullable(),
            OrderNestedsetField::make('Priority'),

            Images::make('Images', CategoryModel::IMAGE_COLLECTION_MAIN)
                ->conversionOnIndexView(CategoryModel::IMAGE_CONVERSION_LIST)
                ->conversionOnDetailView(CategoryModel::IMAGE_CONVERSION_LIST),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    protected static function applyOrderings($query, array $orderings)
    {
        return $query->orderBy(NestedSet::LFT, 'asc');
    }
}
