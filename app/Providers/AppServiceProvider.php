<?php

namespace App\Providers;

use AntistressStore\CdekSDK2\CdekClientV2;
use App\Enums\Catalog\OfferType;
use App\Enums\EntityType;
use App\Models\Data\Article;
use App\Models\Data\Category\Category;
use App\Models\Data\Product\Offers\SimpleOffer;
use App\Models\Data\Product\Product;
use App\Models\Website\Page;
use App\Nova\Data\Catalog\NamedOffer;
use App\Services\AddressService\AddressDecoder;
use App\Services\AddressService\DadataDecoder\DadataDecoder;
use App\Services\Cart\CartService;
use App\Services\CurrentCityRepository;
use App\Services\CurrentUrlRepository;
use App\Services\Delivery\ToDoorDelivery\CdekToDoorService;
use App\Services\Delivery\ToDoorDelivery\DummyToDoorService;
use App\Services\Delivery\ToDoorDelivery\ToDoorDeliveryService;
use App\Services\Settings\SettingHelper;
use App\Services\Wishlist\WishlistService;
use Dadata\DadataClient;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CurrentCityRepository::class);
        $this->app->singleton(CurrentUrlRepository::class);
        $this->app->singleton(WishlistService::class);
        $this->app->singleton(SettingHelper::class);
        $this->app->singleton(CartService::class, fn() => new CartService(Cart::instance('cart')));

        $this->app->bind(CdekClientV2::class, fn() => new CdekClientV2(
            config('cdek.account'),
            config('cdek.secret'),

        )
        );
        $this->app->bind(ToDoorDeliveryService::class, fn() => new CdekToDoorService(
            new CdekClientV2(
                config('cdek.account'),
                config('cdek.secret'),

            )
        ));

        $this->app->bind(AddressDecoder::class, fn() => new DadataDecoder(
            new DadataClient(config('dadata.token'), config('dadata.secret'))
        ));
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            //Entities
            EntityType::PAGE => Page::class,
            EntityType::CATEGORY => Category::class,
            EntityType::PRODUCT => Product::class,
            EntityType::ARTICLE => Article::class,

            //Offers
            OfferType::SIMPLE => SimpleOffer::class,
            OfferType::VARIANT => NamedOffer::class,
        ]);
    }
}
