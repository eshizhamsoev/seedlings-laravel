<?php

namespace App\Providers;

use App\Enums\Website\Settings\CodePosition;
use App\Enums\Website\Settings\Menu;
use App\Enums\Website\Settings\SettingGroup;
use App\Enums\Website\Settings\Social;
use App\Enums\Website\Settings\SpecialPage;
use App\Nova\Website\Page;
use App\View\Data\Common\MenuItemData;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Cards\Help;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use OptimistDigital\MultiselectField\Multiselect;
use OptimistDigital\NovaSettings\NovaSettings;
use Parental\Providers\NovaResourceProvider;
use R64\NovaFields\Row;
use R64\NovaFields\Text;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        $this->app->register(NovaResourceProvider::class);

        $pageSelects = array_map(
            fn(SpecialPage $item) => Multiselect::make($item->description, $item->value)
                ->asyncResource(
                    Page::class
                )
                ->singleSelect(),
            SpecialPage::getInstances()
        );

        NovaSettings::addSettingsFields($pageSelects, [], SettingGroup::PAGES);

        $menus = Menu::getInstances();
        $menuCasts = [];
        foreach ($menus as $menu) {
            $menuCasts[$menu->value] = 'json';
        }

        NovaSettings::addSettingsFields(function () use ($menus) {
            return array_map(
                fn(Menu $item) => Row::make($item->description, [
                    Text::make('Name', MenuItemData::NAME),
                    Text::make('Link', MenuItemData::LINK),
                ], $item->value)
                    ->childConfig([
                        'fieldClasses' => 'w-full px-2 py-2',
                        'wrapperClasses' => 'w-full',
                        'hideLabelInForms' => true,
                    ])
                    ->fieldClasses('w-full px-8 py-6'),
                $menus
            );
        }, $menuCasts, SettingGroup::MENU);

        NovaSettings::addSettingsFields(function () {
            return array_map(
                fn(Social $item) => Text::make($item->description, $item->value),
                Social::getInstances()
            );
        }, [], SettingGroup::SOCIAL);

        NovaSettings::addSettingsFields(function () {
            return array_map(
                fn(CodePosition $item) => Textarea::make($item->description, $item->value),
                CodePosition::getInstances()
            );
        }, [], SettingGroup::CODE_POSITIONS);
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return $user->is_admin;
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new Help,
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            new NovaSettings
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
