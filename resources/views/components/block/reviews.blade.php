<div class='our-reviews'>
    <div class='container'>
        <div class='title title_type_primary our-reviews__title'>Наши отзывы</div>
        <div class='our-reviews__slider slider slider_type_secondary' data-swiper-type='secondary'>
            <div class='swiper'>
                <div class='swiper-wrapper'>
                    @foreach($reviews as $review)
                        <div class='swiper-slide our-reviews__slide'>
                            <x-common.review :review="$review" :small="true" />
                        </div>
                    @endforeach
                </div>
            </div>
            <x-common.btn-arrows/>
        </div>
    </div>
</div>
