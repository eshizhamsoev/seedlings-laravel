<div class="catalog">
    <div class="container">
        <h1 class="title title_type_primary">Каталог</h1>
        <div class="catalog__inner">
            @foreach($categories as $category)
                <div class="catalog-block">
                    <div class="catalog-block__title">{{ $category->name }}</div>
                    <div class="catalog-block__cards">
                        @foreach($category->children as $child)
                            <x-common.catalog-item :category="$child" />
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
