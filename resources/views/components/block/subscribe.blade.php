<div class='subscribe-section'>
    <div class='container subscribe-section__container radial-gradient'>
        <form class='subscribe-section__inner' method="POST" action="{{ route('subscribe') }}">
            <div class='subscribe-section__title'>Будьте в курсе последних новостей и выгодных акций</div>
            <div class='subscribe-section__input-wrapper'>
                <x-base.text-field
                    class="subscribe-section__label"
                    name="email"
                    :type="\App\Enums\Website\Forms\TextFieldType::EMAIL()"
                    label="Ваш e-mail"
                />
                <button class='btn btn_type_secondary subscribe-section__btn'>Подписаться</button>
            </div>
        </form>
    </div>
</div>
