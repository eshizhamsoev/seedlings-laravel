<div class='main-slider'>
    <div class='container'>
        <div class='main-slider__container'>
            <div class='main-slider__swiper-wrapper slider slider_type_main' data-swiper-type='main'>
                <div class='swiper main-slider__swiper'>
                    <div class='swiper-wrapper'>
                        @foreach($banner->items as $item)
                            <div class='swiper-slide'>
                                <div class='main-slider__item main-slider__item_color_primary'>
                                    <div class='main-slider__image-wrapper'>
                                        <x-common.banner-item :banner-item="$item" :dimensions="$dimensions"/>
                                    </div>
                                    <div class='main-slider__content'>
                                        <div class='main-slider__title'>
                                            {{ $item->name }}
                                        </div>
                                        <div class='main-slider__subtitle'>
                                            @markdown($item->description)
                                        </div>
                                        @if($catalogPage)
                                            <x-base.link :page="$catalogPage"
                                                         class='btn btn_type_secondary main-slider__btn'>
                                                <span>Перейти в каталог</span>
                                                <svg width="11" height="18">
                                                    <use xlink:href='sprite.svg#arrow-down'></use>
                                                </svg>
                                            </x-base.link>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class='btn-arrows'>
                    <button class='btn btn-arrows__item btn-arrows__item_prev'>
                        <svg width="10" height="16">
                            <use xlink:href='sprite.svg#arrow'></use>
                        </svg>
                    </button>
                    <button class='btn btn-arrows__item btn-arrows__item_next'>
                        <svg width="10" height="16">
                            <use xlink:href='sprite.svg#arrow'></use>
                        </svg>
                    </button>
                </div>

                <a href='#block_group_1' class='a-clear main-slider__btn-down'>
                    <svg width="22" height="34">
                        <use xlink:href='sprite.svg#arrow-down'></use>
                    </svg>
                </a>
            </div>
            <div class='main-slider__benefits'>
                <div class='main-slider__benefit'>
                    <div class='main-slider__benefit-img-wrapper'><img loading="lazy" width="34" height="30"
                                                                       src='{{ asset('/static/images/benefits/car.svg') }}'
                                                                       alt=''></div>
                    <div class='main-slider__benefit-title'>Доставляем по всей РФ</div>
                </div>
                <div class='main-slider__benefit'>
                    <div class='main-slider__benefit-img-wrapper'><img loading="lazy" width="32" height="32"
                                                                       src='{{ asset('/static/images/benefits/hands.svg') }}'
                                                                       alt=''></div>
                    <div class='main-slider__benefit-title'>Напрямую из питомника</div>
                </div>
                <div class='main-slider__benefit'>
                    <div class='main-slider__benefit-img-wrapper'><img loading="lazy" width="34" height="32"
                                                                       src='{{ asset('/static/images/benefits/ecology.svg') }}'
                                                                       alt=''></div>
                    <div class='main-slider__benefit-title'>Растения без посредников</div>
                </div>
                <div class='main-slider__benefit'>
                    <div class='main-slider__benefit-img-wrapper'><img loading="lazy" width="30" height="30"
                                                                       src='{{ asset('/static/images/benefits/global-warming.svg') }}'
                                                                       alt=''>
                    </div>
                    <div class='main-slider__benefit-title'>Подходят под рос. климат</div>
                </div>
            </div>
        </div>
    </div>
</div>
