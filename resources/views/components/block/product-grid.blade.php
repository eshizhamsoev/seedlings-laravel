<div class='product-grid'>
    <div class='container'>
        @if($title)
            <div class='product-grid__title title title_type_primary'>{{ $title }}</div>
        @endif
        <div class='product-grid__inner'>
            @foreach($products as $product)
                <div class='product-grid__item'>
                    <x-common.product-card.product-card :product="$product" :products-view="\App\Enums\Catalog\ProductsView::GRID()"/>
                </div>
            @endforeach
        </div>
        @if($linkData)
            <div class='product-grid__btn-wrapper'>
                <x-base.link :href="$linkData->getUrl()"
                             class='btn btn_type_primary product-grid__btn'>
                    {{$linkData->getText()}}
                </x-base.link>
            </div>
        @endif
    </div>
</div>
