<div class='news'>
    <div class='container'>
        <div class='title title_type_primary news__title'>Новости</div>
        <div class='news__slider-wrapper slider slider_type_primary' data-swiper-type='freeMode'>
            <div class='swiper news__slider'>
                <div class='swiper-wrapper'>
                    @foreach($news as $slide)
                        <div class='swiper-slide'>
                            <x-common.article-card :article='$slide'></x-common.article-card>
                        </div>
                    @endforeach
                </div>
            </div>
            <x-common.btn-arrows />
        </div>
    </div>
</div>
