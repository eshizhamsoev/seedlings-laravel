@props([
    'top_description'
])
@if($products->isNotEmpty())
    <div class='catalog-filter'>
        <div class='container'>
            <div class='catalog-filter__inner'>
                <div class='catalog-filter__filters'>
                    <x-block.catalog-filter.filters :min-max="$minMax"/>
                </div>
                <div class='catalog-filter__contents'>
                    <div class='catalog-filter__contents-header'>
                        @if(!empty($beforeFilterBar))
                            {{ $beforeFilterBar }}
                        @endif
                        <div class='catalog-filter__filter-bar'>
                            <x-block.catalog-filter.filter-bar
                                :view="$view"
                                :order-field="$orderField"
                                :order-ascending="$orderAscending"
                            />
                        </div>
                        @if(!empty($afterFilterBar))
                            {{ $afterFilterBar }}
                        @endif
                    </div>
                    @if($view->is(\App\Enums\Catalog\ProductsView::GRID))
                        <div class='catalog-filter__cards'>
                            @foreach($products as $product)
                                <x-common.product-card.product-card :product="$product" :products-view="$view"/>
                            @endforeach
                        </div>
                    @elseif($view->is(\App\Enums\Catalog\ProductsView::LIST))
                        <div class='catalog-filter__cards_vertical'>
                            @foreach($products as $product)
                                <x-common.product-card.product-card :product="$product" :products-view="$view"/>
                            @endforeach
                        </div>
                    @endif
                    <div class='catalog-filter__contents-footer'>
                        {{--                    <div class='catalog-filter__more'>--}}
                        {{--                        <button class='btn btn_type_primary catalog-filter__more-btn'>Показать еще (20)</button>--}}
                        {{--                    </div>--}}
                        {!! $links !!}
                        {{--                    {% if params.pagination %}--}}
                        {{--                    <div class='catalog-filter__pagination'>--}}
                        {{--                        {{ component('pagination') }}--}}
                        {{--                    </div>--}}
                        {{--                    {% endif %}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    Парам пам пам
@endif
