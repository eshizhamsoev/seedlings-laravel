<div class='filters'>
    {{--    {% if not params.mobile %}--}}
    <div class='filters__header'>
        <span>Фильтры</span>
        <x-base.svg-icon width="24" height="24" :icon="\App\Enums\Website\SvgIconName::FILTER_FILLED()"/>
    </div>
    {{--    {% endif %}--}}
    <div class='filters__inner'>
        @if($minMax)
            <div class='filters__main js-filter-range'>
                <div class='filters__title'>Цена</div>
                <div class='filters__range-inputs'>
                    <input type='number'
                           min='{{ $minMax->getMin() }}'
                           max='{{ $minMax->getMax() }}'
                           value='{{ $minMax->getMin() }}'
                           data-input='min'
                           aria-label="Цена от"
                           class='base-input filters__input'
                    />
                    <input type='number'
                           min='{{ $minMax->getMin() }}'
                           max='{{ $minMax->getMax() }}'
                           value='{{ $minMax->getMax() }}'
                           data-input='max'
                           aria-label="Цена до"
                           class='base-input filters__input'
                    />
                </div>
                <div class='filters__range'>
                    <div class='range js-range'></div>
                </div>
                <div class='filters__field'>
                    <input type='checkbox' id='0' class='base-input filters__input'>
                    <label class='filters__label' for='0'>Только товары со скидкой</label>
                </div>
            </div>
        @endif
{{--        <div class='filters__item'>--}}
{{--            <details class='filters__details'>--}}
{{--                <summary class='filters__summary'>--}}
{{--                    <span>Цвет кроны</span>--}}
{{--                    <svg class='filters__icon_minus' width="12" height="12">--}}
{{--                        <use xlink:href='sprite.svg#minus'></use>--}}
{{--                    </svg>--}}
{{--                    <svg class='filters__icon_plus' width="12" height="12">--}}
{{--                        <use xlink:href='sprite.svg#plus'></use>--}}
{{--                    </svg>--}}
{{--                </summary>--}}
{{--                <div class='filters__fields'>--}}
{{--                    <div class='filters__field'>--}}
{{--                        <input type='checkbox' id='1' class='base-input filters__input'>--}}
{{--                        <label class='filters__label' for='1'>Зелный (36) </label>--}}
{{--                    </div>--}}
{{--                    <div class='filters__field'>--}}
{{--                        <input type='checkbox' id='2' class='base-input filters__input' checked>--}}
{{--                        <label class='filters__label' for='2'>Красный (15)</label>--}}
{{--                    </div>--}}
{{--                    <div class='filters__field'>--}}
{{--                        <input type='checkbox' id='3' class='base-input filters__input' disabled>--}}
{{--                        <label class='filters__label' for='3'>Желтый (0)</label>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </details>--}}
{{--        </div>--}}
{{--        <div class='filters__item'>--}}
{{--            <details class='filters__details'>--}}
{{--                <summary class='filters__summary'>--}}
{{--                    <span>Цвет кроны</span>--}}
{{--                    <svg class='filters__icon_minus' width="12" height="12">--}}
{{--                        <use xlink:href='sprite.svg#minus'></use>--}}
{{--                    </svg>--}}
{{--                    <svg class='filters__icon_plus' width="12" height="12">--}}
{{--                        <use xlink:href='sprite.svg#plus'></use>--}}
{{--                    </svg>--}}
{{--                </summary>--}}
{{--                <div class='filters__fields'>--}}
{{--                    <div class='filters__field'>--}}
{{--                        <input type='checkbox' id='12' class='base-input filters__input'>--}}
{{--                        <label class='filters__label' for='12'>Зелный (36) </label>--}}
{{--                    </div>--}}
{{--                    <div class='filters__field'>--}}
{{--                        <input type='checkbox' id='22' class='base-input filters__input' checked>--}}
{{--                        <label class='filters__label' for='22'>Красный (15)</label>--}}
{{--                    </div>--}}
{{--                    <div class='filters__field'>--}}
{{--                        <input type='checkbox' id='32' class='base-input filters__input' disabled>--}}
{{--                        <label class='filters__label' for='32'>Желтый (0)</label>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </details>--}}
{{--        </div>--}}
    </div>
</div>
