<div class='filter-bar {{ 'filter-bar_show_filter' /*if params.show*/ }}'>
    <div class='filter-bar__btn-group filter-bar__btn-group_first'>
        @foreach($orders as $order)
            <x-base.link
                :href="$order->getLink()"
                :class="'btn filter-bar__order-btn mobile-hide'
                . ' ' . ($order->isDescending() ? 'filter-bar__order-btn_rotate' : '')
                . ' ' . ($order->isActive() ? 'active' : '')">
                <x-base.svg-icon :width="18" :height="18" :icon="\App\Enums\Website\SvgIconName::GRAPH()"/>
                <span>{{ $order->getName() }}</span>
            </x-base.link>
        @endforeach
        <button class='btn filter-bar__order-btn filter-bar__order-btn_rotate  mobile-show'>
            <x-base.svg-icon :width="18" :height="18" :icon="\App\Enums\Website\SvgIconName::GRAPH()"/>
            <span>Цена</span>
        </button>
    </div>
    <button data-fancybox data-src='#filter-card' class='btn btn_type_primary filter-bar__filter-btn'>Фильтр</button>
    <div class='filter-bar__btn-group filter-bar__btn-group_second'>
        <button class='btn filter-bar__btn {{ $view->is(\App\Enums\Catalog\ProductsView::LIST) ? 'active' : '' }}'
                aria-label="Отображение товаров списком"
                type="button"
                name="view"
                value="{{ \App\Enums\Catalog\ProductsView::LIST }}"
        >
            <x-base.svg-icon :width="24" :height="24" :icon="\App\Enums\Website\SvgIconName::BTN_TWO()"/>
        </button>
        <button class='btn filter-bar__btn {{ $view->is(\App\Enums\Catalog\ProductsView::GRID) ? 'active' : '' }}'
                aria-label="Отображение товаров сеткой"
                type="button"
                name="view"
                value="{{ \App\Enums\Catalog\ProductsView::GRID }}"
        >
            <x-base.svg-icon :width="24" :height="24" :icon="\App\Enums\Website\SvgIconName::BTN_FOUR()"/>
        </button>
    </div>
</div>

{{--{{ component('filter-card') }}--}}
