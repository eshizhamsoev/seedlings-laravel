<div class='popular'>
    <div class='container'>
        <div class='popular__container'>
            <div class='title title_type_primary popular__title'>{{ $heading }}</div>
            <div class='popular__sections'>
                @foreach($categories as $category)
                    <x-common.catalog-item :category="$category"/>
                @endforeach
            </div>
        </div>
    </div>
</div>
