<div class='special-offer'>
    <div class='container'>
        <div class='title title_type_primary special-offer__title'>{{ $heading }}</div>
        <div class='special-offer__cards slider slider_type_primary' data-swiper-type='primary'>
            <div class='swiper'>
                <div class='swiper-wrapper'>
                    @foreach($products as $product)
                        <x-common.product-card.product-card :product="$product" :products-view="\App\Enums\Catalog\ProductsView::GRID()" class='swiper-slide special-offer__slide' />
                    @endforeach
                </div>
            </div>
            <x-common.btn-arrows />
        </div>
    </div>
</div>
