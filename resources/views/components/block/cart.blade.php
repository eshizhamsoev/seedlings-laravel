<div>
    @if($isEmpty)
        <x-common.informer :is-grey="true" :image="\App\Enums\Website\InformerImage::EMPTY_CART()"
                           title="В вашей корзине пусто"
                           sub-title="Чтобы добавить товары, перейдите каталог и выполните их покупку">
            <x-slot name="actions">
                <x-base.link class='btn btn_type_secondary order__pay-btn last-block__btn js-change-image__btn'
                             :page="$catalogPage">
                    Перейти в каталог
                </x-base.link>
            </x-slot>
        </x-common.informer>
    @else

        <div class='cart'>
            <div class='container'>
                <div class='cart__title-wrapper'>
                    <div class='title title_type_primary'>Корзина</div>
                    <x-base.link :page="$checkoutPage" class='btn btn_type_secondary'>
                        Перейти к оплате
                    </x-base.link>
                </div>
                <div id='cart-page'></div>
                <div class='cart__pay'>
                    <x-common.back-button class="cart__pay-comeback"/>
                    <x-base.link :page="$checkoutPage" class='btn btn_type_secondary cart__pay-btn'>
                        Перейти к оплате
                    </x-base.link>
                </div>
            </div>
        </div>
    @endif
</div>
