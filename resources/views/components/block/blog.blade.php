<div class='blog'>
    <x-layouts.breadcrumbs :url="$url"/>
    <div class='container'>
        <div class='title title_type_primary'>Блог</div>
        <div class='blog__inner'>
            @foreach($articles as $article)
                    <x-common.article-card :article='$article'></x-common.article-card>
            @endforeach
        </div>
        <div class='sale__btn-wrapper'>
            {{ $articles->links('components.base.pagination') }}
        </div>
    </div>
</div>
