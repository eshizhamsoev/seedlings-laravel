<div class="favorite">

    <div class="container">
        <div class="favorite__banner">
            <div class="favorite__banner-title">Выберете товар</div>
            <div class="favorite__banner-img-wrapper">
                <img loading="lazy" src="{{ asset('/static/images/components/block/wishlist/favorite-stripes.svg') }}"
                     alt="">
            </div>
        </div>
            <div class="favorite__inner">
                @if($products->isNotEmpty())
                @foreach($products as $product)
                    <x-common.product-card.product-card
                        class="js-wishlist-row"
                        :product="$product"
                        :products-view="\App\Enums\Catalog\ProductsView::LIST()"
                    >
                        <x-slot name="actions">
                            <button
                                class="btn product-card-row__cart-remove js-wishlist-row-remove"
                                data-product-id="{{ $product->id }}"
                                aria-label="Удалить из {{ $product->name  }} избранного"
                            >
                                <x-base.svg-icon :width="16" :height="16"
                                                 :icon="\App\Enums\Website\SvgIconName::DELETE()"/>
                                <span>Удалить</span>
                            </button>
                        </x-slot>
                    </x-common.product-card.product-card>
                @endforeach
                @else
                    <p>У вас нет товаров в избранном</p>
                @endif
            </div>
    </div>
</div>
