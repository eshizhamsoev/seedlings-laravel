<header class="header">
    <div class="header-top wrapper-grey">
        <div class="container">
            <div class="header-top__container">
                <x-common.menu :menu="\App\Enums\Website\Settings\Menu::TOP()" block-name="header-top-menu"
                               class="header-top__menu">
                    <x-layouts.header.more-menu/>
                </x-common.menu>
                <div class="header-top__town">
                    <x-common.your-town/>
                </div>
                <div class="header-top__works">Работаем 24/7</div>
                <div class="header-top__user">
                    {{--                    {% if true %}--}}
                    <div class="header-top__login-wrapper">
                        <x-base.modal-opener class="btn header-top__log header-top__log_type_in"
                                             :modal-type="\App\Enums\Website\ModalType::FANCY_LOGIN()">Вход
                        </x-base.modal-opener>
                        <div class="header-top__user-divider"></div>
                        <x-base.modal-opener class="btn header-top__log header-top__log_type_registration"
                                             :modal-type="\App\Enums\Website\ModalType::FANCY_REGISTRATION()">
                            Регистрация
                        </x-base.modal-opener>
                    </div>
                    {{--                    {% else %}--}}
                    {{--                    {{ component("account") }}--}}
                    {{--                    {% endif %}--}}
                </div>
            </div>
        </div>
    </div>
</header>


<div class='header-bottom sticky'>
    <div class='container'>
        <div class='header-bottom__container'>
            <div class='header-bottom__catalog-btn'>
                <x-layouts.header.catalog-menu/>
            </div>
            <div class='header-bottom__logo-wrapper'>
                <x-base.link href='/'>
                    <img loading="lazy" width="156" height="11" src='{{$logo}}' alt='Логотип'>
                </x-base.link>
                <x-base.phone-link class='a-clear header-bottom__tel' :phone="$currentCity->phone"/>
            </div>
            <x-layouts.header.search/>
            <div class='header-bottom__favorite'>
                @if($wishlistPage)
                    <x-base.link :page="$wishlistPage" class='btn favorite-button'>
                        <x-base.svg-icon width="18" height="16" :icon="\App\Enums\Website\SvgIconName::HEART()"/>
                    </x-base.link>
                @endif
            </div>
            <x-base.link :page="$cartPage" class='header-basket'>
                Корзина
                <span class='header-basket__count js-cart-quantity'>{{ $cartQuantity }}</span>
            </x-base.link>
        </div>
    </div>
</div>
<div class='header-mobile sticky'>
    <div class='container'>
        <div class='header-mobile__container'>
            <a href='#my-mmenu-menu' class='header-mobile__menu-btn a-clear'>
                <x-base.svg-icon width="18" height="12" :icon="\App\Enums\Website\SvgIconName::BTN_MENU()"/>
            </a>
            <div class='header-mobile__logo-wrapper'>
                <x-base.link href="/">
                    <img loading="lazy" width="156" height="11" src='{{$logo}}' alt='Логотип'>
                </x-base.link>
                <x-base.phone-link :phone="$currentCity->phone" class='header-mobile__tel'/>
            </div>
            @if($catalogPage)
                <x-base.link :page='$catalogPage' class='header-mobile__catalog-btn'>Каталог</x-base.link>
            @endif
            <a href='#my-mmenu-search' class='a-clear btn header-mobile__btn-icon'>
                <x-base.svg-icon width="15" height="15" :icon="\App\Enums\Website\SvgIconName::SEARCH()"/>
            </a>
            <button class='btn header-mobile__btn-icon header-mobile__btn-icon_user js-button-click '
                    data-mobile-hide-button="menu">
                <x-base.svg-icon width="16" height="17" :icon="\App\Enums\Website\SvgIconName::USER()"/>
            </button>
            <button
                class='header-basket header-mobile__btn-icon header-mobile__btn-icon_basket js-button-click '
                data-mobile-hide-button="basket">
                <x-base.svg-icon width="18" height="18" :icon="\App\Enums\Website\SvgIconName::BASKET()"/>
                <span class='header-basket__count js-cart-quantity'>{{ $cartQuantity }}</span>
            </button>
        </div>
    </div>
</div>
