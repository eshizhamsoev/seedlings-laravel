<footer class='footer'>
    <div class='container'>
        <div class='footer__container'>
            <div class='footer__logo-wrapper'>
                <x-base.link href="/" class='footer__logo a-clear'>
                    <img loading="lazy" width='156' height='11' src='{{$logo}}' alt='Логотип компании'>
                </x-base.link>
                <div class='footer__your-town'>
                    <x-common.your-town/>
                </div>
            </div>

            <x-common.menu
                :menu="\App\Enums\Website\Settings\Menu::FOOTER()"
                block-name="footer-menu"
                class="footer__menu"
            />

            <div class='footer__contact'>
                @if($currentCity->address)
                    <x-base.link :page="$contacts" class='footer__contact-item'>
                        <x-base.svg-icon class='footer__contact-icon' width="16" height="17"
                                         :icon="\App\Enums\Website\SvgIconName::CARBON()"/>
                        <div class='footer__contact-text'>{{ $currentCity->address }}</div>
                    </x-base.link>
                @endif
                @if($currentCity->phone)
                    <div class='footer__contact-item footer__contact-phone'>
                        <x-base.phone-link :phone="$currentCity->phone">
                            <x-base.svg-icon class='footer__contact-icon' width="16" height="17"
                                             :icon="\App\Enums\Website\SvgIconName::PHONE()"/>
                        </x-base.phone-link>
                        <x-base.phone-link :phone="$currentCity->phone" class="footer__contact-text"/>
                    </div>
                @endif
                <div class='footer__contact-item'>
                    <x-base.svg-icon class='footer__contact-icon' width="16" height="17"
                                     :icon="\App\Enums\Website\SvgIconName::CLOCK()"/>
                    <div class='footer__contact-text footer__contact-text_hours'>{{ $currentCity->work_hours }}</div>
                </div>
                <div class='footer__contact-item'>
                    <a href='mailto:info@eco-plant.ru' class='a-clear'>
                        <x-base.svg-icon class='footer__contact-icon' width="16" height="17"
                                         :icon="\App\Enums\Website\SvgIconName::MAIL()"/>
                    </a>
                    <a href='mailto:info@eco-plant.ru' class='a-clear footer__contact-text'>{{ $currentCity->email }}</a>
                </div>
                <div class='footer__socials'>
                    <x-common.our-socials/>
                </div>
            </div>
        </div>
    </div>
    <div class='footer__bottom'>
        <div class='container'>
            <div class='footer__bottom-container'>
                <div class='footer__bottom-company'>ООО Экоплант, 1999 - {{ date('Y') }} ©</div>

                <x-common.menu
                    :menu="\App\Enums\Website\Settings\Menu::PAGE_BOTTOM()"
                    block-name="footer-bottom-links"
                    class="footer__bottom-links"
                />
            </div>
        </div>
    </div>
</footer>
