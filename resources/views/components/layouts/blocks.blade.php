@foreach($groups as $i => $group)
    <div class="wrapper wrapper_{{ $group->getColor()->value }}" id="block_group_{{ $i }}">
        @foreach($group->getBlocks() as $block)
            <x-dynamic-component :component="$block->getComponentName()" :data="$block->getData()"/>
        @endforeach
    </div>
@endforeach
