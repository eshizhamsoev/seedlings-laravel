<div class='cart-header'>
    <div class='container'>
        <div class='cart-header__inner'>
            <x-base.link href='/' class='cart-header__logo a-clear'>
                <img loading="lazy" width='184' height='13' src='{{ asset('static/images/common/logo.svg') }}' alt=''>
            </x-base.link>
            <div class='cart-header__come-back'>
                <x-common.back-button />
            </div>
        </div>
    </div>
</div>
