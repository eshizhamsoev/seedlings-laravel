<div class='product-catalog-list js-catalog-list'>
    <button class='btn product-catalog-list__btn js-catalog-btn'>
        <span>Каталог</span>
        <x-base.svg-icon width="20" height="14" :icon="\App\Enums\Website\SvgIconName::BTN_MENU()"/>
    </button>
    <ul class='product-catalog-list__catalog ul-clear'>
        @foreach($categories as $category)
            <li class='product-catalog-list__catalog-item js-sub-catalog'>
                <x-base.link :url="$category->url" class='product-catalog-list__catalog-link js-catalog-link'>
                    {{ $category->name }}
                </x-base.link>
                @if($category->children && $category->children->isNotEmpty())
                    <ul class='product-catalog-list__sub-catalog ul-clear'>
                        @foreach($category->children as $child)
                            <li class='product-catalog-list__catalog-item'>
                                <x-base.link :url="$child->url" class='product-catalog-list__catalog-link'>
                                    {{ $child->name }}
                                </x-base.link>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
</div>
