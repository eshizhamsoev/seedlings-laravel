<div class='more-menu js-additional-content'>
    <button class='btn more-menu__btn' data-additional="btn">
        <span>Еще</span>
        <x-base.svg-icon width="8" height="3" :icon="\App\Enums\Website\SvgIconName::TRIANGLE()"/>
    </button>
    <x-common.menu
        :menu="\App\Enums\Website\Settings\Menu::TOP_MORE()"
        block-name="header-top-more-menu"
        class="more-menu__inner" data-additional="content"
    />
</div>
