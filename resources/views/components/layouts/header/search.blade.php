<div class='search-block js-search-block {{--{{ 'search-block_is-search' }}--}}'>
    <label class='search-block__inner'>
        <input type='text' name="searchQuery" placeholder='Поиск' class='search-block__input js-search-input' aria-label="Поиск" autocomplete="off">
        <button class='btn search-block__btn'>
            <x-base.svg-icon width="18" height="18" :icon="\App\Enums\Website\SvgIconName::SEARCH()"/>
        </button>
    </label>
    <div class='search-block__results js-search-results'></div>
</div>
