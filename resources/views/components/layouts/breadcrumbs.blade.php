<div class='bread-crumbs'>
    <div class='container'>
        <nav>
            <ul class='bread-crumbs__container ul-clear'>
                @foreach($links as $link)
                    <li class='bread-crumbs__item'>
                        <x-base.link :href="$link->getLink()">{{ $link->getName() }}</x-base.link>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
</div>
