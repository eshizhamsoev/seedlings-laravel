<div class='fancy-login'>
    <form class='fancy-login__inner'>
        <div class='fancy-login__title'>Регистрация</div>
        <div class='fancy-login__subtitle'>
            <span>Есть аккаунт?</span>
            <x-base.modal-opener class="btn fancy-login__link"
                                 :modal-type="\App\Enums\Website\ModalType::FANCY_LOGIN()">Войти
            </x-base.modal-opener>
        </div>
        <div class='fancy-login__input-wrapper'>
            <x-base.text-field
                name="text"
                :type="\App\Enums\Website\Forms\TextFieldType::TEXT()"
                label="Ваше имя"
                placeholder="Ваше имя"
            />
            <x-base.text-field
                name="email"
                :type="\App\Enums\Website\Forms\TextFieldType::EMAIL()"
                label="Электронная почта"
                placeholder="Электронная почта"
            />
            <x-base.text-field
                name="tel"
                :type="\App\Enums\Website\Forms\TextFieldType::TEL()"
                label="Номер телефона"
                placeholder="Номер телефона"
            />
            <x-base.text-field
                name="password"
                :type="\App\Enums\Website\Forms\TextFieldType::PASSWORD()"
                label="Пароль"
                placeholder="Пароль"
            />
            <x-base.text-field
                name="password"
                :type="\App\Enums\Website\Forms\TextFieldType::PASSWORD()"
                label="Повторите пароль"
                placeholder="Повторите пароль"
            />
        </div>
        <div class='fancy-login__btn-wrapper'>
            <x-base.checking-input-field :is-checked="true" class="fancy-login__checkbox"
                                         name="test"
                                         :type="\App\Enums\Website\Forms\CheckingFieldType::CHECKBOX()">
                <x-slot name="label">Я согласен на <a href='#!' class='fancy-login__link'>обработку данных</a></x-slot>
            </x-base.checking-input-field>
            <button type="submit" class='btn btn_type_secondary fancy-login__btn fancy-login__btn_come-in'>Войти
            </button>
        </div>
    </form>
    <div class='fancy-login__bottom'>
        <div class='fancy-login__come-in'>Войти через:</div>
        <div class='fancy-login__socials'>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::GOOGLE()"/>
            </a>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::FACEBOOK()"/>
            </a>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::VK()"/>
            </a>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::YANDEX()"/>
            </a>
        </div>
    </div>
</div>
