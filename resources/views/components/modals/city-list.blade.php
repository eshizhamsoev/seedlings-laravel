<div class='fancy-towns js-search-container'>
    <div class='fancy-towns__title'>Ваш город</div>
    <x-base.text-field
        class="fancy-towns__input"
        name="city"
        :type="\App\Enums\Website\Forms\TextFieldType::TEXT()"
        label="Введите название"
        placeholder="Введите название"
    />
    <ul class='fancy-towns__list ul-clear'>
        @foreach($cities as $city)
            <li class='fancy-towns__item' data-search-value="{{ $city->name }}">
                <x-base.link :href="$city->url"
                             :target="\App\Enums\Website\LinkTarget::SELF()"
                             :class="'fancy-towns__link ' . ($city->id === $currentCity->id ? 'fancy-towns__link_active' : '')">{{ $city->name }}</x-base.link>
            </li>
        @endforeach
    </ul>
    <div hidden class="fancy-towns__no-results js-search-no-results">
        Нет городов по вашим критериям
    </div>
</div>
