<div class='fancy-login'>
    <div class='fancy-login__inner'>
        <form class='fancy-login__input-wrapper'>
            <div class='fancy-login__title'>Восстановление пароля</div>
            <x-base.text-field
                class="subscribe-section__label"
                name="email"
                :type="\App\Enums\Website\Forms\TextFieldType::EMAIL()"
                label="Ваш e-mail"
                placeholder="Ваша электронная почта"
            />
            <button type="submit" class='btn btn_type_secondary fancy-login__btn'>Отправить письмо</button>
        </form>
        <a href='#!' class='a-clear fancy-login__back'>Вернутся назад</a>
    </div>
</div>
