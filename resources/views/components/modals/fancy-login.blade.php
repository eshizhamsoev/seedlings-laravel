<div class='fancy-login'>
    <div class='fancy-login__inner'>
        <div class='fancy-login__title'>Вход</div>
        <div class='fancy-login__subtitle'>
            <span>Нет аккаунта?</span>
            <x-base.modal-opener class="btn fancy-login__link"
                                 :modal-type="\App\Enums\Website\ModalType::FANCY_REGISTRATION()">
                Зарегистрироваться
            </x-base.modal-opener>
        </div>
        <form class='fancy-login__input-wrapper'>
            <x-base.text-field
                name="text"
                :type="\App\Enums\Website\Forms\TextFieldType::TEXT()"
                label="text"
                placeholder="Ваше имя или электронная почта"
            />
            <div>
                <x-base.text-field
                    name="text"
                    class="fancy-login__checkbox"
                    :type="\App\Enums\Website\Forms\TextFieldType::PASSWORD()"
                    label="Пароль"
                    placeholder="Пароль"
                />
                <x-base.checking-input-field class="fancy-login__checkbox" label="Запомнить меня" name="test"
                                             :type="\App\Enums\Website\Forms\CheckingFieldType::CHECKBOX()"/>
            </div>
            <button type="submit" class='btn btn_type_secondary fancy-login__btn'>Войти</button>
        </form>
        <div class='fancy-login__forgot'>
            <span>Забыли пароль?</span>
            <x-base.modal-opener class="btn fancy-login__link"
                                 :modal-type="\App\Enums\Website\ModalType::FANCY_RECOVERY()">Восстановить
            </x-base.modal-opener>
        </div>
    </div>
    <div class='fancy-login__bottom'>
        <div class='fancy-login__come-in'>Войти через:</div>
        <div class='fancy-login__socials'>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::GOOGLE()"/>
            </a>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::FACEBOOK()"/>
            </a>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::VK()"/>
            </a>
            <a href='#!' class='a-clear fancy-login__social'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::YANDEX()"/>
            </a>
        </div>
    </div>
</div>
