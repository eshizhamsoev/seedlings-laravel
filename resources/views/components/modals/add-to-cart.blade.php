<?php
/**
 * @var \App\View\Data\Modals\AddToCart\CartItemInfo[] $items
 */
?>
<div class="add-to-cart">
    <div class="add-to-cart__inner">
        <div class="add-to-cart__title">Товар добавлен в корзину</div>
        <div class="add-to-cart__items">
            @foreach($items as $item)
                <div class="add-to-cart__item">
                    <x-base.link :url="$item->getLink()" class="add-to-cart__image-wrapper">
                        @if($item->getImage())
                            {{ $item->getImage()(\App\Models\Data\Product\Product::IMAGE_CONVERSION_DETAILS_LARGE) }}
                        @else
                            <img
                                src="{{ asset('/static/images/common/product.svg') }}"
                                alt=""
                                width="300"
                                height="400"
                            />
                        @endif
                    </x-base.link>
                    <x-base.link :url="$item->getLink()" class="add-to-cart__name">{{ $item->getName() }}</x-base.link>
                    <div class="add-to-cart__count">{{ $item->getCount() }} шт.</div>
                    <div class="add-to-cart__price">
                        <x-base.rubles :number="$item->getPrice()"/>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="add-to-cart__buttons">
            <button class="btn btn_type_additional add-to-cart__button" data-action="close">Продолжить покупки</button>
            <x-base.link :url="$cartUrl" class="btn btn_type_primary add-to-cart__button">В корзину</x-base.link>
        </div>
    </div>
</div>

