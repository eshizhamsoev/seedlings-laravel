<div class='product-block'>
    <div class='container'>
        <div class='product-block__back'>
            @if($previous)
                <x-base.link :href="$previous" class='come-back'>
                    <x-base.svg-icon width="16" height="16" :icon="\App\Enums\Website\SvgIconName::ARROW_BACK()"/>
                    <span>Вернуться назад</span>
                </x-base.link>

            @endif
        </div>
        <div class='product-block__top'>
            <x-entities.product.product-images :product="$product"/>
            <div class='product-block__info'>
                <x-entities.product.product-info :product="$product"/>
            </div>
        </div>
        <div class='product-block__tabs base-paragraph'>
            <x-base.tabs>
                @if($product->description)
                    <x-base.tabs.item heading="О товаре" :is-active="true">
                        <div class="product-block__content product-block__content_type_about">
                            <div class="product-block__title">Описание</div>
                            <div class="product-block__desc">
                                @markdown($product->description)
                            </div>
                        </div>
                    </x-base.tabs.item>
                @endif
                @if($product->short_description && $product->attributes)
                    <x-base.tabs.item heading="Характеристики">
                        <div class="product-block__content product-block__content_type_specification">
                            <div class="product-block__title-wrapper">
                                <div class="product-block__title">Характеристики</div>
                                <div class="product-block__subtitle">
                                    @markdown($product->short_description)
                                </div>
                            </div>
                            <x-entities.product.product-specification :product="$product"/>
                        </div>
                    </x-base.tabs.item>
                @endif
                @if($product->how_to_plant)
                    <x-base.tabs.item heading="Как сажать">
                        <div class="product-block__content product-block__content_type_how-to-plant">
                            <div class="product-block__title">Как сажать</div>
                            <div class="product-block__desc">
                                @markdown($product->how_to_plant)
                            </div>
                        </div>
                    </x-base.tabs.item>
                @endif
                @if($product->reviews->isNotEmpty())
                    <x-base.tabs.item heading="Отзывы">
                        <div class="product-block__content product-block__content_type_reviews">
                            @foreach($product->reviews as $review)
                                <div class="product-block__review">
                                    <x-common.review :review="$review"/>
                                </div>
                            @endforeach
                        </div>
                        {{--                        <div class="product-block__reviews-btn-wrapper"><button>btn</button></div>--}}
                    </x-base.tabs.item>
                @endif
                @if($product->recommendation)
                    <x-base.tabs.item heading="Рекомендации по уходу">
                        <div class="product-block__content product-block__content_type_recommendations">
                            <div class="product-block__title">Рекомендации</div>
                            @markdown($product->recommendation)
                        </div>
                    </x-base.tabs.item>
                @endif
            </x-base.tabs>
        </div>
    </div>
</div>

<div class="wrapper wrapper_grey">
    @if($relatedProductData)
        <x-block.product-grid :data="$relatedProductData"/>
    @endif
    <x-block.recently-watched :data="$recently_watched_settings"/>
</div>
