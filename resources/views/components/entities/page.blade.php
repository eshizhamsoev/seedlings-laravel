@if($page->blocks)
    <x-layouts.blocks :blocks="$page->blocks->all()"/>
@endif
