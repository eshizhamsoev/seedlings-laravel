<div class='product-info'>
    <div class='product-info__top'>
        <div class='product-info__title-wrapper'>
            <h1 class='product-info__title'>{{ $heading }}</h1>
            <div class='product-info__subtitle-wrapper'>
                <div class='product-info__availability'>
                    {{--                    {{ component('availability', {--}}
                    {{--                      stock: 'В наличии (23)'--}}
                    {{--                    }) }}--}}
                </div>
                <div class='product-info__watch'>Сейчас смотрит 23 чел.</div>
            </div>
        </div>
        <form action="" method="POST" class='product-info__inner js-cart-product-form'>
            @if($offer)
                <input type="hidden" name="offer_id" value="{{ $offer->id }}" />
                <input type="hidden" name="offer_type" value="{{ $product->type }}" />
                <div class='product-info__prices'>
                    <div class='product-info__price-wrapper'>
                        @if($offer->old_price)
                            <x-base.rubles
                                class='product-info__price product-info__price_old'
                                :number="$offer->old_price"
                            />
                        @endif
                        <x-base.rubles
                            class='product-info__price product-info__price_new'
                            :number="$offer->price"
                        />
                    </div>
                    @if($profit)
                        <div class='product-info__profit'>
                            <div>Выгода: {{ $profit->getPercent() }}%</div>
                            <div>
                                <x-base.rubles
                                    class='product-info__price product-info__price_new'
                                    :number="$profit->getTotal()"
                                />
                            </div>
                        </div>
                    @endif
                </div>
                <div class='product-info__calculate'>
                    <x-common.quantity-input field-name="count" label="Количество"/>
                </div>
                <button class='btn btn_type_secondary product-info__btn product-info__btn_type_cart'>
                    <x-base.svg-icon width="24" height="24" :icon="\App\Enums\Website\SvgIconName::CART()"/>
                    <span>В корзину</span>
                </button>
            @endif
            <div class='product-info__buttons'>
                <x-common.favorite-button :product="$product"/>
                <button class='btn product-info__btn product-info__btn_type_share' type="button">
                    <x-base.svg-icon width="26" height="26" :icon="\App\Enums\Website\SvgIconName::SHARE()"/>
                </button>
            </div>
        </form>
    </div>
    <div class='product-info__bottom'>
        <div class='product-info__bottom-item'>
            <div class='product-info__bottom-title'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::BOX()"/>
                <span>Доставка</span>
            </div>
            <div class='product-info__bottom-desc'>Стоимость адресной доставки - <span>300 ₽</span></div>
        </div>
        <div class='product-info__bottom-item'>
            <div class='product-info__bottom-title'>
                <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::CREDIT_CARD()"/>
                <span>Оплата</span>
            </div>
            <div class='product-info__bottom-desc'>Оплата картами VISA, MASTERCARD и МИРА при получении товара.</div>
        </div>
    </div>
</div>
