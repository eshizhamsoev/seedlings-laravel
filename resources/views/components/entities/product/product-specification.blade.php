<div class="product-specification">
    @foreach($lines as $line)
        <div class="product-specification__item">
            <div class="product-specification__title">{{ $line['label'] }}</div>
            <div class="product-specification__divider"></div>
            <div class="product-specification__name">{{ $line['value'] }}</div>
        </div>
    @endforeach
</div>
