<div class='product-block__slider-wrapper'>
    <div data-swiper-type='thumbs' class='product-block__slider-thumbs'>
        <div class='swiper'>
            <div class='swiper-wrapper'>
                @forelse($gallery as $image)
                    <div class='swiper-slide product-block__thumb-slide'>
                        {{ $image(\App\Models\Data\Product\Product::IMAGE_CONVERSION_DETAILS_LARGE) }}
                    </div>
                @empty
                    <div class='swiper-slide product-block__thumb-slide'>
                        <img src="{{ asset('/static/images/common/product.svg') }}" alt="" width="67" class="base-img"
                             height="90"/>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
    <div data-swiper-type='mainThumb' class='product-block__slider-main'>
        <div class='swiper'>
            <div class='swiper-wrapper'>
                @forelse($gallery as $image)
                    <div class='swiper-slide'>
                        <a data-fancybox='product-block-image'
                           href='{{ $image->getUrl(\App\Models\Data\Product\Product::IMAGE_CONVERSION_DETAILS_POPUP) }}'
                           class='a-clear product-block__main-slide'>
                            {{ $image(\App\Models\Data\Product\Product::IMAGE_CONVERSION_DETAILS_LARGE) }}
                        </a>
                    </div>
                @empty
                    <img src="{{ asset('/static/images/common/product.svg') }}" alt="" width="600" class="base-img"
                         height="800"/>
                @endforelse
            </div>
        </div>
    </div>
</div>
