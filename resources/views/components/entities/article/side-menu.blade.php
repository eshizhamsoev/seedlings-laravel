<div class='side-menu js-side-menu'>
    @if($title)
        <div class='side-menu__title'>{{ $title }}</div>
    @endif

    <ul class='side-menu__list ul-clear'>
        @foreach($articles as $article)
        <li class='side-menu__item {{ $article->id === $activeId ? 'active' : '' }}'>
            <x-base.link :url='$article->url'
               class='a-clear side-menu__link '>{{ $article->title }}</x-base.link>
        </li>
        @endforeach
    </ul>
</div>
