<div class='article'>
    <x-layouts.breadcrumbs :url="$article->url"/>
    <div class='container'>
        <div class='title title_type_primary'>{{$article->title}}</div>
        <div class='article__inner'>
            <div class='article__content'>
                <div class='article__text base-paragraph'>
                    @markdown($article->text)
                </div>
            </div>
            <div class='article__aside'>
                <x-entities.article.side-menu
                    :articles='$otherArticles'
                    :activeId='$article->id'
                    title='Другие статьи'
                ></x-entities.article.side-menu>
            </div>
        </div>
    </div>
</div>
