<div class="category-page">
    <x-layouts.breadcrumbs :url="$category->url"/>
    <x-block.catalog-filter :settings="$settings">
        @if($category->top_description)
            <x-slot name="afterFilterBar">
                <div class='catalog-filter__desc'>
                    @markdown($category->top_description)
                </div>
            </x-slot>
        @endif
        @if($category->children)
            <x-slot name="beforeFilterBar">
                <div class='catalog-filter__type-plants'>
                    <ul class='ul-clear type-plants'>
                        @foreach($category->children as $child)
                            <li class='type-plants__item'>
                                <x-base.link :url="$child->url"
                                             class="type-plants__item-link">{{ $child->name }}</x-base.link>
                            </li>
                        @endforeach
                    </ul>

                </div>
            </x-slot>
        @endif
    </x-block.catalog-filter>
</div>

<div class="article base-paragraph base-paragraph_type_grey">
    <div class="container">
        <h1 class="title title_type_primary">
            {{ $h1 }}
        </h1>
        <div class="article__text">
            @markdown($category->description)
        </div>
    </div>
</div>
