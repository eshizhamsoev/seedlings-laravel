<div class="calculate js-calculate">
    <button class="btn calculate__btn calculate__btn_minus" data-calculate="decrease" type="button" aria-label="Уменьшить количество">
        <x-base.svg-icon width="12" height="12" :icon="\App\Enums\Website\SvgIconName::MINUS()"/>
    </button>
    <input class="calculate__input" data-input step="{{ $step }}" {{ $min !== null ? 'min="10"' : ''  }} {{ $max !== null ? 'max="10"' : ''  }} type="number" value="{{ $value }}" aria-label="{{ $label }}"
           name="{{ $fieldName }}"/>
    <button class="btn calculate__btn" data-calculate="increment" type="button"  aria-label="Увеличить количество">
        <svg width="12" height="12">
            <use xlink:href="sprite.svg#plus"></use>
        </svg>
    </button>
</div>
