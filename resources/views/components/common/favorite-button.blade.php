<button class='btn favorite-button js-wishlist'
        type="button"
        data-product-id="{{ $id }}"
        data-text-add="{{ $textAdd }}"
        data-text-remove="{{ $textRemove }}"
        data-state="{{ $added ? 'added' : 'not_added' }}"
        title="{{ $added ? $textRemove : $textAdd }}">
    <x-base.svg-icon width="18" height="16" :icon="\App\Enums\Website\SvgIconName::HEART_FILLED()" class="favorite-button__icon-filled"/>
    <x-base.svg-icon width="18" height="16" :icon="\App\Enums\Website\SvgIconName::HEART()" class="favorite-button__icon-empty"/>
</button>
