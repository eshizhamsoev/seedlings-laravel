<div class='review {{ $small ? 'review_small' : '' }}'>
    <div class='review__top'>
        <div class='review__user'>
            @if($image)
                <span class='review__ava'>
                    <img src="{{ $image->getUrl(App\Models\Data\Review\Review::IMAGE_AUTHOR_MAIN_CONVERSION) }}"
                         alt="{{ $review->author }}"/>
                </span>
            @endif
            <div class='review__name-wrapper'>
                <div class='review__name'>{{ $review->author }}</div>
                @if(!$small && $review->role)
                    <div class='review__level'>{{ $review->role }}</div>
                @endif
                @if($small)
                    @if($review->rating)
                        <x-common.rating :value="$review->rating"/>
                    @endif
                @endif
            </div>
        </div>
        @if(!$small && $review->date)
            <div class='review__date-wrapper'>
                <div class='review__date'>{{ $review->date->format('d.m.Y') }}</div>
                @if($review->rating)
                    <x-common.rating :value="$review->rating"/>
                @endif
            </div>
        @endif
    </div>
    <div class='review__content'>
        @if($review->text)
            <div class='review__text'>
                @markdown($review->text)
            </div>
        @endif
        {{--        {% if params.options.video %}--}}
        {{--        <div class='review__video'>--}}
        {{--            {{ component('video', {--}}
        {{--              options: params.options.video--}}
        {{--            }) }}--}}
        {{--        </div>--}}
        {{--        {% endif %}--}}
        {{--        {% if params.options.photo %}--}}
        {{--        <div class='review__photo-wrapper'>--}}
        {{--            <div class='review__photo-title'>Фото</div>--}}
        {{--            <div class='review__photo-inner'>--}}
        {{--                {% for item in params.options.photo %}--}}
        {{--                <a href='{{ item }}' data-fancybox='review-image' class='a-clear review__photo-item'>--}}
        {{--                    <img loading="lazy" width='160' height='100' src="{{ item }}" alt=''>--}}
        {{--                </a>--}}
        {{--                {% endfor %}--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        {% endif %}--}}
    </div>
    @if(!$small && $review->answer)
        <div class='review__answer'>
            <div class='review__answer-header'>
                <img loading="lazy" width='156' height='11' src='{{ $logo }}' alt='Экоплант'>
                @if($review->answer->date)
                    <div class='review__date'>{{ $review->answer->date->format('d.m.Y') }}</div>
                @endif
            </div>
            <div class='review__answer-text'>
                @markdown($review->answer->text)
            </div>
        </div>
    @endif
</div>
