<div class='btn-arrows'>
    <button class='btn btn-arrows__item btn-arrows__item_prev'>
        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width="10" height="16" />
    </button>
    <button class='btn btn-arrows__item btn-arrows__item_next'>
        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width="10" height="16" />
    </button>
</div>
