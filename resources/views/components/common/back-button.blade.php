@if($previous)
    <x-base.link :href="$previous" class='come-back'>
        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW_BACK()" :width="16" :height="16"/>
        <span>Вернуться назад</span>
    </x-base.link>
@endif
