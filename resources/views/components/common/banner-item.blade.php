@push('body_start')
<script>
    function onImageLoad (el) {
        window.requestAnimationFrame(() => {
            const size = el.getBoundingClientRect().width
            if (!size) {
                return
            }
            el.onload = null
            const sizes = Math.ceil(size / window.innerWidth * 100) + 'vw'
            for (const source of Array.from(el.parentElement.querySelectorAll('source'))) {
                source.sizes = sizes
            }

        })
    }
</script>
<style>
    @foreach($mediaStyles as $mediaClass)
    @media screen @if($mediaClass['to'])and (max-width: {{$mediaClass['to']}}px)@endif @if($mediaClass['from'])and (min-width: {{$mediaClass['from']}}px)@endif         {
        .{{$mediaClass['name']}}{
            aspect-ratio:{{ $mediaClass['width'] }}/{{ $mediaClass['height'] }};
        }
    }
    @endforeach
</style>
@endpush
<picture class="banner-item">
    @foreach($images as $image)
        <source srcset="{{ $image['srcset'] }}"
                sizes="1px"
                media="(min-width: {{ $image['showFrom'] }}px)@if($image['showTo'])and (max-width: {{ $image['showTo'] }}px)@endif">
    @endforeach
    <img src="{{ $defaultImage['src'] }}" {{ $lazy ? 'loading="lazy"' : '' }} width="{{ $defaultImage['width'] }}"
         height="{{ $defaultImage['height'] }}"
         alt="" class="banner-item__image {{ implode(' ', $mediaClasses) }}"
         onload="onImageLoad(this)"/>
</picture>
