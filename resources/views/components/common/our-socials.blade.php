<div class='our-socials {{ $isWhite ? 'our-socials_white' : '' }}'>
    @foreach($links as $link)
        <x-base.link :href="$link['href']" :aria-label="$link['title']" class="our-socials__item">
            <x-base.svg-icon width="20" height="21" :icon="$link['icon']"/>
        </x-base.link>
    @endforeach
</div>
