<div class='rating'>
    @for($i = 0; $i < 5; $i++)
        <x-base.svg-icon
            class="rating__item {{ $value && $value > $i ? 'active' : '' }}"
            :width="14" :height="14" :icon="$icon"
        />
    @endfor
</div>
