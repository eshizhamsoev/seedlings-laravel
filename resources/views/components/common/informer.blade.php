<div class='last-block {{ $isGrey ? 'js-change-image' : ''  }}'>
    <div class='container'>
        <div class='last-block__inner'>
            <div class='last-block__img-wrapper'>
                <img loading="lazy"
                     class='last-block__image js-change-image__pic {{ $isGrey ? 'grey' : '' }}'
                     width='{{ $image['width'] }}' height='{{ $image['height'] }}'
                     src='{{ $image['src'] }}' alt=''>
            </div>
            <div class='title title_type_primary last-block__title'>{{ $title }}</div>
            <div class='last-block__subtitle'>{{ $subTitle }}</div>
            @if(!empty($actions))
                <div class='last-block__btn-wrapper'>
                    {{ $actions }}
                </div>
            @endif
        </div>
    </div>
</div>
