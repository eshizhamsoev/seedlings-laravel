<button class="btn cart-btn js-add-to-cart-button"
        type="button"
        data-product-id="{{ $id }}"
        data-type="{{ $type }}"
        @if($offerId)
        data-offer-id="{{ $offerId }}"
        @endif
        aria-label="Добавить в корзину {{ $name }}"
>
    <x-base.svg-icon width="20" height="20" :icon="\App\Enums\Website\SvgIconName::BASKET()"/>
</button>
