<div {{ $attributes->merge(['class' => 'product-card-row product-card-row_type_catalog']) }}>
    <div class="product-card-row__container">
        <x-base.link :url="$url" class="product-card-row__img-wrapper">
            @if($image)
                {{ $image($conversionName) }}
            @else
                <img src="{{ asset('/static/images/common/product.svg') }}" alt="" width="300" height="400"/>
            @endif
            @if($label)
                <span class='label'>
                    {{ $label->description }}
                </span>
            @endif
        </x-base.link>
        <div class="product-card-row__inner">
            <div class="product-card-row__name-wrapper">
                <x-base.link :url="$url" class='product-card-row__name'>{{ $name }}</x-base.link>

                {{--                <div class="product-card-row__count"><span>Остаток:</span>2 шт.</div>--}}


                {{--                <div class="product-card-row__rating">--}}
                {{--                    <a href="#!" class="rating a-clear">--}}
                {{--                        <svg class="rating__item active" width="14" height="14">--}}
                {{--                            <use xlink:href="sprite.svg#star"></use>--}}
                {{--                        </svg>--}}
                {{--                        <svg class="rating__item" width="14" height="14">--}}
                {{--                            <use xlink:href="sprite.svg#star"></use>--}}
                {{--                        </svg>--}}
                {{--                        <svg class="rating__item" width="14" height="14">--}}
                {{--                            <use xlink:href="sprite.svg#star"></use>--}}
                {{--                        </svg>--}}
                {{--                        <svg class="rating__item" width="14" height="14">--}}
                {{--                            <use xlink:href="sprite.svg#star"></use>--}}
                {{--                        </svg>--}}
                {{--                        <svg class="rating__item" width="14" height="14">--}}
                {{--                            <use xlink:href="sprite.svg#star"></use>--}}
                {{--                        </svg>--}}
                {{--                    </a>--}}

                {{--                    <span class="product-card-row__rating-count">(5)</span>--}}
                {{--                </div>--}}

                <div class="product-card-row__options">

                </div>
            </div>

            <div class="product-card-row__price-wrapper">
                @if($offer)
                    @if($offer->old_price)
                        <x-base.rubles
                            class='product-card-row__price product-card-row__price_old'
                            :number="$offer->old_price"
                        />
                    @endif
                    <x-base.rubles
                        class='product-card-row__price product-card-row__price_new'
                        :number="$offer->price"
                    />
                @endif
            </div>

            <div class="product-card-row__cart-wrapper">
                <x-common.product-card.product-cart-button :product="$product" class="product-card-row__cart-btn"/>

                @if($actions)
                    {{ $actions }}
                @endif

            </div>


        </div>
    </div>
</div>
