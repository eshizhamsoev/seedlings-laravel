<div {{ $attributes->merge(['class' => 'product-card']) }}>
    <div class="product-card__img-wrapper">
        <x-base.link :url="$url" class="product-card__img-link">
            @if($image)
                {{ $image($conversionName) }}
            @else
                <img src="{{ asset('/static/images/common/product.svg') }}" alt="" width="300" height="400" />
            @endif
        </x-base.link>

        <span class='product-card__labels labels'>
            @if($label)
                <span class='labels__item {{ 'labels__item_' . $label->value }}'>
                    {{ $label->description }}
                </span>
            @endif
            {{--                {% if params.options.favorite %}--}}
            {{--                <button class='btn labels__favorite'>--}}
            {{--                    <svg width="14" height="14">--}}
            {{--                        <use--}}
            {{--                            xlink:href='sprite.svg#heart-filled'></use>--}}
            {{--                    </svg>--}}
            {{--                </button>--}}
            {{--                {% endif %}--}}
        </span>
    </div>
    <x-base.link :url="$url" class='product-card__name'>{{ $name }}</x-base.link>
    {{--    {% if params.options.count %}--}}
    {{--    <div class='product-card__count'><span>Остаток:</span> {{ params.options.count }} шт.</div>--}}
    {{--    {% else %}--}}
    {{--    <div class='product-card__availability'>--}}
    {{--        {{ component('availability', {--}}
    {{--          stock: params.options.stock--}}
    {{--        }) }}--}}
    {{--    </div>--}}
    {{--    {% endif %}--}}
    <div class='product-card__bottom'>
        <div class='product-card__prices'>
            @if($offer)
                @if($offer->old_price)
                    <x-base.rubles
                        class='product-card__price product-card__price_old'
                        :number="$offer->old_price"
                    />
                @endif
                <x-base.rubles
                    class='product-card__price product-card__price_new'
                    :number="$offer->price"
                />
            @endif
        </div>
        <div class='product-card__cart'>
            <x-common.product-card.product-cart-button :product="$product"/>
        </div>
    </div>
</div>
