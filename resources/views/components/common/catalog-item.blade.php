<div class="catalog-item">
    <x-base.link :url="$category->url" class="catalog-item__img-wrapper">
        @if($media)
            {{ $media(\App\Models\Data\Category\Category::IMAGE_CONVERSION_LIST) }}
        @else
            <img
                src="{{ asset('/static/images/common/category.svg') }}"
                alt=""
                width="400"
                height="400"
                class="base-img"
            />

        @endif
    </x-base.link>
    <x-base.link :url="$category->url" class="catalog-item__link">
        {{ $category->name }}
    </x-base.link>
</div>
