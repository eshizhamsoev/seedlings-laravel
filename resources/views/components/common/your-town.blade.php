<div class="your-town">
    <div class="your-town__title">Ваш город:</div>
    <x-base.modal-opener class="btn your-town__name" :modal-type="\App\Enums\Website\ModalType::CITY_LIST()">
        {{ $city->name }}
        <x-base.svg-icon width="8" height="3" :icon="\App\Enums\Website\SvgIconName::TRIANGLE()"/>
    </x-base.modal-opener>
</div>
