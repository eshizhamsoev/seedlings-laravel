<ul {{ $attributes->merge(['class' => $blockName . ' ul-clear']) }}>
    @foreach($links as $link)
        <li class="{{ $blockName . '__item' }}">
            <x-base.link :href="$link->getLink()" :class="$blockName . '__link'">
                {{ $link->getName() }}
            </x-base.link>
        </li>
    @endforeach
    @if($slot)
        <li class="{{ $blockName . '__item' }}">
            {{ $slot }}
        </li>
    @endif
</ul>
