<x-base.link :url="$url" class='a-clear news__item'>
    @if($isSlide)
        <div class='news__img-wrapper '>
            @if($image)
                {{ $image($conversion) }}
            @else
                <img src="{{ asset('/static/images/common/product.svg') }}" alt="" width="270" height="200" />
            @endif
        </div>
    @else
        <div class='news__img-wrapper news__img-wrapper_blog '>
            @if($image)
                {{ $image($conversion) }}
            @else
                <img src="{{ asset('/static/images/common/product.svg') }}" alt="" width="300" height="274" />
            @endif
        </div>
    @endif
    <div class='news__content'>
        <div class='news__name '>{{ $title }}</div>
        <div class='news__text'>
            {{ $description }}
        </div>
    </div>
</x-base.link>
