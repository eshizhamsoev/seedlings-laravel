<div class='base-tabs js-tabs'>
    <nav class='base-tabs__nav'>
        <ul class='ul-clear base-tabs__menu' role="tablist">
            @foreach($tabsContainer->getTabs() as $tab)
                <li class='base-tabs__menu-item'>
                    <button role="tab" aria-selected="{{ $tab->isActive() ? 'true' : 'false' }}"
                            aria-controls="{{ $tab->getId() }}" tabindex="{{ $tab->isActive() ? '0' : '-1' }}"
                            class='btn base-tabs__menu-btn'>{{ $tab->getHeading() }}</button>
                </li>
            @endforeach
        </ul>
    </nav>
    {{$slot}}

    {{--    @dump($component)--}}
</div>
