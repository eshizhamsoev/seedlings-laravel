@if ($paginator->hasPages())
    <nav>
        <ul class="ul-clear pagination">
            {{-- Previous Page Link --}}
            @if (!$paginator->onFirstPage())
                <li class='pagination__item pagination__item_prev'>
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')" class="a-clear pagination__btn">
                        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width='8' height='12' />
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span class="btn pagination__item pagination__item_type_dots">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class='pagination__item' aria-current="page"><span class='a-clear pagination__btn pagination__page pagination__page_current'>{{ $page }}</span></li>
                        @else
                            <li class='pagination__item'><a href="{{ $url }}" class='a-clear pagination__btn pagination__page'>{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class='pagination__item pagination__item_next'>
                    <a class='a-clear pagination__btn pagination__btn_next' href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width='8' height='12' />
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif
