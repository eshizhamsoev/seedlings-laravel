<input
    {{ $attributes->merge(['class' => 'text-field']) }}
    {{ $isDisabled ? 'disabled' : '' }}
    name='{{ $name}}'
    value='{{ $value}}'
    type='{{ $type->value }}'
    placeholder='{{ $placeholder }}'
    aria-label="{{ $label }}"
>
