@if($rewrite)
    <img loading="lazy" src="{{ $rewrite }}" width="{{ $width }}" height="{{ $height }}" {{ $attributes }} />
@else
    <svg width="{{ $width }}" height="{{ $height }}" {{ $attributes }}>
        <use xlink:href='{{ $href }}'></use>
    </svg>
@endif
