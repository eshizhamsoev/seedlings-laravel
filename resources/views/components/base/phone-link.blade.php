<a href='tel:{{ $phoneNumber }}' {{ $attributes->merge(['class' => 'a-clear']) }}>{{ $slot->isNotEmpty() ? $slot : $phoneFormatted }}</a>
