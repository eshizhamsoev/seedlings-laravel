<button {{ $attributes->merge([
    'data-type' => "fancybox-async",
    'data-src' => $type
]) }}>
    {{ $slot }}
</button>
