<label {{ $attributes->only(['class'])->merge([
    'class' => 'base-checkbox'
]) }}>
    <input
        {{ $attributes->except(['class'])->merge([
    'class' => 'base-checkbox__input'
]) }}
        {{ $isChecked ? 'checked' : '' }}
        {{ $isDisabled ? 'disabled' : '' }}
        name='{{ $name}}'
        value='{{ $value}}'
        type='{{ $type->value }}'
    >
    <span class="base-checkbox__text">{{ $label }}</span>
</label>
