@if($isCurrent)
    <span {{ $attributes->merge(['class' => $currentLinkClass]) }}>
        {{ $slot }}
    </span>
@else
    <a {{ $attributes->merge(['class' => 'a-clear'])  }} target="{{$target}}" href="{{ $href }}">
        {{ $slot }}
    </a>
@endif
