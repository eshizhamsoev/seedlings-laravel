@aware(['tabsContainer'])
@php($id = $tabsContainer->addTab($heading, $isActive ?? false))
<div
    role="tabpanel"
    id="{{$id}}"
    {{ !empty($isActive) ? '' : 'hidden' }}
    class='base-tabs__content'>
    {{ $slot }}
</div>
