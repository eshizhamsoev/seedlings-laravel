@extends('layouts.base')
@section('html_class', 'page')
@section('content')
    <div>
        <x-layouts.header/>
        <main>
            @yield('before-page-content')
            @yield('page-content')
        </main>
        <x-layouts.footer/>
    </div>
    <x-layouts.mobile-menu/>
@endsection
