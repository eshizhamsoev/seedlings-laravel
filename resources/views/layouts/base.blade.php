<!DOCTYPE html>
<html lang="ru">
<head>
    <title>@yield('title')</title>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('head')
</head>
<body>
@stack('body_start')
@yield('content')

@stack('body_end_before_script')
<script src="{{ mix('/js/app.js') }}" type="module"></script>
<div id="fancy-modals" hidden></div>
@stack('body_end')
</body>
</html>
