@extends('layouts.common')
@section('before-page-content')
    <x-layouts.breadcrumbs :url="$url"/>
    <div class="container">
        <h1 class="title title_type_primary">{{ $url->entity->name }}</h1>
    </div>
@endsection
