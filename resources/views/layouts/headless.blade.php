@extends('layouts.base')
@section('html_class', 'page')
@section('content')
    <div>
        <x-layouts.small-header/>
        <main>
            @yield('page-content')
        </main>
        <x-layouts.footer/>
    </div>
    <x-layouts.mobile-menu/>
@endsection
