const url =
  'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';
const token = '47084528251b430fd3ccb9bf10803087949d31d8';

const abortController = new AbortController();

export function abort() {
  abortController.abort();
}

export function getAddressSuggests(query: string) {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Token ${token}`,
    },
    body: JSON.stringify({ query }),
    signal: abortController.signal,
  };

  return fetch(url, options).then((response) => response.json());
}
