import './init/sprite';
import './init/mask';
import { initMmenu } from './init/myMmenu';
import { initCalculate } from './init/calculate';
import { initTabs } from './init/tabs';
import { sideMenu } from './init/sideMenu';
import { productCatalog } from './init/productCatalog';
import { activeItem } from './init/activeItem';
import { initSlider } from './init/swiper';
import { oneActiveItem } from './init/oneActiveItem';
import { scrollUp } from './init/scrollUp';
import { changeImage } from './init/changeImage';
import { additionalContent } from './init/moreMenu';
import { mobileHideContent } from './init/mobileHideContent';
import { Api } from './lib/api/api';
import { Cart } from './lib/cart/cart';
import { Wishlist } from './lib/wishlist/wishlist';
import { initCart } from './init/cart';
import { initFancy } from './init/fancy';
import { initWishlistButton, initWishlistPage } from './init/wishlist';

initFancy();
const api = new Api(document.head.querySelector('[name="csrf-token"]').content);

// const serverData = window[SERVER_DATA]
const cart = new Cart(api, []);
initCart(cart);

const wishList = new Wishlist(api);
initWishlistButton(wishList);
initWishlistPage(wishList);

const links = document.querySelectorAll('.js-footer-menu-link');
activeItem(links);

const filterRange = document.querySelector('.js-filter-range');
if (filterRange) {
  import('./init/range').then(({ connectInputRange }) =>
    connectInputRange(filterRange)
  );
}

const cartPage = document.querySelector('#cart-page');

if (cartPage) {
  import('./init/cart-page.ts').then(({ initCartPage }) =>
    initCartPage(cartPage, { cart, api })
  );
}

const checkoutPage = document.querySelector('#checkout-page');

if (checkoutPage) {
  import('./init/checkout-page.ts').then(({ initCheckoutPage }) =>
    initCheckoutPage(checkoutPage, { api })
  );
}

const searchInput = document.querySelector('.js-search-input');
const searchBlock = document.querySelector('.js-search-block');
const searchResults = document.querySelector('.js-search-results');
if (searchInput && searchBlock && searchResults) {
  searchInput.addEventListener(
    'focus',
    () => {
      import('./init/search-form.ts').then(({ initSearch }) => {
        initSearch(
          {
            input: searchInput,
            block: searchBlock,
            results: searchResults,
          },
          api
        );
      });
    },
    {
      once: true,
    }
  );
}

const catalogList = document.querySelector('.js-catalog-list');
productCatalog(catalogList);

const sideMenuElement = document.querySelector('.js-side-menu');
sideMenu(sideMenuElement);

const tabs = document.querySelectorAll('.js-tabs');
initTabs(tabs);

const sliders = document.querySelectorAll('[data-swiper-type]');
for (const slider of sliders) {
  initSlider(slider);
}

const calculateElements = document.querySelectorAll('.js-calculate');
for (const item of calculateElements) {
  initCalculate(item);
}

const plants = document.querySelectorAll('.js-type-plants');
oneActiveItem(plants);

const btnUp = document.querySelector('.js-aside-btn');
scrollUp(btnUp, 540);

const mmenus = document.querySelectorAll('[data-mmenu]');
for (const item of mmenus) {
  initMmenu(item);
}

const colorImage = document.querySelector('.js-change-image');
changeImage(colorImage);

const items = document.querySelectorAll('.js-additional-content');
for (const item of items) {
  additionalContent(item);
}

const mobileHideMenus = document.querySelectorAll('[data-mobile-hide-button]');
for (const item of mobileHideMenus) {
  mobileHideContent(item);
}
