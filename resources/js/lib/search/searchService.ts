import { Api } from '../api/api';

const SEARCH_ENDPOINT = '/api/website/search';

export type SearchResult = {
  id: number;
  name: string;
  image: string;
  link: string;
};

export class SearchService {
  private api: Api;

  constructor(api: Api) {
    this.api = api;
  }

  search(query: string): Promise<SearchResult[]> {
    return this.api.get(`${SEARCH_ENDPOINT}?q=${query}`);
  }
}
