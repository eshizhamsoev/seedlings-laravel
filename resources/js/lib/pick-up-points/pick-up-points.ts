import { Api } from '../api/api';
import { City, Point } from './types';

const ENDPOINTS = {
  CITIES: '/api/website/pick-up-points/cities',
  POINTS: '/api/website/pick-up-points/points',
};

export class PickUpPointsService {
  private api: Api;

  constructor(api: Api) {
    this.api = api;
  }

  getCities(): Promise<City[]> {
    return this.api.get<City[]>(ENDPOINTS.CITIES);
  }

  getPoints(cityName: string): Promise<Point[]> {
    const params = new URLSearchParams();
    params.set('city', cityName);
    return this.api.get<Point[]>(`${ENDPOINTS.POINTS}?${params}`);
  }
}
