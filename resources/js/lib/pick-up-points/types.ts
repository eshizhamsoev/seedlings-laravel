export type City = string;

export type Point = {
  id: number;
  code: string;
  name: string;
  lat: number;
  lng: number;

  city: string;
  address: string;
  addressComment: string;
  workTime: string;
};
