import { Api } from '../api/api';

const TOGGLE = '/api/website/wishlist/toggle';
const REMOVE = '/api/website/wishlist/remove';

export class Wishlist {
  private api: Api;

  constructor(api: Api) {
    this.api = api;
  }

  async toggle(productId: number) {
    return this.api.post<{ active: boolean }>(TOGGLE, {
      id: productId,
    });
  }

  async remove(productId: number) {
    return this.api.post(REMOVE, {
      id: productId,
    });
  }
}
