export enum CartItemType {
  SIMPLE = 'simple',
  VARIANT = 'variant',
}

export type CartIdentifier = {
  type: CartItemType;
  id: number;
};

export type CartRowId = string;

export type CartItem = {
  identifier: CartIdentifier;
  name: string;
  price: number;
  count: number;
  link: string;
};

export type CartData = {
  total: number;
  count: number;
  items: CartItem[];
};
