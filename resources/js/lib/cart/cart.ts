import { CartData, CartIdentifier, CartRowId } from './types';

type CartSubscriber = (newData: CartData, oldData: CartData | null) => unknown;

type Api = {
  get<T>(url: string, config?: RequestInit): Promise<T>;
  post<T>(url: string, data: unknown, config?: RequestInit): Promise<T>;
};

enum CART_ENDPOINTS {
  ADD = '/api/website/cart/add',
  UPDATE = '/api/website/cart/update',
  REMOVE = '/api/website/cart/remove',
  INFO = '/api/website/cart',
}

export enum CART_EVENTS {
  PRODUCT_ADD,
  PRODUCT_UPDATE,
  PRODUCT_REMOVE,
  INFORMATION_UPDATE,
}

export class Cart {
  private api: Api;

  private currentData: CartData | null = null;

  private abortController: AbortController;

  private eventListeners: Map<CART_EVENTS, CartSubscriber[]>;

  private firedEvents: CART_EVENTS[] = [];

  constructor(api: Api, currentData: CartData | null = null) {
    this.currentData = currentData;
    this.api = api;
    this.abortController = new AbortController();
    this.eventListeners = new Map();
  }

  async add(product: CartIdentifier, count = 1) {
    await this.api.post(CART_ENDPOINTS.ADD, {
      product,
      count,
    });

    this.fetchData(CART_EVENTS.PRODUCT_ADD);
  }

  async remove(product: CartIdentifier) {
    await this.api.post(CART_ENDPOINTS.REMOVE, {
      product,
    });

    this.fetchData(CART_EVENTS.PRODUCT_REMOVE);
  }

  async update(rowId: CartRowId, count: number) {
    await this.api.post(CART_ENDPOINTS.UPDATE, {
      rowId,
      count,
    });

    this.fetchData(CART_EVENTS.PRODUCT_UPDATE);
  }

  async fetchData(event: CART_EVENTS) {
    this.firedEvents.push(event);
    const prevData = this.currentData;
    this.abortController.abort();
    this.abortController = new AbortController();
    const data = await this.api.get<CartData>(CART_ENDPOINTS.INFO, {
      signal: this.abortController.signal,
    });

    this.currentData = data;
    const events = this.firedEvents;
    this.firedEvents = [];
    events.push(CART_EVENTS.INFORMATION_UPDATE);

    for (const firedEvents of events) {
      const listeners = this.eventListeners.get(firedEvents);
      if (listeners) {
        for (const listener of listeners) {
          listener(data, prevData);
        }
      }
    }
  }

  on(eventName: CART_EVENTS, subscribeFunction: CartSubscriber) {
    const currentListeners = this.eventListeners.get(eventName) || [];
    this.eventListeners.set(eventName, [
      ...currentListeners,
      subscribeFunction,
    ]);
  }

  off(eventName: CART_EVENTS, subscribeFunction: CartSubscriber) {
    const currentListeners = this.eventListeners.get(eventName) || [];
    const filtered = currentListeners.filter(
      (item) => item !== subscribeFunction
    );
    this.eventListeners.set(eventName, filtered);
  }
}
