import { ErrorResponse } from './error-response';
import { ValidationErrorResponse } from './validation-error-response';

const defaultFetchConfig = {
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
};

export class Api {
  private csrfToken: string;

  constructor(csrfToken: string) {
    this.csrfToken = csrfToken;
  }

  static fetch<T>(url: string, config?: RequestInit): Promise<T> {
    return fetch(url, config).then((res) => {
      if (res.ok) {
        return res.json();
      }
      if (res.status === 422) {
        return Promise.reject(new ValidationErrorResponse(res));
      }
      return Promise.reject(new ErrorResponse(res));
    });
  }

  get<T>(url: string, config?: RequestInit): Promise<T> {
    return Api.fetch<T>(url, {
      ...defaultFetchConfig,
      headers: {
        ...defaultFetchConfig.headers,
        'X-CSRF-Token': this.csrfToken,
      },
      ...config,
    });
  }

  post<T>(url: string, data: object, config?: RequestInit) {
    return Api.fetch<T>(url, {
      ...defaultFetchConfig,
      headers: {
        ...defaultFetchConfig.headers,
        'X-CSRF-Token': this.csrfToken,
      },
      body: JSON.stringify(data),
      method: 'post',
      ...config,
    });
  }
}
