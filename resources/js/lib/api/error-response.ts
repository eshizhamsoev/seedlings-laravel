export class ErrorResponse {
  private response: Response;

  constructor(response: Response) {
    if (response.ok) {
      throw new Error('Logical error: Error response cannot be ok');
    }
    this.response = response;
  }

  getResponse() {
    return this.response;
  }

  getCode() {
    return this.response.status;
  }
}
