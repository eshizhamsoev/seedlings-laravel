import { ErrorResponse } from './error-response';

export class ValidationErrorResponse extends ErrorResponse {
  async getErrors(): Promise<{ [field: string]: string[] }> {
    return this.getResponse().json();
  }
}
