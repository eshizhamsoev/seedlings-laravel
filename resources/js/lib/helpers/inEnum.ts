export function inEnum<T>(
  item: unknown,
  enumObject: { [key: string]: T }
): item is T {
  return Object.values(enumObject).includes(item as T);
}
