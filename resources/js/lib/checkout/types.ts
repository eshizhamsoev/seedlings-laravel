export type Customer = {
  name: string | null;
  email: string | null;
  phone: string | null;
};

export enum DeliveryType {
  TO_DOOR = 'to-door',
  PICKUP = 'pickup',
}

export enum PaymentMethod {
  ONLINE = 'online',
  ON_PLACE = 'on-place',
}

export type OrderData = {
  customer: Customer;
  comment: string | null;
  deliveryType: DeliveryType | null;
  paymentMethod: PaymentMethod | null;
  address: string | null;
  pickUpPointId: number | null;
};

export type DeliveryLine = {
  label: string;
  value: string;
};

export type DeliveryInfo = DeliveryLine[];

export type DecodedAddress = {
  lat: number | null;
  lng: number | null;
};

export type OrderResponse = {
  data: OrderData;
  total: number;
  deliveryInfo: DeliveryInfo | null;
  decodedAddress: DecodedAddress | null;
  pickPointCity: string | null;
};
