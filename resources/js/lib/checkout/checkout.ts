import { OrderData, OrderResponse } from './types';
import { Api } from '../api/api';

const ENDPOINTS = {
  INFO: '/api/website/order',
  SAVE: '/api/website/order',
  FINISH: '/api/website/order/finish',
};

export class CheckoutService {
  private api: Api;

  constructor(api: Api) {
    this.api = api;
  }

  getInfo() {
    return this.api.get<OrderResponse>(ENDPOINTS.INFO);
  }

  save(order: OrderData) {
    const abortController = new AbortController();
    return {
      promise: this.api.post<OrderResponse>(ENDPOINTS.SAVE, order, {
        signal: abortController.signal,
      }),
      abort: () => abortController.abort(),
    };
  }

  async finish(order: OrderData) {
    return this.api.post(ENDPOINTS.FINISH, order);
  }
}
