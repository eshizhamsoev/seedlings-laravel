declare module '@fancyapps/ui' {
  type SlideType = 'image' | 'html' | 'inline' | 'iframe';
  type Slide = {
    src: string;
    type: SlideType;
  };

  type Slides = Slide[];

  type FancyBoxEventName =
    | 'init'
    | 'initLayout'
    | 'initCarousel'
    | 'ready'
    | 'load'
    | 'reveal'
    | 'done'
    | 'keydown'
    | 'shouldClose'
    | 'closing'
    | 'destroy';

  // eslint-disable-next-line no-use-before-define
  type FancyBoxEventCallBack = (fancybox: Fancybox, slide: unknown) => void;

  type Events = { [key in FancyBoxEventName]: FancyBoxEventCallBack };

  // Todo: concrete type for keys and value
  type KeyboardActions = { [key: string]: string };

  type AllFancyOptions = {
    startIndex: number;
    preload: number;
    infinite: number;
    showClass: string | false;
    hideClass: string | false;
    animated: boolean;
    hideScrollbar: boolean;
    parentEl: HTMLElement | null;
    mainClass: string | null;
    autoFocus: boolean;
    trapFocus: boolean;
    placeFocusBack: boolean;
    click: function;
    closeButton: string;
    dragToClose: boolean;
    keyboard: KeyboardActions | false;
    // Todo: add type
    template: unknown;
    // Todo: add type
    l10n: unknown;
    groupAll: boolean;
    groupAttr: string | false;
    caption: function | null;
    on: Partial<Events>;
    // Todo: add type
    Carousel: unknown;
  };

  export type FancyOptions = Partial<AllFancyOptions>;

  // Todo: add type
  type Carousel = unknown;

  declare class Fancybox {
    constructor(slides: Slides, options?: FancyOptions);

    static show(slides: Slides, options?: FancyOptions);

    public $backdrop: HTMLElement;

    public $carousel: HTMLElement;

    public $container: HTMLElement;

    public $root: HTMLElement;

    public Carousel: Carousel;

    public close(): void;
  }
}
