export function init(el: HTMLElement) {
  const searchContainer = el.querySelector<HTMLElement>('.js-search-container');
  const search = el.querySelector<HTMLInputElement>('[name="city"]');
  const items = Array.from(
    el.querySelectorAll<HTMLElement & { dataset: { searchValue: string } }>(
      '[data-search-value]'
    )
  );

  const noResults = el.querySelector<HTMLElement>('.js-search-no-results');

  if (!search || !items.length || !searchContainer) {
    throw new Error('Elements for search are not found');
  }

  search.addEventListener('input', (e) => {
    const input = e.target;
    if (!input || !(input instanceof HTMLInputElement)) {
      return;
    }
    searchContainer.style.height = `${searchContainer.clientHeight}px`;
    const value = input?.value;
    let allHidden = true;
    for (const item of items) {
      const hasValue = item.dataset.searchValue
        .toLowerCase()
        .includes(value.toLowerCase());
      item.hidden = !hasValue;
      allHidden &&= !hasValue;
    }
    if (noResults) {
      noResults.hidden = !allHidden;
    }
  });
}
