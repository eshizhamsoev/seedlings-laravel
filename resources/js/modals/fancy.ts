import '@fancyapps/ui/dist/fancybox.css';

import { Fancybox, FancyOptions } from '@fancyapps/ui';

export enum MODAL_TYPE {
  CITY_LIST = 'city-list',
  FANCY_LOGIN = 'fancy-login',
  FANCY_REGISTRATION = 'fancy-registration',
  FANCY_RECOVERY = 'fancy-recovery',
  ADD_TO_CART = 'add-to-cart',
}

type InitializationModule = {
  init(element: HTMLElement): void;
};

type AsyncModule<T> = () => Promise<T>;

const initializationModules: Partial<
  {
    [key in MODAL_TYPE]: AsyncModule<InitializationModule>;
  }
> = {
  [MODAL_TYPE.CITY_LIST]: () => import('./initializers/city-list'),
};

const closeButtonSelector = '[data-action="close"]';

function registerCloseOnButton(fancybox: Fancybox) {
  fancybox.$carousel.addEventListener('click', ({ target }: MouseEvent) => {
    if (target instanceof HTMLElement && target.closest(closeButtonSelector)) {
      fancybox.close();
    }
  });
}

function loadModal(url: string, options?: FancyOptions): Promise<Fancybox> {
  return fetch(url)
    .then((resp) => resp.text())
    .then((text) =>
      Fancybox.show(
        [
          {
            src: text,
            type: 'html',
          },
        ],
        options
      )
    );
}

export function openModal(modalName: MODAL_TYPE): Promise<Fancybox> {
  if (!Object.values(MODAL_TYPE).includes(modalName)) {
    throw new Error(`Modal "${modalName}" does not exists`);
  }
  const moduleLoader = initializationModules[modalName];
  const initializationModule = moduleLoader ? moduleLoader() : null;
  return loadModal(`/api/website/modals/${modalName}`, {
    mainClass: 'fancy-cancel-padding',
    on: {
      ready(fancybox) {
        registerCloseOnButton(fancybox);
        if (initializationModule) {
          initializationModule.then(({ init }) => init(fancybox.$carousel));
        }
      },
    },
  });
}
