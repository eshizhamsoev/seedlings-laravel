import { Wishlist } from '../lib/wishlist/wishlist';

const DATA_ID_PROPERTY_NAME = 'productId';
const TOGGLE_WISHLIST_SELECTOR = '.js-wishlist';

const STATES = {
  active: 'added',
  passive: 'not_added',
};

export function initWishlistButton(wishlist: Wishlist) {
  document.addEventListener('click', ({ target }) => {
    if (!(target instanceof Element)) {
      return;
    }
    const el = target.closest(TOGGLE_WISHLIST_SELECTOR);
    if (!(el instanceof HTMLElement)) {
      return;
    }

    const id = el.dataset[DATA_ID_PROPERTY_NAME] ?? null;
    if (id) {
      wishlist.toggle(parseInt(id, 10)).then(({ active }) => {
        el.dataset.state = active ? STATES.active : STATES.passive;
      });
    }
  });
}

const WISHLIST_ROW = '.js-wishlist-row';
const WISHLIST_ROW_REMOVE_BUTTON = '.js-wishlist-row-remove';

export function initWishlistPage(wishlist: Wishlist) {
  document.addEventListener('click', ({ target }) => {
    if (!(target instanceof Element)) {
      return;
    }
    const el = target.closest(WISHLIST_ROW_REMOVE_BUTTON);
    if (!(el instanceof HTMLElement)) {
      return;
    }

    const id = el.dataset[DATA_ID_PROPERTY_NAME] ?? null;
    if (id) {
      wishlist.remove(parseInt(id, 10)).then(() => {
        const parent = el.closest(WISHLIST_ROW);
        if (parent) {
          parent.remove();
        }
      });
    }
  });
}
