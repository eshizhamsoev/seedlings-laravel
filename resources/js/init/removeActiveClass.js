export function removeActiveClass(items) {
  for (const item of items) {
    item.classList.remove('active');
  }
}
