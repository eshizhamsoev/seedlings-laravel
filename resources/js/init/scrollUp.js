export function scrollUp(button, offset) {
  if (!button) {
    return;
  }
  window.addEventListener('scroll', () => {
    if (window.pageYOffset > offset) {
      button.classList.add('active');
    } else {
      button.classList.remove('active');
    }
  });
  button.addEventListener('click', () => {
    window.scrollTo(0, 0);
  });
}
