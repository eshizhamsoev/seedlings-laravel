import { Cart, CART_EVENTS } from '../lib/cart/cart';
import { MODAL_TYPE, openModal } from '../modals/fancy';
import { CartData, CartItemType } from '../lib/cart/types';
import { inEnum } from '../lib/helpers/inEnum';

const ADD_CART_SELECTOR = '.js-add-to-cart-button';
const PRODUCT_FORM_SELECTOR = '.js-cart-product-form';

const CART_QUANTITY_SELECTOR = '.js-cart-quantity';

function updateCartQuantity({ count }: CartData) {
  const nodes = document.querySelectorAll<HTMLElement>(CART_QUANTITY_SELECTOR);
  for (const item of Array.from(nodes)) {
    item.textContent = count.toString();
  }
}

function cartButtonClick(cart: Cart, button: HTMLElement) {
  const { type, offerId } = button.dataset;
  if (type === CartItemType.SIMPLE && offerId) {
    cart.add({
      id: +offerId,
      type,
    });
  }
}

function productFormSubmit(cart: Cart, form: HTMLFormElement) {
  const formData = new FormData(form);
  const offerId = formData.get('offer_id')?.toString();
  const offerType = formData.get('offer_type')?.toString();
  const count = formData.get('count')?.toString();
  if (!offerId || !offerType || !count) {
    throw new Error(
      `Product form data is invalid: offerId: ${offerId}, offerType: ${offerType}, count: ${count}`
    );
  }

  if (!inEnum(offerType, CartItemType)) {
    return;
  }

  cart.add(
    {
      id: parseInt(offerId, 10),
      type: offerType,
    },
    parseInt(count, 10)
  );
}

export function initCart(cart: Cart) {
  cart.on(CART_EVENTS.INFORMATION_UPDATE, updateCartQuantity);
  cart.on(CART_EVENTS.PRODUCT_ADD, () => openModal(MODAL_TYPE.ADD_TO_CART));

  document.addEventListener('click', ({ target }) => {
    if (!(target instanceof Element)) {
      return;
    }
    const el = target.closest(ADD_CART_SELECTOR);
    if (!(el instanceof HTMLElement)) {
      return;
    }
    if (el) {
      cartButtonClick(cart, el);
    }
  });

  document.addEventListener('submit', (e) => {
    const { target } = e;
    if (!(target instanceof Element)) {
      return;
    }
    if (
      !target.matches(PRODUCT_FORM_SELECTOR) ||
      !(target instanceof HTMLFormElement)
    ) {
      return;
    }
    e.preventDefault();
    productFormSubmit(cart, target);
  });
}
