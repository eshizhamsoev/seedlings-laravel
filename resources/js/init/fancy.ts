import { MODAL_TYPE, openModal } from '../modals/fancy';

export function initFancy() {
  document.addEventListener('click', ({ target }) => {
    if (!(target instanceof HTMLElement)) {
      return;
    }
    const el = target.closest('[data-type="fancybox-async"]');
    if (!(el instanceof HTMLElement)) {
      return;
    }
    openModal(el.dataset.src as MODAL_TYPE);
  });
}
