export function changeImage(element) {
  if (!element) {
    return;
  }
  const image = element.querySelector('.js-change-image__pic');
  const btn = element.querySelector('.js-change-image__btn');
  btn.addEventListener('mouseenter', () => {
    image.classList.toggle('grey');
  });
  btn.addEventListener('mouseleave', () => {
    image.classList.toggle('grey');
  });
}
