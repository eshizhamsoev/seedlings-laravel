import { removeActiveClass } from './removeActiveClass';

export function sideMenu(node) {
  if (!node) {
    return;
  }
  const items = node.querySelectorAll('.js-side-menu__item');
  for (const item of items) {
    item.addEventListener('click', () => {
      if (
        !item.classList.contains('active') &&
        !item.classList.contains('side-menu__link_exit')
      ) {
        removeActiveClass(items);
        item.classList.add('active');
      }
    });
  }
}
