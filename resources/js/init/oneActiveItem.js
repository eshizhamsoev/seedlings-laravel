import { removeActiveClass } from './removeActiveClass';

export function oneActiveItem(items) {
  for (const item of items) {
    item.addEventListener('click', () => {
      removeActiveClass(items);
      item.classList.add('active');
    });
  }
}
