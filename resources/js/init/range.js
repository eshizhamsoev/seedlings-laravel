import 'nouislider/dist/nouislider.css';
import noUiSlider from 'nouislider';

function initRange(node, { min, max }, value = null) {
  if (!node) {
    return;
  }
  noUiSlider.create(node, {
    range: {
      min,
      max,
    },
    step: 1,
    start: value,
    connect: true,
  });
}

export function connectInputRange(node) {
  if (!node) {
    return;
  }
  const range = node.querySelector('.js-range');
  const inputs = node.querySelectorAll('[data-input]');
  const min = node.querySelector('[data-input="min"]');
  const max = node.querySelector('[data-input="max"]');
  initRange(range, { min: +min.min, max: +max.max }, [+min.value, +max.value]);
  range.noUiSlider.on('update', () => {
    // eslint-disable-next-line no-param-reassign
    min.value = Math.round(range.noUiSlider.get()[0]);
    // eslint-disable-next-line no-param-reassign
    max.value = Math.round(range.noUiSlider.get()[1]);
  });
  for (const input of inputs) {
    input.addEventListener('change', () => {
      range.noUiSlider.set([min.value, max.value]);
    });
  }
}
