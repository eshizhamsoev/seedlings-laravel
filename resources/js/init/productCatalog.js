export function productCatalog(node) {
  if (!node) {
    return;
  }
  const subCatalogs = node.querySelectorAll('.js-sub-catalog');
  const btn = node.querySelector('.js-catalog-btn');
  btn.addEventListener('click', () => {
    btn.classList.toggle('active');
  });
  document.querySelector('body').addEventListener('click', ({ target }) => {
    if (!target.closest('.js-catalog-btn') && !target.closest('.js-catalog')) {
      btn.classList.remove('active');
    }
  });
  for (const item of subCatalogs) {
    const link = item.querySelector('.js-catalog-link');
    link.addEventListener('mouseover', () => {
      item.classList.add('active');
    });
    item.addEventListener('mouseleave', () => {
      item.classList.remove('active');
    });
  }
}
