function changeTabindex(elem) {
  let tabFocus = 0;
  const tabList = elem.querySelector('[role="tablist"]');
  const tabs = elem.querySelectorAll('[role="tab"]');

  tabList.addEventListener('keydown', (e) => {
    if (e.keyCode === 39 || e.keyCode === 37) {
      tabs[tabFocus].setAttribute('tabindex', -1);
      if (e.keyCode === 39) {
        tabFocus += 1;
        if (tabFocus >= tabs.length) {
          tabFocus = 0;
        }
      } else if (e.keyCode === 37) {
        tabFocus -= 1;
        if (tabFocus < 0) {
          tabFocus = tabs.length - 1;
        }
      }
      tabs[tabFocus].setAttribute('tabindex', 0);
      tabs[tabFocus].focus();
    }
  });
}

export function initTabs(tabContainers) {
  for (const item of tabContainers) {
    const tabs = item.querySelectorAll('[role="tab"]');
    const panels = item.querySelectorAll('[role="tabpanel"]');

    tabs.forEach((tab) => {
      tab.addEventListener('click', (e) => {
        const { target } = e;
        const activeTabId = target.getAttribute('aria-controls');
        for (const tabItem of tabs) {
          tabItem.setAttribute('aria-selected', false);
        }
        target.setAttribute('aria-selected', true);
        for (const panel of panels) {
          panel.setAttribute('hidden', true);
          if (panel.getAttribute('id') === activeTabId) {
            panel.removeAttribute('hidden');
          }
        }
      });
    });
    changeTabindex(item);
  }
}
