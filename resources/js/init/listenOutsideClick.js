export function listenOutsideClick(element) {
  let tempFunc;
  let close;
  const promise = new Promise((resolve) => {
    close = resolve;
    tempFunc = (event) => {
      if (!element.contains(event.target)) {
        resolve();
      }
    };
    setTimeout(() => {
      document.addEventListener('click', tempFunc);
    }, 0);
  }).then(() => {
    document.removeEventListener('click', tempFunc);
  });
  return { promise, close };
}
