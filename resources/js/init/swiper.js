import Swiper, { Navigation, FreeMode, Thumbs } from 'swiper';
import { SCREEN_WIDTH } from './variables';

const options = {
  main: {
    modules: [],
    spaceBetween: 30,
  },
  primary: {
    modules: [],
    spaceBetween: 10,
    slidesPerView: 1,
    breakpoints: {
      860: {
        spaceBetween: 30,
        slidesPerView: 4,
      },
      560: {
        spaceBetween: 10,
        slidesPerView: 3,
      },
      374: {
        spaceBetween: 10,
        slidesPerView: 2,
      },
    },
  },
  secondary: {
    modules: [],
    spaceBetween: 10,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        spaceBetween: 30,
        slidesPerView: 3,
      },
      768: {
        spaceBetween: 10,
        slidesPerView: 2,
      },
    },
  },
  freeMode: {
    modules: [FreeMode],
    freeMode: true,
    spaceBetween: 10,
    slidesPerView: 1,
    breakpoints: {
      860: {
        spaceBetween: 30,
        slidesPerView: 4,
      },
      560: {
        spaceBetween: 10,
        slidesPerView: 3,
      },
      400: {
        spaceBetween: 10,
        slidesPerView: 2,
      },
    },
  },
  thumbs: {
    direction: 'vertical',
    slidesPerView: 'auto',
    spaceBetween: 22,
    watchSlidesProgress: true,
  },
  mainThumb: {
    modules: [Thumbs],
    spaceBetween: 10,
    thumbs: {
      swiper: '',
    },
  },
};

function generateOptions(name) {
  return options[name];
}

if (SCREEN_WIDTH <= 1024) {
  options.thumbs.direction = 'horizontal';
  options.thumbs.spaceBetween = 13;
}

export function initSlider(node) {
  const sliderName = node.dataset.swiperType;
  const sliderOptions = generateOptions(sliderName);
  if (!sliderOptions) {
    return;
  }
  const swiperNode = node.querySelector('.swiper');
  const prevEl = node.querySelector('.btn-arrows__item_prev');
  const nextEl = node.querySelector('.btn-arrows__item_next');
  if (prevEl && nextEl) {
    sliderOptions.modules.push(Navigation);
    sliderOptions.navigation = {
      prevEl,
      nextEl,
    };
  }
  const swiper = new Swiper(swiperNode, sliderOptions);
  if (sliderName === 'thumbs') {
    generateOptions('mainThumb').thumbs.swiper = swiper;
  }
}
