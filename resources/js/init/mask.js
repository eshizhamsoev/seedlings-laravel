import IMask from 'imask';

const telephones = document.querySelectorAll('input[type="tel"]');
for (const item of telephones) {
  IMask(item, {
    mask: '+{7}(000)000-00-00',
  });
}
