export function initCalculate(element) {
  if (!element) {
    return;
  }
  const buttons = element.querySelectorAll('[data-calculate]');
  const inputElement = element.querySelector('[data-input]');
  const step = Number(inputElement.step);
  const min = Number(inputElement.min);
  for (const btn of buttons) {
    btn.addEventListener('click', () => {
      let inputValue = Number(inputElement.value);
      switch (btn.dataset.calculate) {
        case 'increment':
          inputValue += step;
          break;
        case 'decrease':
          inputValue -= step;
          break;
        default:
          break;
      }
      inputElement.value = inputValue < min ? min : inputValue;
    });
  }
}
