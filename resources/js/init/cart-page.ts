import { createApp } from 'vue';
import CartPage from '../vue/cart/cart-page.vue';
import { Cart } from '../lib/cart/cart';
import { Api } from '../lib/api/api';

export function initCartPage(
  el: HTMLElement,
  { cart, api }: { cart: Cart; api: Api }
) {
  createApp(CartPage, {
    cartService: cart,
    apiService: api,
  }).mount(el);
}
