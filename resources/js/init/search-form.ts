import { createApp } from 'vue';
import { SearchService } from '../lib/search/searchService';
import { Api } from '../lib/api/api';
import SearchBox from '../vue/search/SearchBox.vue';

export function initSearch(
  elements: {
    block: HTMLElement;
    input: HTMLInputElement;
  },
  api: Api
) {
  const app = createApp(SearchBox, {
    initialSearchInput: elements.input.value,
  });
  app.provide('searchService', new SearchService(api));
  app.mount(elements.block);
}
