export function activeItem(links) {
  for (const link of links) {
    link.addEventListener('click', () => {
      link.classList.toggle('active');
    });
  }
}
