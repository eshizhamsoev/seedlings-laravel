import { createApp } from 'vue';
import Maska from 'maska';
import CheckoutPage from '../vue/checkout/checkout-page.vue';
import { Api } from '../lib/api/api';
import { CheckoutService } from '../lib/checkout/checkout';
import { PickUpPointsService } from '../lib/pick-up-points/pick-up-points';

export function initCheckoutPage(el: HTMLElement, { api }: { api: Api }) {
  const app = createApp(CheckoutPage);
  app.provide('checkoutService', new CheckoutService(api));
  app.provide('pickUpPointsService', new PickUpPointsService(api));

  app.use(Maska);
  app.mount(el);
}
