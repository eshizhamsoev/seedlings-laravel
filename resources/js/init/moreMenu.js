import { listenOutsideClick } from './listenOutsideClick';

export function additionalContent(element) {
  const btn = element.querySelector('[data-additional="btn"]');
  const content = element.querySelector('[data-additional="content"]');
  btn.addEventListener('click', () => {
    btn.classList.toggle('active');
    if (btn.classList.contains('active')) {
      listenOutsideClick(content).promise.then(() => {
        btn.classList.remove('active');
      });
    }
  });
}
