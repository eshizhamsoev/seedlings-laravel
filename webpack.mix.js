const mix = require('laravel-mix');
const config = require('./webpack.config');
require('laravel-mix-svg-sprite');

mix
  .webpackConfig(config)
  .ts('resources/js/app.js', 'js')
  .sass('resources/scss/app.scss', 'css')
  .setPublicPath('public')
  .svgSprite('resources/svg', 'sprite.svg')
  .version()
  .vue();

mix.babelConfig({
  presets: ['@babel/preset-env'],
});
