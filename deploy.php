<?php
namespace Deployer;

$hostname = getenv('HOSTNAME');
$username = getenv('USERNAME');
$deployPath = getenv('DEPLOY_PATH');

require 'recipe/laravel.php';
require 'recipe/rsync.php';
require 'recipe/cachetool.php';


set('cachetool', '/var/run/php/php-fpm.sock');
set('bin/cachetool', 'cachetool-7.0.0.phar');

// Shared files/dirs between deploys
add('shared_files', [
    '.env'
]);

add('shared_dirs', [
    'storage'
]);


set('rsync_src', function () {
    return __DIR__; // If your project isn't in the root, you'll need to change this.
});

// Configuring the rsync exclusions.
// You'll want to exclude anything that you don't want on the production server.
add('rsync', [
    'exclude' => [
        '.git',
        '/.env',
        '/storage/app/*',
        '/storage/framework/*',
        '/storage/framework/cache/*',
        '/storage/framework/sessions/*',
        '/storage/framework/views/*',
        '/storage/logs/*',
        '/vendor/',
        '/node_modules/',
        '/.gitlab-ci.yml',
        '/deploy.php',
    ],
]);

set('allow_anonymous_stats', false);

set('cachetool_args', '--tmp-dir=/tmp --fcgi=/var/run/php/php7.4-fpm.sock');

host('main')
    ->hostname($hostname)
    ->user($username)
    ->port('22')
    ->forwardAgent(true)
    ->multiplexing(false)
    ->set('deploy_path', $deployPath)
    ->addSshOption('StrictHostKeyChecking', 'no');


task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync', // Deploy code & built assets
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link', // |
    'artisan:view:cache',   // |
    'artisan:config:cache', // | Laravel specific steps
    'artisan:optimize',     // |
    'artisan:migrate',      // |
    'deploy:symlink',
    'deploy:unlock',
    'cleanup'
]);
// Recipe modifiers:

// Unlock after failed deploy process
after('deploy:failed', 'deploy:unlock');
